const AdminModel = require('../models/admin')
var mongoose = require('mongoose')
const ObjectId = mongoose.Types.ObjectId;

exports.get_admin_details = ({ id }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let admin = await AdminModel.findById(ObjectId(id));
            if (!admin || admin == null) {
                resolve({ success: false, message: 'Admin does not exist.' })
            } else {
                resolve({ success: true, data: admin, message: 'Admin details returned successfully' });
            }
        } catch (err) {
            reject(err);
        }
    });
}

exports.update_adminprofile = ({ id, body, file }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let data = JSON.parse(body.data); let response = {}
            let isAdminExist = await AdminModel.findOne({ $and: [{ 'email': data.email }, { status: 'Active' }] }, { firstName: 1, lastName: 1, email: 1, mobile_number: 1, status: 1 })
            if (isAdminExist && isAdminExist._id != id) {
                response = { success: false, message: 'Email address you have entered is already registered.' };
            } else {
                if (id.match(/^[0-9a-fA-F]{24}$/)) {
                    let admin_data = {
                        firstName: data.firstName,
                        lastName: data.lastName,
                        email: data.email,
                        mobile_number: data.mobile_number,
                        dob: data.dob,
                        gender: data.gender
                    }
                    var new_data = {};
                    if (file) {
                        new_data = Object.assign({ profileImage: file.filename }, admin_data);
                    } else {
                        new_data = admin_data;
                    }
                    let admin_updated = await AdminModel.findByIdAndUpdate(ObjectId(id), { $set: new_data }, { new: true })
                    if (!admin_updated) {
                        response = { success: false, message: 'Admin profile does not exists.' };
                    } else {
                        response = { success: true, data: admin_updated, message: 'Admin profile update successfully' };
                    }
                } else {
                    response = { success: false, message: 'Admin is not valid.' };
                }
            }
            resolve(response)
        } catch (err) {
            reject(err)
        }
    })
}

exports.change_adminpassword = ({ req, body }) => {
    return new Promise(async (resolve, reject) => {
        try {
            const adminCredentials = await AdminModel.findOne({ $and: [{ email: req.decoded.email }, { status: 'Active' }] })
            let model = new AdminModel();
            let validatePass = await model.compare(body.oldpassword, adminCredentials.password);
            if (!validatePass) {
                resolve({ success: false, message: 'Invalid old password' })
            } else {
                adminCredentials.password = await model.hash(body.newpassword);
                await adminCredentials.save();
                resolve({ success: true, message: 'Your password has been changed successfully.' })
            }
        } catch (error) {
            reject(error);
        }
    });
}