const CreditNoteModel = require("../models/creditnote");
const generateuid = require("../services/generateuid");
// const PDFConverter = require("phantom-html-to-pdf")({});

var PDFConverter = require("html-pdf");
let width = 794,
  height = 1122;

var options = {
  width: `${width}px`,
  height: `${height}px`,
  border: "5px",
  viewportSize: {
    width,
    height
  }
};

const customerModel = require("../models/customer");
const invoiceModel = require("../models/packinglist");
const Mailer = require("../services/mailer");
var moment = require("moment");
const path = require("path");
const fs = require("fs");

exports.create_creditnote = ({ res, body }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let credit_note_data = await CreditNoteModel.find()
        .sort("-credit_note_number")
        .limit(1)
        .exec();
      let {
        credit_note_uid,
        credit_note_number
      } = await generateuid.generateuidtypes({
        uiddata: credit_note_data,
        uidtype: "creditnote"
      });
      const creditNoteObj = new CreditNoteModel({
        credit_note_uid: credit_note_uid,
        credit_note_number: credit_note_number,
        client: body.client,
        credit_note_no: body.credit_note_no,
        invoice_uid: body.invoice_uid,
        product_uid: body.product_uid,
        batch_number: body.batch_number,
        reason_for_creditnote: body.reason_for_creditnote,
        qauntity: body.qauntity,
        price: body.price,
        percentage: body.percentage,
        credit_amount: body.credit_amount
      });
      let creditnote = await creditNoteObj.save();
      if (!creditnote) {
        throw new Error("Credit note Not created ...");
      }
      await generateandsentcreditnotedoc(res, credit_note_uid, "Admin");
      resolve(creditnote);
    } catch (err) {
      reject(err);
    }
  });
};

exports.getallcreditnotes = ({ query }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let { pageNo, pageSize, search } = query;
      let querydata = {};
      let _pageNo = parseInt(isNaN(pageNo) ? 1 : pageNo);
      let _pageSize = parseInt(isNaN(pageSize) ? 10 : pageSize);
      let skip = parseInt((_pageNo - 1) * _pageSize);
      let sort = { createdAt: -1 };
      if (search && search != "") {
        querydata = { $text: { $search: `\"${search}\"` } };
      }
      let _count = await CreditNoteModel.countDocuments(querydata);
      let creditnotes = await CreditNoteModel.find(querydata)
        .sort(sort)
        .skip(skip)
        .limit(_pageSize);
      resolve({
        success: true,
        data: creditnotes,
        count: _count,
        message: "Creditnote fetched Successfully"
      });
    } catch (err) {
      reject(err);
    }
  });
};

exports.uploadsigned_creditnote = ({ invoiceid, body, file }) => {
  return new Promise(async (resolve, reject) => {
    try {
      body = JSON.parse(body.data);
      const creditnote = await CreditNoteModel.findOneAndUpdate(
        {
          $and: [
            { invoice_uid: invoiceid },
            { credit_note_uid: body.credit_note_uid },
            { product_uid: body.product_id }
          ]
        },
        { $set: { status: "Approved", signed_creditnote_doc: file.filename } }
      );
      resolve(creditnote);
    } catch (err) {
      reject(err);
    }
  });
};

exports.send_cn_email_to_client = ({ res, credit_note_uid }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let cndoc = await generateandsentcreditnotedoc(
        res,
        credit_note_uid,
        "User"
      );
      resolve(cndoc);
    } catch (err) {
      reject(err);
    }
  });
};

exports.getcreditnote_details = ({ creditnoteid }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let creditnotedetails = await CreditNoteModel.findOne({
        credit_note_uid: creditnoteid
      });
      resolve(creditnotedetails);
    } catch (err) {
      reject(err);
    }
  });
};

exports.getallinvoices = ({}) => {
  return new Promise(async (resolve, reject) => {
    try {
      let invoices = await invoiceModel
        .find({ invoice_uid: { $exists: true } })
        .sort({ createdAt: -1 });
      resolve(invoices);
    } catch (err) {
      reject(err);
    }
  });
};

exports.update_creditnote = ({ creditnoteid, body }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let data = {
        client: body.client,
        credit_note_no: body.credit_note_no,
        invoice_uid: body.invoice_uid,
        product_uid: body.product_uid,
        batch_number: body.batch_number,
        reason_for_creditnote: body.reason_for_creditnote,
        qauntity: body.qauntity,
        price: body.price,
        percentage: body.percentage,
        credit_amount: body.credit_amount
      };
      let cn = await CreditNoteModel.findByIdAndUpdate(
        creditnoteid,
        { $set: data },
        { new: true }
      );
      if (!cn) {
        throw new Error("CreditNote does not exists.");
      } else {
        resolve(cn);
      }
    } catch (err) {
      reject(err);
    }
  });
};

exports.delete_creditnote = ({ invoiceid, creditnoteid }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let invoices = await invoiceModel.findOne({
        $and: [
          { invoice_uid: { $exists: true } },
          { invoice_uid: invoiceid },
          { status: "CloseInvoice" }
        ]
      });
      if (invoices) {
        throw new Error(
          "Creditnote con't be removed, because invoice is already closed."
        );
      } else {
        let creditnotes = await CreditNoteModel.findOne({
          $and: [
            { invoice_uid: invoiceid },
            { credit_note_uid: creditnoteid },
            { status: "Approved" }
          ]
        });
        if (creditnotes) {
          throw new Error(
            "Creditnote con't be removed, because creditnote is already approved."
          );
        } else {
          let credit = await CreditNoteModel.deleteOne({
            $and: [
              { invoice_uid: invoiceid },
              { credit_note_uid: creditnoteid }
            ]
          });
          if (credit.deletedCount == 0) {
            throw new Error("Creditnote is not removed");
          } else {
            resolve();
          }
        }
      }
    } catch (err) {
      reject(err);
    }
  });
};

function generateandsentcreditnotedoc(res, credit_note_uid, sender_type) {
  return new Promise(async (resolve, reject) => {
    try {
      let creditnotedata = await CreditNoteModel.findOne({
        credit_note_uid: credit_note_uid
      });
      if (!creditnotedata || creditnotedata == null) {
        throw new Error("Credit Note does not exists.");
      }
      let customerdata = await customerModel.findOne({
        customer_uid: creditnotedata.client.client_uid
      });
      if (!customerdata || customerdata == null) {
        throw new Error("customer does not exists.");
      }
      let dir = `${path.dirname(
        process.mainModule.filename
      )}/upload/creditnote_doc`;
      let creditnote_document = `creditnote_${
        creditnotedata.credit_note_uid
      }_${new Date().getTime()}.pdf`;
      let creditnote = `${dir}/${creditnote_document}`;
      if (sender_type == "Admin") {
        await CreditNoteModel.findOneAndUpdate(
          { credit_note_uid: credit_note_uid },
          { $set: { creditnote_document: creditnote_document } },
          { new: true }
        );
      } else {
        await CreditNoteModel.findOneAndUpdate(
          { credit_note_uid: credit_note_uid },
          {
            $set: {
              creditnote_document: creditnote_document,
              sent_cn_to_client: true
            }
          },
          { new: true }
        );
      }
      let total_price = await inWords(creditnotedata.credit_amount.toFixed(2));
      res.render(
        "creditnote",
        {
          creditnote: creditnotedata,
          customerdata: customerdata,
          total_price: total_price,
          moment: moment
        },
        async (err, html) => {
          if (err) {
            throw new Error("html is not generated.");
          } else {
            PDFConverter.create(html, options).toStream(async (err, pdf) => {
              if (err) {
                throw new Error("pdf is not generated.");
              } else {
                if (!fs.existsSync(dir)) {
                  fs.mkdirSync(dir);
                }
                let stream = fs.createWriteStream(creditnote);
                pdf.pipe(stream);
                attachments = [
                  {
                    filename: creditnote_document,
                    contentType: "application/pdf",
                    path: `${path.dirname(
                      process.mainModule.filename
                    )}/upload/creditnote_doc/${creditnote_document}`
                  }
                ];
                if (sender_type == "Admin") {
                  await Mailer.send(
                    process.env.ADMINEMAIL,
                    "Credit Note",
                    `Please find the below attachment for ${creditnotedata.credit_note_uid}`,
                    attachments
                  );
                } else {
                  await Mailer.send(
                    customerdata.basic_details.email,
                    "Credit Note",
                    `Please find the below attachment for ${creditnotedata.credit_note_uid}`,
                    attachments
                  );
                }
                resolve(creditnotedata);
              }
            });
          }
        }
      );
    } catch (error) {
      reject(error);
    }
  });
}

async function inWords(num) {
  var a = [
    "",
    "One ",
    "Two ",
    "Three ",
    "Four ",
    "Five ",
    "Six ",
    "Seven ",
    "Eight ",
    "Nine ",
    "Ten ",
    "Eleven ",
    "Twelve ",
    "Thirteen ",
    "Fourteen ",
    "Fifteen ",
    "Sixteen ",
    "Seventeen ",
    "Sighteen ",
    "Nineteen "
  ];
  var b = [
    "",
    "",
    "Twenty",
    "Thirty",
    "Forty",
    "Fifty",
    "Sixty",
    "Seventy",
    "Eighty",
    "Ninety"
  ];
  if ((num = num.toString()).length > 9) return "overflow";
  var numarray = num.toString().split(".");
  num = numarray[0];
  dot = numarray[1];
  n = ("000000000" + num)
    .substr(-9)
    .match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
  d = ("000" + dot).substr(-2).match(/^(\d{2})$/);
  if (!n) return;
  var str = "";
  str +=
    n[1] != 0
      ? (a[Number(n[1])] || b[n[1][0]] + " " + a[n[1][1]]) + "crore "
      : "";
  str +=
    n[2] != 0
      ? (a[Number(n[2])] || b[n[2][0]] + " " + a[n[2][1]]) + "lakh "
      : "";
  str +=
    n[3] != 0
      ? (a[Number(n[3])] || b[n[3][0]] + " " + a[n[3][1]]) + "thousand "
      : "";
  str +=
    n[4] != 0
      ? (a[Number(n[4])] || b[n[4][0]] + " " + a[n[4][1]]) + "hundred "
      : "";
  str += n[5] != 0 ? a[Number(n[5])] || b[n[5][0]] + " " + a[n[5][1]] : "";
  str +=
    d[1] != 0
      ? (d[1] != "" ? "And " : "") +
        (a[Number(d[1])] || b[d[1][0]] + " " + a[d[1][1]]) +
        "Cent Only "
      : "Only";
  return str;
}
