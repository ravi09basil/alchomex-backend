const customerModel = require("../models/customer");
const productModel = require("../models/product");
const generateuid = require("../services/generateuid");

exports.create_customer = ({ body }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let customer_data = await customerModel
        .find()
        .sort("-customer_number")
        .limit(1)
        .exec();
      let {
        customer_uid,
        customer_number
      } = await generateuid.generateuidtypes({
        uiddata: customer_data,
        uidtype: "customer"
      });
      const customerobj = new customerModel({
        customer_uid: customer_uid,
        customer_number: customer_number,
        basic_details: body.basic_details,
        notify_party: body.notify_party,
        client_poc: body.client_poc,
        additional_details: body.additional_details
      });
      customerobj.basic_details.password = customerobj.hash(
        customerobj.basic_details.password
      );
      let customer = await customerobj.save();
      if (!customer || customer == null) {
        throw new Error("User not added.");
      }
      resolve(customer);
    } catch (err) {
      reject(err);
    }
  });
};

exports.getallcustomers = ({ query }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let { pageNo, pageSize, search } = query;
      let querydata;
      let _pageNo = parseInt(isNaN(pageNo) ? 1 : pageNo);
      let _pageSize = parseInt(isNaN(pageSize) ? 100 : pageSize);
      let skip = parseInt((_pageNo - 1) * _pageSize);
      let sort = { createdAt: 1 };
      if (search && search != "") {
        querydata = {
          $and: [{ $text: { $search: `\"${search}\"` } }, { status: "Active" }]
        };
      } else {
        querydata = { status: "Active" };
      }
      let _count = await customerModel.countDocuments(querydata);
      let customers = await customerModel
        .find(querydata)
        .sort(sort)
        .skip(skip)
        .limit(_pageSize);
      resolve({
        success: true,
        data: customers,
        count: _count,
        message: "Customers data fetched Successfully"
      });
    } catch (err) {
      reject(err);
    }
  });
};

exports.getcustomeruids = ({}) => {
  return new Promise(async (resolve, reject) => {
    try {
      let customers = await customerModel.find(
        { status: "Active" },
        {
          _id: 0,
          customer_uid: 1,
          "basic_details.registered_name": 1,
          "basic_details.ports_of_discharge": 1
        }
      );
      resolve(customers);
    } catch (err) {
      reject(err);
    }
  });
};

exports.getOne_customer = ({ customerid }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let customerdata = await customerModel.findById(customerid);
      if (!customerdata) {
        throw new Error("Customer does not exists.");
      } else {
        resolve(customerdata);
      }
    } catch (err) {
      reject(err);
    }
  });
};

exports.update_customer = ({ customerid, body }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let data = {
        basic_details: body.basic_details,
        notify_party: body.notify_party,
        client_poc: body.client_poc,
        additional_details: body.additional_details
      };
      let customerdata = await customerModel.findByIdAndUpdate(
        customerid,
        body,
        { new: true }
      );
      if (!customerdata) {
        throw new Error("Customer does not exists.");
      } else {
        resolve(customerdata);
      }
    } catch (err) {
      reject(err);
    }
  });
};

exports.delete_customer = ({ customeruid }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let products = await productModel.find(
        { customer_uid: customeruid },
        { product_uid: 1 }
      );
      if (products.length > 0) {
        throw new Error("Customer con't be removed, already in used");
      } else {
        let customer = await customerModel.deleteOne({
          customer_uid: customeruid
        });
        if (customer.deletedCount == 0) {
          throw new Error("Customer is not removed");
        } else {
          resolve();
        }
      }
    } catch (err) {
      reject(err);
    }
  });
};

exports.change_password = ({ req, body }) => {
  return new Promise(async (resolve, reject) => {
    try {
      const userCredentials = await customerModel.findOne({
        $and: [
          { "basic_details.email": req.decoded.email },
          { status: "Active" }
        ]
      });
      let model = new customerModel();
      let validatePass = await model.compare(
        body.oldpassword,
        userCredentials.basic_details.password
      );
      if (!validatePass) {
        resolve({ success: false, message: "Invalid old password" });
      } else {
        userCredentials.basic_details.password = await model.hash(
          body.newpassword
        );
        await userCredentials.save();
        resolve({
          success: true,
          message: "Your password has been changed successfully."
        });
      }
    } catch (error) {
      reject(error);
    }
  });
};
