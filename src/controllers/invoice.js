const invoiceModel = require('../models/packinglist');
const CreditNoteModel = require('../models/creditnote');
const paymentModel = require('../models/payment');
const generateuid = require('../services/generateuid');
const PDFConverter = require("phantom-html-to-pdf")({});
const customerModel = require('../models/customer');
const piModel = require('../models/pi');
const Mailer = require('../services/mailer')
const fs = require('fs');
const path = require('path');
const async = require("async");
var moment = require('moment');
var round = require('mongo-round');
var mongoose = require('mongoose');
const supplierModel = require('../models/supplier');
const ObjectId = mongoose.Types.ObjectId;

exports.create_invoice = ({ req, res, packinglistid, body }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let invoice_data = await invoiceModel.find({ "invoice_uid": { $exists: true, $ne: null } }).sort('-invoice_number').limit(1).exec();
            let { invoice_uid, invoice_number } = await generateuid.generateuidtypes({ uiddata: invoice_data, uidtype: 'invoice' })
            let data = {
                invoice_uid: invoice_uid,
                invoice_number: invoice_number,
                gross_price: body.gross_price,
                insurance: (body.insurance) ? body.insurance : 0,
                freight: (body.freight) ? body.freight : 0,
                insurance_usd: (body.insurance_usd) ? body.insurance_usd : 0,
                freight_usd: (body.freight_usd) ? body.freight_usd : 0,
                air_freight: (body.air_freight) ? body.air_freight : 0,
                total_fob_value: (body.total_fob_value) ? body.total_fob_value : 0,
                total_price: body.total_price,
                credit_price: body.credit_price,
                invoice_format: body.invoice_format,
                invoice_date: new Date()
            }
            if (data.invoice_format == 'LUT') {
                data = Object.assign({ LUT_number: body.LUT_number }, data);
            }
            let invoice = await invoiceModel.findByIdAndUpdate(packinglistid, { $set: data }, { new: true });
            console.log()
            if (!invoice || invoice == null) {
                throw new Error('Invoice is not found..');
            } else {
                async.each(invoice.products, async (product, cb) => {
                    await piModel.findOneAndUpdate({ pi_uid: product.pi_uid, "products": { "$elemMatch": { "product_uid": product.product_id, "batches._id": ObjectId(product.batch_id) } } }, { "$set": { "products.$[outer].batches.$[inner].dispatch.status": true, "products.$[outer].batches.$[inner].dispatch.uploaded_date": new Date(), "products.$[outer].batches.$[inner].dispatch.uploaded_by": req.decoded.id, "products.$[outer].batches.$[inner].workstatus": "Dispatch", "products.$[outer].batches.$[inner].greenTick": 5, "products.$[outer].batches.$[inner].redTick": 2 } }, { "arrayFilters": [{ "outer.product_uid": product.product_id }, { "inner._id": ObjectId(product.batch_id) }] })
                    let pidata = await piModel.aggregate([{ $match: { $and: [{ status: 'OpenOrder' }, { pi_uid: product.pi_uid }] } }, { $project: { client: 1, total_batches: { "$sum": { "$map": { "input": "$products", "as": "product", "in": { "$sum": { "$map": { "input": "$$product.batches", "as": "batch", "in": { "$cond": [{ $and: [{ $ne: ["$batch._id", null] }] }, 1, 0] } } } } } } }, "dispatchcount": { "$sum": { "$map": { "input": "$products", "as": "product", "in": { "$sum": { "$map": { "input": "$$product.batches", "as": "batch", "in": { "$cond": [{ $and: [{ "$eq": ["$$batch.artwork.status", true] }, { "$eq": ["$$batch.manufacture.status", true] }, { "$eq": ["$$batch.testing.status", true] }, { "$eq": ["$$batch.dispatch.status", true] }] }, 1, 0] } } } } } } } } }]);
                    if (pidata && pidata[0] && (parseInt(pidata[0].total_batches) == parseInt(pidata[0].dispatchcount)))
                        await piModel.findOneAndUpdate({ pi_uid: product.pi_uid }, { "$set": { status: 'ShippedOrder' } })
                    cb ? cb(null) : null
                }, async (err) => {
                    if (err) {
                        throw new Error('Internal error occured...');
                    } else {
                        await generateandsentinvoicedoc(res, invoice.invoice_uid);
                        resolve(invoice)
                    }
                })
            }
        } catch (err) {
            reject(err)
        }
    })
}

exports.update_invoice = ({ res, packinglistid, body }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let data = {
                gross_price: body.gross_price,
                insurance: (body.insurance) ? body.insurance : 0,
                freight: (body.freight) ? body.freight : 0,
                insurance_usd: (body.insurance_usd) ? body.insurance_usd : 0,
                freight_usd: (body.freight_usd) ? body.freight_usd : 0,
                air_freight: (body.air_freight) ? body.air_freight : 0,
                total_fob_value: (body.total_fob_value) ? body.total_fob_value : 0,
                total_price: body.total_price,
                credit_price: body.credit_price
            }
            let invoice = await invoiceModel.findByIdAndUpdate(packinglistid, { $set: data }, { new: true });
            if (!invoice || invoice == null) {
                throw new Error('Invoice is not found..');
            }
            await generateandsentinvoicedoc(res, invoice.invoice_uid);
            resolve(invoice)
        } catch (err) {
            reject(err)
        }
    })
}

exports.getinvoice_uids = ({ client_uid }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let invoices = await invoiceModel.find({ $and: [{ 'client.client_uid': client_uid }, { invoice_uid: { $exists: true } }] }, { invoice_uid: 1 }).sort({ "createdAt": -1 });
            resolve(invoices)
        } catch (err) {
            reject(err)
        }
    })
}

exports.getallinvoices = ({ query }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let { pageNo, pageSize, search } = query; let querydata = {};
            let _pageNo = parseInt(isNaN(pageNo) ? 1 : pageNo);
            let _pageSize = parseInt(isNaN(pageSize) ? 100 : pageSize);
            let skip = parseInt((_pageNo - 1) * _pageSize);
            let sort = { "createdAt": -1 };
            if (search && search != '') {
                querydata = { $and: [{ $text: { $search: `\"${search}\"` } }, { invoice_uid: { $exists: true } }] }
            } else {
                querydata = { invoice_uid: { $exists: true } }
            }
            let _count = await invoiceModel.countDocuments(querydata);
            let invoices = await invoiceModel.find(querydata).sort(sort).skip(skip).limit(_pageSize);
            resolve({ success: true, data: invoices, count: _count, message: 'Invoice lists fetched Successfully' });
        } catch (err) {
            reject(err)
        }
    })
}

exports.getinvoice_products = ({ client_uid, invoice_uid }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let products = [];
            let invoices = await invoiceModel.findOne({ $and: [{ 'client.client_uid': client_uid }, { invoice_uid: { $exists: true } }, { invoice_uid: invoice_uid }] });
            if (invoices && invoices.products.length > 0) {
                invoices.products.filter(function (item) {
                    var i = products.findIndex(x => x.product_id == item.product_id);
                    if (i <= -1) {
                        products.push({ product_id: item.product_id, customerCodeNumber: item.customerCodeNumber, brand_name: item.brand_name });
                    }
                    return null;
                });
            }
            resolve(products)
        } catch (err) {
            reject(err)
        }
    })
}

exports.getinvoice_product_batches = ({ invoice_uid, product_id }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let invoices = await invoiceModel.aggregate([{ $match: { $and: [{ invoice_uid: { $exists: true } }, { invoice_uid: invoice_uid }] } }, { $unwind: "$products" }, { $match: { 'products.product_id': product_id } }, { $group: { _id: "$products.batch_number", "price": { "$first": "$products.price" } } }, { $group: { _id: "$batch_number", distinctbatchs: { $addToSet: { "batch_number": "$_id", "price": "$price" } } } }])
            if (invoices.length > 0) {
                resolve(invoices[0].distinctbatchs)
            } else {
                resolve(invoices)
            }
        } catch (err) {
            reject(err)
        }
    })
}

exports.get_post_shipping_orders = ({ query, req }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let { pageNo, pageSize, search } = query; let data_query;
            let _pageNo = parseInt(isNaN(pageNo) ? 1 : pageNo);
            let _pageSize = parseInt(isNaN(pageSize) ? 100 : pageSize);
            let skip = parseInt((_pageNo - 1) * _pageSize);
            if (req.decoded.userType == 'Admin') {
                if (search && search != '') {
                    data_query = { $and: [{ $text: { $search: `\"${search}\"` } }, { invoice_uid: { $exists: true } }, { $or: [{ status: 'OpenInvoice' }, { status: 'CloseInvoice' }] }] };
                } else {
                    data_query = { $and: [{ $or: [{ status: 'OpenInvoice' }, { status: 'CloseInvoice' }] }, { invoice_uid: { $exists: true } }] };
                }
            } else {
                if (search && search != '') {
                    data_query = { $and: [{ $text: { $search: `\"${search}\"` } }, { invoice_uid: { $exists: true } }, { 'client.client_uid': req.decoded.customer_uid }, { $or: [{ status: 'OpenInvoice' }, { status: 'CloseInvoice' }] }] };
                } else {
                    data_query = { $and: [{ invoice_uid: { $exists: true } }, { 'client.client_uid': req.decoded.customer_uid }, { $or: [{ status: 'OpenInvoice' }, { status: 'CloseInvoice' }] }] };
                }
            }
            let invoicelists = await invoiceModel.aggregate([{ $match: data_query }, { $lookup: { from: "creditnotes", localField: "invoice_uid", foreignField: "invoice_uid", as: "creditnote" } }, { $lookup: { from: "payments", let: { invoice_uid: "$invoice_uid" }, pipeline: [{ $match: { $expr: { $and: [{ $eq: ["$$invoice_uid", "$invoice_uid"] }] } } }, { $sort: { createdAt: -1 } }], as: "payment" } }, {
                $project: {
                    po_reference_no: { $map: { input: "$products", as: "product", "in": "$$product.client_po_number" } },
                    payments: { $arrayElemAt: ["$payment", 0] }, "admin_document.productinvoice_doc": 1, client: 1, invoice_uid: 1, invoice_date: 1, total_price: 1, credit_price: 1, shipped_onboard_date: 1, payment_due_date: 1, "creditnoteTotal": { "$sum": { "$map": { "input": "$creditnote", "as": "credit", "in": { "$cond": [{ "$eq": ["$$credit.status", 'Approved'] }, "$$credit.credit_amount", 0] } } } }, "paymentTotal": { "$sum": { "$map": { "input": "$payment", "as": "payment", "in": "$$payment.amount_received" } } }
                }
            }, { "$project": { po_reference_no: "$po_reference_no", actual_payment_date: "$payments.createdAt", invoice_doc: "$admin_document.productinvoice_doc", client: 1, invoice_uid: 1, invoice_date: 1, shipped_onboard_date: 1, payment_due_date: 1, credit_price: 1, no_of_days_overdo: round({ "$cond": { "if": { "$or": [{ $eq: ["$status", 'CloseInvoice'] }, { "$lte": [{ "$subtract": [{ "$subtract": ["$total_price", "$creditnoteTotal"] }, { "$sum": ["$credit_price", "$paymentTotal"] }] }, 0] }] }, "then": 0, else: { $divide: [{ $subtract: [new Date(), '$payment_due_date'] }, 1000 * 60 * 60 * 24] } } }, 0), "invoice_amount": round("$total_price", 2), "total_creditnote_amount": round("$creditnoteTotal", 2), "total_paid_amount": round({ "$sum": ["$credit_price", "$paymentTotal"] }, 2), "total_pending_amount": round({ "$subtract": [{ "$subtract": ["$total_price", "$creditnoteTotal"] }, { "$sum": ["$credit_price", "$paymentTotal"] }] }, 2), "no_of_days_delayed": round({ "$cond": { "if": { "$or": [{ $eq: ["$status", 'CloseInvoice'] }, { "$lte": [{ "$subtract": [{ "$subtract": ["$total_price", "$creditnoteTotal"] }, { "$sum": ["$credit_price", "$paymentTotal"] }] }, 0] }] }, "then": { $divide: [{ $subtract: ["$payments.createdAt", '$payment_due_date'] }, 1000 * 60 * 60 * 24] }, else: 0 } }, 0) } }, { $facet: { edges: [{ $sort: { "invoice_date": -1 } }, { $skip: skip }, { $limit: _pageSize }], pageInfo: [{ $group: { _id: null, count: { $sum: 1 } } }] } }])
            if (invoicelists[0].edges.length > 0) {
                resolve({ success: true, data: invoicelists[0].edges, count: invoicelists[0].pageInfo[0].count, message: 'Invoice lists data fetched Successfully' })
            } else {
                resolve({ success: true, data: invoicelists[0].edges, count: 0, message: 'Invoice lists data fetched Successfully' })
            }
        } catch (err) {
            reject(err);
        }
    });
}

exports.get_invoices = ({ query, req }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let { invoicelisttype } = query;
            let invoicequery = await invoiceQuery(query, req);
            let invoice = await invoiceModel.aggregate(invoicequery);
            // if (invoicelisttype && invoicelisttype != '' && invoicelisttype == 'invoicelistcount') {
            //     resolve({ success: true, data: invoice, message: 'All PIs returned Successfully' })
            // } else {
            //     resolve({ success: true, data: pi[0].edges, count: pi[0].pageInfo[0].count, message: 'All PIs returned Successfully' })
            // }
            resolve(invoice)
        } catch (err) {
            reject(err)
        }
    })
}

exports.upload_admindoc = ({ invoiceid, packing_doc, invoice_doc, certificate_of_origin_doc, bill_of_lading_doc, insurance_doc, airway_bill_doc }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let admindoc = {};
            if (packing_doc != '') {
                admindoc = Object.assign({ 'admin_document.packinglist_doc': packing_doc }, admindoc);
            }
            // if (coa_doc != '') {
            //     admindoc = Object.assign({ 'admin_document.CoA_doc': coa_doc }, admindoc);
            // }
            if (invoice_doc != '') {
                admindoc = Object.assign({ 'admin_document.productinvoice_doc': invoice_doc }, admindoc);
            }
            if (certificate_of_origin_doc != '') {
                admindoc = Object.assign({ 'admin_document.certificate_of_origin_doc': certificate_of_origin_doc }, admindoc);
            }
            if (bill_of_lading_doc != '') {
                admindoc = Object.assign({ 'admin_document.bill_of_lading_doc': bill_of_lading_doc }, admindoc);
            }
            if (insurance_doc != '') {
                admindoc = Object.assign({ 'admin_document.insurance_doc': insurance_doc }, admindoc);
            }
            if (airway_bill_doc != '') {
                admindoc = Object.assign({ 'admin_document.airway_bill_doc': airway_bill_doc }, admindoc);
            }
            let invoice = await invoiceModel.findOneAndUpdate({ $and: [{ invoice_uid: invoiceid }, { invoice_uid: { $exists: true } }] }, { $set: admindoc }, { new: true });
            if (!invoice || invoice == null) {
                throw new Error('Invoice does not exists.');
            } else {
                resolve(invoice)
            }
        } catch (err) {
            reject(err)
        }
    })
}

exports.sent_docs_email_for_client = ({ invoiceid }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let invoice = await invoiceModel.findOne({ $and: [{ invoice_uid: invoiceid }, { invoice_uid: { $exists: true } }] }, {});
            if (!invoice || invoice == null) {
                throw new Error('Invoice does not exists.');
            } else {
                let attachments = [];
                let customers = await customerModel.findOne({ $and: [{ customer_uid: invoice.client.client_uid }, { status: 'Active' }] }, { _id: 0, customer_uid: 1, "basic_details.registered_name": 1, 'basic_details.email': 1 })
                if (invoice.admin_document.CoA_doc) {
                    for (let file of invoice.admin_document.CoA_doc) {
                        attachments.push({ filename: file, contentType: 'application/pdf', path: `${path.dirname(process.mainModule.filename)}/upload/CoA_doc/${file}` })
                    }
                }
                if (invoice.admin_document.packinglist_doc)
                    attachments.push({ filename: invoice.admin_document.packinglist_doc, contentType: 'application/pdf', path: `${path.dirname(process.mainModule.filename)}/upload/packinglist/${invoice.admin_document.packinglist_doc}` })
                if (invoice.admin_document.productinvoice_doc)
                    attachments.push({ filename: invoice.admin_document.productinvoice_doc, contentType: 'application/pdf', path: `${path.dirname(process.mainModule.filename)}/upload/invoice/${invoice.admin_document.productinvoice_doc}` })
                if (invoice.admin_document.certificate_of_origin_doc)
                    attachments.push({ filename: invoice.admin_document.certificate_of_origin_doc, contentType: 'application/pdf', path: `${path.dirname(process.mainModule.filename)}/upload/certificate_of_origin_doc/${invoice.admin_document.certificate_of_origin_doc}` })
                if (invoice.admin_document.bill_of_lading_doc)
                    attachments.push({ filename: invoice.admin_document.bill_of_lading_doc, contentType: 'application/pdf', path: `${path.dirname(process.mainModule.filename)}/upload/bill_of_lading_doc/${invoice.admin_document.bill_of_lading_doc}` })
                if (invoice.admin_document.insurance_doc)
                    attachments.push({ filename: invoice.admin_document.insurance_doc, contentType: 'application/pdf', path: `${path.dirname(process.mainModule.filename)}/upload/insurance_doc/${invoice.admin_document.insurance_doc}` })
                if (invoice.admin_document.airway_bill_doc)
                    attachments.push({ filename: invoice.admin_document.airway_bill_doc, contentType: 'application/pdf', path: `${path.dirname(process.mainModule.filename)}/upload/airwaybill_doc/${invoice.admin_document.airway_bill_doc}` })
                await Mailer.send(customers.basic_details.email, `Invoice ${invoice.invoice_uid} docs`, `Please find the below attachments for Invoice : ${invoice.invoice_uid}`, attachments);
                await invoiceModel.findOneAndUpdate({ $and: [{ invoice_uid: invoiceid }, { invoice_uid: { $exists: true } }] }, { $set: { 'admin_document.send_to_client': true, "admin_document.send_to_client_date": new Date() } }, { new: true });
                resolve()
            }
        } catch (err) {
            reject(err);
        }
    });
}

exports.update_doc_approval_status = ({ invoiceid, body }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let invoice = await invoiceModel.findOneAndUpdate({ $and: [{ invoice_uid: invoiceid }, { invoice_uid: { $exists: true } }] }, { $set: { "admin_document.approval_status": body.approval_status } }, { fields: { "admin_document": 1 }, new: true });
            if (!invoice || invoice == null) {
                throw new Error('Invoice does not exists.');
            } else {
                resolve(invoice)
            }
        } catch (err) {
            reject(err)
        }
    })
}

exports.Add_shipped_info = ({ invoiceid, body }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let shipped_info = {
                'shipped_info.tracking_number': body.tracking_number,
                'shipped_info.date_of_courier': body.date_of_courier,
                'shipped_info.courires_agency': body.courires_agency,
                'shipped_info.delivery_date': body.delivery_date,
                'shipped_info.delivery_status': body.delivery_status,
            }
            let invoice = await invoiceModel.findOneAndUpdate({ $and: [{ invoice_uid: invoiceid }, { invoice_uid: { $exists: true } }] }, { $set: shipped_info }, { new: true });
            if (!invoice || invoice == null) {
                throw new Error('Invoice does not exists.');
            } else {
                resolve(invoice)
            }
        } catch (err) {
            reject(err)
        }
    })
}

exports.shipped_at_port = ({ invoiceid }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let invoice = await invoiceModel.findOneAndUpdate({ $and: [{ invoice_uid: invoiceid }, { invoice_uid: { $exists: true } }] }, { $set: { shipped_at_port: true } }, { fields: { "invoice_uid": 1, "total_price": 1, "status": 1 }, new: true });
            if (!invoice || invoice == null) {
                throw new Error('Invoice does not exists.');
            } else {
                resolve(invoice)
            }
        } catch (err) {
            reject(err)
        }
    })
}

exports.getinvoice_details = ({ invoiceid, req }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let data_query = {};
            if (req.decoded.userType == 'Admin') {
                data_query = { $and: [{ invoice_uid: invoiceid }, { invoice_uid: { $exists: true } }] }
            } else {
                data_query = { $and: [{ 'client.client_uid': req.decoded.customer_uid }, { invoice_uid: invoiceid }, { invoice_uid: { $exists: true } }] }
            }
            let invoicedetails = await invoiceModel.aggregate([{ $match: data_query }, { $project: { invoice_uid: 1, client: 1, createdAt: 1, admin_document: 1, shipped_info: 1, status: 1 } }]);
            resolve(invoicedetails)
        } catch (err) {
            reject(err)
        }
    })
}

exports.getinvoice_details_old = ({ invoiceid, req }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let data_query = {};
            if (req.decoded.userType == 'Admin') {
                data_query = { $and: [{ invoice_uid: invoiceid }, { invoice_uid: { $exists: true } }] }
            } else {
                data_query = { $and: [{ 'client.client_uid': req.decoded.customer_uid }, { invoice_uid: invoiceid }, { invoice_uid: { $exists: true } }] }
            }
            let invoicedetails = await invoiceModel.aggregate([{ $match: data_query }, { $lookup: { from: "creditnotes", localField: "invoice_uid", foreignField: "invoice_uid", as: "creditnote" } }, { $lookup: { from: 'payments', localField: 'invoice_uid', foreignField: 'invoice_uid', as: 'payment' } }, { $project: { invoice_uid: 1, client: 1, products: 1, workstatus_date: 1, createdAt: 1, estimated_arrival_date: 1, payment_due_date: 1, shipped_at_port: 1, shipped_onboard_date: 1, total_products: { $size: "$products" }, total_price: 1, status: 1, admin_document: 1, client_document: 1, shipped_info: 1, gross_price: 1, insurance: 1, insurance_usd: 1, freight: 1, freight_usd: 1, air_freight: 1, total_price: 1, credit_price: 1, status: 1, mode_of_shipping: 1, place_of_receipt: 1, port_of_loading: 1, port_of_discharge: 1, test_of_goods_info: 1, "creditnoteTotal": { "$sum": { "$map": { "input": "$creditnote", "as": "credit", "in": { "$cond": [{ "$eq": ["$$credit.status", 'Approved'] }, "$$credit.credit_amount", 0] } } } }, "paymentTotal": { "$sum": { "$map": { "input": "$payment", "as": "payment", "in": "$$payment.amount_received" } } } } }, { "$project": { invoice_uid: 1, client: 1, products: 1, workstatus_date: 1, createdAt: 1, estimated_arrival_date: 1, payment_due_date: 1, shipped_at_port: 1, shipped_onboard_date: 1, total_products: { $size: "$products" }, total_price: 1, status: 1, admin_document: 1, client_document: 1, shipped_info: 1, gross_price: 1, insurance: 1, insurance_usd: 1, freight: 1, freight_usd: 1, air_freight: 1, "total_invoice_amount": round("$total_price", 2), credit_price: 1, status: 1, mode_of_shipping: 1, place_of_receipt: 1, port_of_loading: 1, port_of_discharge: 1, test_of_goods_info: 1, "total_amount_paid": round({ "$sum": ["$credit_price", "$paymentTotal"] }, 2), "total_creditnote_amount": round("$creditnoteTotal", 2), "total_amount_due": round({ "$subtract": [{ "$subtract": ["$total_price", "$creditnoteTotal"] }, { "$sum": ["$credit_price", "$paymentTotal"] }] }, 2) } }]);
            resolve(invoicedetails)
        } catch (err) {
            reject(err)
        }
    })
}

exports.uploadsigned_creditnote = ({ invoiceid, body, files, signedcreditnote_doc }) => {
    return new Promise(async (resolve, reject) => {
        try {
            body = JSON.parse(body.data);
            let rejection_files = [];
            let signedcreditnote_info = {
                'credit_note_uid': body.credit_note_uid,
                'doc_name': signedcreditnote_doc
            }
            if (files) {
                for (const file of files) {
                    rejection_files.push({ file_name: file.filename })
                }
            }
            let invoice = await invoiceModel.findOneAndUpdate({ $and: [{ invoice_uid: invoiceid }, { invoice_uid: { $exists: true } }, { 'products.product_id': body.product_id }] }, { $push: { test_of_goods_info: { product_id: body.product_id, client_product_code: body.client_product_code, batch_number: body.batch_number, status: 'Rejected', rejection_files: rejection_files, signed_creditnote_info: signedcreditnote_info } } }, { new: true });
            if (!invoice || invoice == null) {
                throw new Error('Invoice Product does not exists.');
            } else {
                await CreditNoteModel.findOneAndUpdate({ $and: [{ invoice_uid: invoiceid }, { credit_note_uid: body.credit_note_uid }, { product_uid: body.product_id }] }, { $set: { status: 'Approved', signed_creditnote_doc: signedcreditnote_doc } });
                resolve(invoice)
            }
        } catch (err) {
            reject(err)
        }
    })
}

exports.getsigned_creditnotes = ({ invoiceid }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let signed_creditnote_doc = [], totalcreditamount = 0;
            let signed_creditnotes = await invoiceModel.findOne({ $and: [{ invoice_uid: invoiceid }, { invoice_uid: { $exists: true } }] }, { total_price: 1, 'test_of_goods_info.signed_creditnote_info': 1 }).sort({ "createdAt": -1 });
            if (signed_creditnotes.test_of_goods_info.length > 0) {
                async.each(signed_creditnotes.test_of_goods_info, function (creditnote, callback) {
                    if (creditnote.signed_creditnote_info) {
                        signed_creditnote_doc.push({ credit_note_uid: creditnote.signed_creditnote_info.credit_note_uid, signed_creditnote_doc: creditnote.signed_creditnote_info.doc_name })
                    }
                    callback ? callback(null) : null;
                }, async function (err) {
                    if (!err) {
                        let total_creditamount = await CreditNoteModel.aggregate([{ $match: { $and: [{ invoice_uid: invoiceid }, { "status": "Approved" }] } }, { "$group": { "_id": "$invoice_uid", "credit_amount": { "$sum": "$credit_amount" } } }]);
                        if (total_creditamount && total_creditamount[0]) {
                            totalcreditamount = signed_creditnotes.total_price - total_creditamount[0].credit_amount;
                        }
                        resolve({ signed_creditnote_doc: signed_creditnote_doc, total_invoice_amount: signed_creditnotes.total_price, updated_invoice_amount: totalcreditamount })
                    } else {
                        reject(err)
                    }
                })
            } else {
                resolve({ signed_creditnote_doc: signed_creditnote_doc, total_invoice_amount: signed_creditnotes.total_price, updated_invoice_amount: signed_creditnotes.total_price })
            }
        } catch (err) {
            reject(err)
        }
    })
}

exports.getmanagepayments = ({ invoiceid, req }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let data_query = {};
            if (req.decoded.userType == 'Admin') {
                data_query = { $and: [{ invoice_uid: invoiceid }, { invoice_uid: { $exists: true } }] }
            } else {
                data_query = { $and: [{ 'client.client_uid': req.decoded.customer_uid }, { invoice_uid: invoiceid }, { invoice_uid: { $exists: true } }] }
            }
            let invoice = await invoiceModel.aggregate([{ $match: data_query }, { $lookup: { from: "creditnotes", localField: "invoice_uid", foreignField: "invoice_uid", as: "creditnote" } }, { $lookup: { from: 'payments', localField: 'invoice_uid', foreignField: 'invoice_uid', as: 'payment' } }, { $project: { invoice_uid: 1, payment_due_date: 1, total_price: 1, credit_price: 1, "creditnoteTotal": { "$sum": { "$map": { "input": "$creditnote", "as": "credit", "in": { "$cond": [{ "$eq": ["$$credit.status", 'Approved'] }, "$$credit.credit_amount", 0] } } } }, "paymentTotal": { "$sum": { "$map": { "input": "$payment", "as": "payment", "in": "$$payment.amount_received" } } } } }, { "$project": { invoice_uid: 1, total_price: 1, credit_price: 1, due_date_of_payment: round("$payment_due_date", 2), "total_invoice_amount": round("$total_price", 2), "total_creditamount": round("$creditnoteTotal", 2), "total_amount_due": round({ "$subtract": [{ "$subtract": ["$total_price", "$creditnoteTotal"] }, { "$sum": ["$credit_price", "$paymentTotal"] }] }, 2), "totalamount_received": round({ "$sum": ["$credit_price", "$paymentTotal"] }, 2), "pending_amount": round({ "$subtract": [{ "$subtract": ["$total_price", "$creditnoteTotal"] }, { "$sum": ["$credit_price", "$paymentTotal"] }] }, 2) } }]);
            resolve(invoice)
        } catch (err) {
            reject(err)
        }
    })
}

exports.update_invoicestatus = ({ invoiceid }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let invoice = await invoiceModel.findOneAndUpdate({ $and: [{ invoice_uid: invoiceid }, { invoice_uid: { $exists: true } }] }, { $set: { status: 'CloseInvoice', closeinvoice_date: new Date() } }, { fields: { "invoice_uid": 1, "total_price": 1, "status": 1 }, new: true });
            if (!invoice || invoice == null) {
                throw new Error('Invoice does not exists.');
            } else {
                resolve(invoice)
            }
        } catch (err) {
            reject(err)
        }
    })
}

exports.update_invoice_to_openinvoice = ({ req, invoiceuid, body }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let coa_doc = [];
            let { shipped_onboard_date, estimated_arrival_date, material_delivery_date } = body;
            let artwork_uploaded_date, manufacture_uploaded_date, testing_uploaded_date, dispatch_uploaded_date;
            let invoice = await invoiceModel.findOne({ $and: [{ invoice_uid: invoiceuid }, { invoice_uid: { $exists: true } }] }, { products: 1, client: 1 });
            if (invoice && invoice.products.length > 0) {
                async.each(invoice.products, async (product, cb) => {
                    if (material_delivery_date != "") {
                        await piModel.findOneAndUpdate({ pi_uid: product.pi_uid, "products": { "$elemMatch": { "product_uid": product.product_id, "batches._id": ObjectId(product.batch_id) } } }, { "$set": { "products.$[outer].batches.$[inner].shipped.status": true, "products.$[outer].batches.$[inner].shipped.uploaded_date": new Date(), "products.$[outer].batches.$[inner].shipped.uploaded_by": req.decoded.id, "products.$[outer].batches.$[inner].workstatus": "Shipped", "products.$[outer].batches.$[inner].greenTick": 6, "products.$[outer].batches.$[inner].redTick": 1, "products.$[outer].batches.$[inner].shipped.shipped_onboard_date": shipped_onboard_date } }, { "arrayFilters": [{ "outer.product_uid": product.product_id }, { "inner._id": ObjectId(product.batch_id) }] })
                    } else {
                        await piModel.findOneAndUpdate({ pi_uid: product.pi_uid, "products": { "$elemMatch": { "product_uid": product.product_id, "batches._id": ObjectId(product.batch_id) } } }, { "$set": { "products.$[outer].batches.$[inner].shipped.shipped_onboard_date": shipped_onboard_date } }, { "arrayFilters": [{ "outer.product_uid": product.product_id }, { "inner._id": ObjectId(product.batch_id) }] })
                    }
                    let pidata = await piModel.aggregate([{ $match: { $and: [{ pi_uid: product.pi_uid }, { 'products.product_uid': product.product_id }] } }, { $project: { products: { $filter: { input: { "$map": { "input": "$products", "as": "product", "in": { "batches": { "$filter": { "input": "$$product.batches", "as": "batch", cond: { $and: [{ "$eq": ["$$batch._id", ObjectId(product.batch_id)] }] } } } } } }, "as": "product", "cond": { "$gt": [{ "$size": "$$product.batches" }, 0] } } } } }])
                    for (let i of pidata[0].products[0].batches) {
                        if (i.testing.CoA_doc && coa_doc.indexOf(i.testing.CoA_doc) == -1) {
                            coa_doc.push(i.testing.CoA_doc)
                        }
                    }
                    if (pidata && pidata[0] && pidata[0].products[0] && pidata[0].products[0].batches[0]) {
                        artwork_uploaded_date = pidata[0].products[0].batches[0].artwork.uploaded_date;
                        manufacture_uploaded_date = pidata[0].products[0].batches[0].manufacture.uploaded_date;
                        testing_uploaded_date = pidata[0].products[0].batches[0].testing.uploaded_date;
                        dispatch_uploaded_date = pidata[0].products[0].batches[0].dispatch.uploaded_date;
                    }
                    cb ? cb(null) : null
                }, async (err) => {
                    if (err) {
                        reject(err);
                    } else {
                        let customers = await customerModel.findOne({ $and: [{ customer_uid: invoice.client.client_uid }, { status: 'Active' }] }, { _id: 0, "additional_details.terms_of_payment_days": 1 })
                        let payment_due_date = moment(shipped_onboard_date).add(customers.additional_details.terms_of_payment_days, 'days').format("YYYY-MM-DD")
                        await invoiceModel.findOneAndUpdate({ $and: [{ invoice_uid: invoiceuid }, { invoice_uid: { $exists: true } }] }, { $set: { "status": "OpenInvoice", 'admin_document.CoA_doc': coa_doc, shipped_onboard_date: shipped_onboard_date, estimated_arrival_date: estimated_arrival_date, payment_due_date: payment_due_date, material_delivery_date: material_delivery_date, 'workstatus_date.artwork_uploaded_date': artwork_uploaded_date, 'workstatus_date.manufacture_uploaded_date': manufacture_uploaded_date, 'workstatus_date.testing_uploaded_date': testing_uploaded_date, 'workstatus_date.dispatch_uploaded_date': dispatch_uploaded_date, 'workstatus_date.shipped_uploaded_date': new Date() } }, { new: true })
                        resolve();
                    }
                })
            } else {
                throw new Error('Invoice does not exists.');
            }
        } catch (err) {
            reject(err);
        }
    });
}

exports.add_material_delivery_date = ({ req, invoiceuid, body }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let { material_delivery_date } = body;
            let invoice = await invoiceModel.findOne({ $and: [{ invoice_uid: invoiceuid }, { invoice_uid: { $exists: true } }] }, { products: 1 });
            if (invoice && invoice.products.length > 0) {
                async.each(invoice.products, async (product, cb) => {
                    await piModel.findOneAndUpdate({ pi_uid: product.pi_uid, "products": { "$elemMatch": { "product_uid": product.product_id, "batches._id": ObjectId(product.batch_id) } } }, { "$set": { "products.$[outer].batches.$[inner].shipped.status": true, "products.$[outer].batches.$[inner].shipped.uploaded_date": new Date(), "products.$[outer].batches.$[inner].shipped.uploaded_by": req.decoded.id, "products.$[outer].batches.$[inner].workstatus": "Shipped", "products.$[outer].batches.$[inner].greenTick": 6, "products.$[outer].batches.$[inner].redTick": 1 } }, { "arrayFilters": [{ "outer.product_uid": product.product_id }, { "inner._id": ObjectId(product.batch_id) }] })
                    cb ? cb(null) : null
                }, async (err) => {
                    if (err) {
                        throw new Error('Internal error occurred');
                    } else {
                        await invoiceModel.findOneAndUpdate({ $and: [{ invoice_uid: invoiceuid }, { invoice_uid: { $exists: true } }] }, { $set: { material_delivery_date: material_delivery_date, 'workstatus_date.shipped_uploaded_date': new Date() } }, { new: true })
                        resolve();
                    }
                })
            } else {
                throw new Error('Invoice does not exists.');
            }
        } catch (err) {
            reject(err);
        }
    });
}


exports.delete_invoice = ({ invoiceuid }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let invoices = await invoiceModel.findOne({ $and: [{ invoice_uid: { $exists: true } }, { invoice_uid: invoiceuid }, { status: 'CloseInvoice' }] });
            if (invoices) {
                throw new Error("Invoice con't be removed, because invoice is already closed.");
            } else {
                let payments = await paymentModel.find({ invoice_uid: invoiceuid });
                let creditnotes = await CreditNoteModel.find({ invoice_uid: invoiceuid });
                if (payments.length > 0 || creditnotes.length > 0) {
                    throw new Error("Invoice con't be removed, because invoice is already in use.");
                } else {
                    let invoice = await invoiceModel.findOneAndUpdate({ $and: [{ invoice_uid: invoiceuid }, { invoice_uid: { $exists: true } }] }, { $unset: { 'invoice_number': "", 'invoice_uid': "", "invoice_date": "", "status": "Pending", 'admin_document.CoA_doc': '', shipped_onboard_date: '', estimated_arrival_date: '', payment_due_date: '', material_delivery_date: '', 'workstatus_date.artwork_uploaded_date': '', 'workstatus_date.manufacture_uploaded_date': '', 'workstatus_date.testing_uploaded_date': '', 'workstatus_date.dispatch_uploaded_date': '', 'workstatus_date.shipped_uploaded_date': '' }, $set: { 'gross_price': 0, 'insurance': 0, 'insurance_usd': 0, 'freight': 0, 'freight_usd': 0, "air_freight": 0, 'total_price': 0, 'credit_price': 0 } }, { new: true });
                    if (!invoice) {
                        throw new Error('Invoice does not exists.');
                    } else {
                        async.each(invoice.products, async (product, cb) => {
                            await piModel.findOneAndUpdate({ pi_uid: product.pi_uid, "products": { "$elemMatch": { "product_uid": product.product_id, "batches._id": ObjectId(product.batch_id) } } }, { "$set": { "products.$[outer].batches.$[inner].dispatch.status": false, "products.$[outer].batches.$[inner].workstatus": "Testing", "products.$[outer].batches.$[inner].greenTick": 4, "products.$[outer].batches.$[inner].redTick": 3, "products.$[outer].batches.$[inner].shipped.status": false }, $unset: { "products.$[outer].batches.$[inner].dispatch.uploaded_date": '', "products.$[outer].batches.$[inner].dispatch.uploaded_by": '', "products.$[outer].batches.$[inner].shipped.uploaded_date": '', "products.$[outer].batches.$[inner].shipped.uploaded_by": '' } }, { "arrayFilters": [{ "outer.product_uid": product.product_id }, { "inner._id": ObjectId(product.batch_id) }] })
                            cb ? cb(null) : null
                        }, async (err) => {
                            if (err) {
                                throw new Error('Internal error occurred');
                            } else {
                                resolve();
                            }
                        })
                    }
                }
            }
        } catch (err) {
            reject(err)
        }
    })
}

exports.post_shipping_order_report = ({ customerUid }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let results = [];
            let data_query = { $and: [{ invoice_uid: { $exists: true } }, { 'client.client_uid': customerUid }, { $or: [{ status: 'OpenInvoice' }, { status: 'CloseInvoice' }] }] };
            let productsdata = await invoiceModel.aggregate([{
                $match: data_query
            }, {
                $unwind: {
                    path: "$products", preserveNullAndEmptyArrays: true
                }
            }, {
                $project: {
                    po_number: "$products.client_po_number", pi_number: "$products.pi_uid", client: 1, pi_date: "$products.pi_uid_date", invoice_no: "$invoice_uid", dispatch_date: "$workstatus_date.dispatch_uploaded_date", no_of_box: "$products.number_of_packages", description_of_goods: { $concat: ["$products.customerCodeNumber", "-", "$products.brand_name"] },
                    quantity: "$products.quantity", unit: "$products.unit_of_billing", rate: "$products.price", amount: "$products.amount", container_number: "$shipping.container_number", shipped_onboard_date: "$shipped_onboard_date", document_sent_for_approval_on: "$admin_document.send_to_client_date", tracking_number: "$shipped_info.tracking_number", document_sent_on: "$shipped_info.date_of_courier", estimated_delivery_date: "$material_delivery_date", delivery_no_of_days: { $divide: [{ $subtract: ["$material_delivery_date", "$shipped_onboard_date"] }, 1000 * 60 * 60 * 24] }
                }
            }])
            if (productsdata.length > 0) {
                resolve({ success: true, data: productsdata, message: 'Client products returned Successfully' })
            } else {
                resolve({ success: false, data: productsdata, message: 'Client products not returned' })
            }
        } catch (err) {
            reject(err);
        }
    });
}


exports.coaData = ({ invoiceuid }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let docArray = []
            let invoice = await invoiceModel.findOne({ $and: [{ invoice_uid: invoiceuid }, { invoice_uid: { $exists: true } }] }, { products: 1, client: 1 });
            if (invoice && invoice.products.length > 0) {
                async.each(invoice.products, async (product, cb) => {
                    let piData = await piModel.aggregate([{
                        $match: {
                            $and: [{ pi_uid: product.pi_uid },
                            { 'products.product_uid': product.product_id }]
                        }
                    },
                    {
                        $project: {
                            client: 1, status: 1,
                            products: {
                                $filter: {
                                    input: {
                                        "$map": {
                                            "input": "$products", "as": "product",
                                            "in": {
                                                "batches": {
                                                    "$filter": {
                                                        "input": "$$product.batches",
                                                        "as": "batch", cond: {
                                                            $and: [
                                                                { "$eq": ["$$batch._id", ObjectId(product.batch_id)] }
                                                            ]
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    },
                                    "as": "product", "cond": { "$gt": [{ "$size": "$$product.batches" }, 0] }
                                }
                            }
                        }
                    }]);
                    for (let i of piData[0].products[0].batches) {
                        if (i.testing.CoA_doc)
                            docArray.push(i.testing.CoA_doc)
                    }
                    cb ? cb(null) : null
                }, async (err) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(docArray);
                    }
                })
            } else {
                throw new Error('Invoice does not exists.');
            }
        } catch (err) {
            reject(err);
        }
    });
}




function invoiceQuery(query, req) {
    return new Promise(async (resolve, reject) => {
        try {
            let { invoicelisttype } = query; let querydata; let data_query = {};
            //let { invoicelisttype, pageNo, pageSize } = query; let querydata; let data_query = {};
            // let _pageNo = parseInt(isNaN(pageNo) ? 1 : pageNo);
            // let _pageSize = parseInt(isNaN(pageSize) ? 50 : pageSize);
            // let skip = parseInt((_pageNo - 1) * _pageSize);
            if (invoicelisttype && invoicelisttype != '' && invoicelisttype == 'invoicelistcount') {
                if (req.decoded.userType == 'Admin') {
                    data_query = { $or: [{ status: 'OpenInvoice' }, { status: 'CloseInvoice' }] };
                } else {
                    data_query = { $and: [{ 'client.client_uid': req.decoded.customer_uid }, { $or: [{ status: 'OpenInvoice' }, { status: 'CloseInvoice' }] }] };
                }
                querydata = [{ $match: data_query }, { "$group": { "_id": 0, "openinvoice_count": { "$sum": { "$cond": [{ "$eq": ["$status", "OpenInvoice"] }, 1, 0] } }, "closeinvoice_count": { "$sum": { "$cond": [{ "$eq": ["$status", "CloseInvoice"] }, 1, 0] } } } }, { "$project": { "_id": 0, "OpenInvoice": "$openinvoice_count", "CloseInvoice": "$closeinvoice_count" } }]
            } else if (invoicelisttype && invoicelisttype != '' && invoicelisttype == 'openinvoicelist') {
                if (req.decoded.userType == 'Admin') {
                    data_query = { status: 'OpenInvoice' };
                } else {
                    data_query = { $and: [{ 'client.client_uid': req.decoded.customer_uid }, { status: 'OpenInvoice' }] };
                }
                querydata = [{ $match: data_query }, { $lookup: { from: "creditnotes", localField: "invoice_uid", foreignField: "invoice_uid", as: "creditnote" } }, { $lookup: { from: 'payments', localField: 'invoice_uid', foreignField: 'invoice_uid', as: 'payment' } }, { $project: { invoice_uid: 1, client: 1, estimated_arrival_date: 1, payment_due_date: 1, total_products: { $size: "$products" }, total_price: 1, credit_price: 1, status: 1, "creditnoteTotal": { "$sum": { "$map": { "input": "$creditnote", "as": "credit", "in": { "$cond": [{ "$eq": ["$$credit.status", 'Approved'] }, "$$credit.credit_amount", 0] } } } }, "paymentTotal": { "$sum": { "$map": { "input": "$payment", "as": "payment", "in": "$$payment.amount_received" } } } } }, { "$project": { invoice_uid: 1, client: 1, estimated_arrival_date: 1, payment_due_date: 1, total_products: 1, total_price: 1, credit_price: 1, status: 1, creditnoteTotal: round("$creditnoteTotal", 2), "total_invoice_amount": round("$total_price", 2), "paid_amount": round({ "$sum": ["$credit_price", "$paymentTotal"] }, 2), "total_creditnote_amount": round("$creditnoteTotal", 2), "total_amount_due": round({ "$subtract": [{ "$subtract": ["$total_price", "$creditnoteTotal"] }, { "$sum": ["$credit_price", "$paymentTotal"] }] }, 2) } }, { $sort: { "createdAt": -1 } }]
            } else if (invoicelisttype && invoicelisttype != '' && invoicelisttype == 'closeinvoicelist') {
                if (req.decoded.userType == 'Admin') {
                    data_query = { status: 'CloseInvoice' };
                } else {
                    data_query = { $and: [{ 'client.client_uid': req.decoded.customer_uid }, { status: 'CloseInvoice' }] };
                }
                querydata = [{ $match: data_query }, { $lookup: { from: "creditnotes", localField: "invoice_uid", foreignField: "invoice_uid", as: "creditnote" } }, { $lookup: { from: 'payments', localField: 'invoice_uid', foreignField: 'invoice_uid', as: 'payment' } }, { $project: { invoice_uid: 1, client: 1, estimated_arrival_date: 1, payment_due_date: 1, total_products: { $size: "$products" }, total_price: 1, credit_price: 1, status: 1, "creditnoteTotal": { "$sum": { "$map": { "input": "$creditnote", "as": "credit", "in": { "$cond": [{ "$eq": ["$$credit.status", 'Approved'] }, "$$credit.credit_amount", 0] } } } }, "paymentTotal": { "$sum": { "$map": { "input": "$payment", "as": "payment", "in": "$$payment.amount_received" } } } } }, { "$project": { invoice_uid: 1, client: 1, estimated_arrival_date: 1, payment_due_date: 1, total_products: 1, total_price: 1, credit_price: 1, status: 1, creditnoteTotal: round("$creditnoteTotal", 2), "total_invoice_amount": round("$total_price", 2), "paid_amount": round({ "$sum": ["$credit_price", "$paymentTotal"] }, 2), "total_creditnote_amount": round("$creditnoteTotal", 2), "total_amount_due": round({ "$subtract": [{ "$subtract": ["$total_price", "$creditnoteTotal"] }, { "$sum": ["$credit_price", "$paymentTotal"] }] }, 2) } }, { $sort: { "createdAt": -1 } }]
            } else {
                querydata = [{ $match: { $or: [{ status: 'Pending' }, { status: 'Approved' }] } }, { $project: { client_po_number: 1, pi_uid: 1, client: 1, date_of_issue_of_po: 1, total_products: { $size: "$products" }, status: 1 } }, { $sort: { "createdAt": -1 } }]
            }
            resolve(querydata);
        } catch (err) {
            reject(err);
        }
    })
}

function generateandsentinvoicedoc(res, invoice_uid) {
    return new Promise(async (resolve, reject) => {
        try {
            let invoicedata = await invoiceModel.findOne({ invoice_uid: invoice_uid })
            console.log("========>>>>>>>", invoicedata)
            if (!invoicedata || invoicedata == null) {
                throw new Error('invoice does not exists.');
            }
            let customerdata = await customerModel.findOne({ customer_uid: invoicedata.client.client_uid })
            if (!customerdata || customerdata == null) {
                throw new Error('customer does not exists.');
            }
            let dir = `${path.dirname(process.mainModule.filename)}/upload/invoice`
            let invoice_document = `invoice_${invoicedata.invoice_uid}_${new Date().getTime()}.pdf`
            let invoice = `${dir}/${invoice_document}`;
            let total_price = await inWords((invoicedata.total_price).toFixed(2));
            let total_val = await generateinvoiceproducts(invoicedata);
            let supplierdata = await supplierModel.find({ _id: { $in: invoicedata.supplier_id } })
            await invoiceModel.findOneAndUpdate({ $and: [{ invoice_uid: invoice_uid }, { invoice_uid: { $exists: true } }] }, { $set: { 'admin_document.productinvoice_doc': invoice_document } }, { new: true })
            res.render('invoice', { invoice: invoicedata, customerdata: customerdata, total_price: total_price, total_val: total_val, moment: moment, supplier: supplierdata }, async (err, html) => {
                if (err) {
                    throw new Error('html is not generated.');
                } else {
                    PDFConverter({ html: html, printDelay: 0, fitToPage: false, allowLocalFilesAccess: true }, async (err, pdf) => {
                        if (err) {
                            throw new Error('pdf is not generated.');
                        } else {
                            if (!fs.existsSync(dir)) {
                                fs.mkdirSync(dir);
                            }
                            let stream = fs.createWriteStream(invoice);
                            pdf.stream.pipe(stream);
                            attachments = [{
                                filename: invoice_document,
                                contentType: 'application/pdf',
                                path: `${path.dirname(process.mainModule.filename)}/upload/invoice/${invoice_document}`
                            }]
                            await Mailer.send(process.env.ADMINEMAIL, 'Invoice', `Please find the below attachment for ${invoicedata.invoice_uid}`, attachments);
                            resolve(invoicedata)
                        }
                    })
                }
            })
        } catch (err) {
            reject(err)
        }
    })
}

async function generateinvoiceproducts(invoice) {
    return new Promise(async (resolve, reject) => {
        try {
            let supplierorderno = []; let supplierorderdate = []; let buyerorderno = []; let buyerorderdate = []; let total_quantity = 0; let units = []; let products = []; let cartonsNo = 1; let total_carton = 0;
            async.each(invoice.products, function (product, callback) {
                if (supplierorderno.indexOf(product.pi_uid) < 0) {
                    supplierorderno.push(product.pi_uid);
                    supplierorderdate.push(moment(product.pi_uid_date).format('DD/MM/YYYY'));
                }
                if (buyerorderno.indexOf(product.client_po_number) < 0) {
                    buyerorderno.push(product.client_po_number);
                    buyerorderdate.push(moment(product.date_of_issue_of_po).format('DD/MM/YYYY'));
                }
                if (units.indexOf(product.unit_of_billing) < 0) {
                    units.push(product.unit_of_billing);
                }
                total_quantity += product.quantity;
                let no_of_pkgs = '';
                total_carton += product.number_of_packages;
                no_of_pkgs = no_of_pkgs.concat(cartonsNo).concat(' To ').concat(total_carton);
                cartonsNo += product.number_of_packages;
                products.push({ total_packing_carton: product.number_of_packages, no_of_pkgs: no_of_pkgs, complete_packaging_profile: product.complete_packaging_profile, client_product_code: product.client_product_code, brand_name: product.brand_name, batch_number: product.batch_number, manufacturing_date: product.manufacturing_date, expiry_date: product.expiry_date, hsn_code: product.hsn_code, quantity: product.quantity, unit_of_billing: product.unit_of_billing, price: product.price, amount: product.amount })
                callback ? callback(null) : null;
            }, async function (err) {
                if (!err) {
                    resolve({ products: products, supplierorderno: supplierorderno.join(), supplierorderdate: supplierorderdate.join(), units: units.join('/'), buyerorderno: buyerorderno.join(), buyerorderdate: buyerorderdate.join(), total_quantity: total_quantity })
                } else {
                    reject(err)
                }
            }
            );
            resolve(invoice)
        } catch (err) {
            reject(err)
        }
    })
}

async function inWords(num) {
    var a = ['', 'One ', 'Two ', 'Three ', 'Four ', 'Five ', 'Six ', 'Seven ', 'Eight ', 'Nine ', 'Ten ', 'Eleven ', 'Twelve ', 'Thirteen ', 'Fourteen ', 'Fifteen ', 'Sixteen ', 'Seventeen ', 'Sighteen ', 'Nineteen '];
    var b = ['', '', 'Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety'];
    if ((num = num.toString()).length > 9) return 'overflow';
    var numarray = num.toString().split('.');
    num = numarray[0];
    dot = numarray[1];
    n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
    d = ('000' + dot).substr(-2).match(/^(\d{2})$/);
    if (!n) return; var str = '';
    str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'crore ' : '';
    str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'lakh ' : '';
    str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'thousand ' : '';
    str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'hundred ' : '';
    str += (n[5] != 0) ? (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) : '';
    str += (d[1] != 0) ? ((d[1] != '') ? 'And ' : '') + (a[Number(d[1])] || b[d[1][0]] + ' ' + a[d[1][1]]) + 'Cent Only ' : 'Only';
    return str;
}