const generateuid = require('../services/generateuid')
const packinglistModel = require('../models/packinglist');
const portofloadingModel = require('../models/portofloading');
const placeofreceiptModel = require('../models/placeofreceipt');
const piModel = require('../models/pi');
const PDFConverter = require("phantom-html-to-pdf")({});
const customerModel = require('../models/customer')
const CreditNoteModel = require('../models/creditnote');
const paymentModel = require('../models/payment');
const Mailer = require('../services/mailer')
var moment = require('moment');
const fs = require('fs');
const path = require('path');
const async = require("async");
var mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;
const supplierModel = require('../models/supplier');


exports.create_packinglist = ({ res, body }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let packing_list_data = await packinglistModel.find().sort('-packing_list_number').limit(1).exec();
            let { packing_list_uid, packing_list_number } = await generateuid.generateuidtypes({ uiddata: packing_list_data, uidtype: 'packinglist' })
            let productlist = [];
            if (body.products.length > 0) {
                async.eachSeries(body.products, function (product, callback) {
                    if (!product.product_id && !product.batch_id && !product.pi_uid) {
                        callback ? callback(`${!product.pi_uid ? 'pi_uid' : !product.product_id ? 'product_uid' : !product.batch_id ? 'batch_id' : ''} is required!.`) : null;
                    } else {
                        let price = parseFloat(product.price);
                        if (product.flatDiscount) {
                            price = price - parseFloat(product.flatDiscount)
                        }
                        let amount = price * parseFloat(product.quantity);
                        let expiry_date = new Date(product.manufacturing_date);
                        if (product.shelf_life.includes("Months")) {
                            let shelf_life = parseFloat(product.shelf_life.split(' ')[0])
                            expiry_date.setMonth(expiry_date.getMonth() + (shelf_life - 1))
                            expiry_date = expiry_date.toString('dd-MM-yyyy');
                        }

                        let productobj = {
                            product_id: product.product_id, batch_id: product.batch_id, client_product_code: product.client_product_code, brand_name: product.brand_name, secondary_carton_packing: product.secondary_carton_packing, customerCodeNumber: product.customerCodeNumber, hsn_code: product.hsn_code, shipper_size_inner_dimension: product.shipper_size_inner_dimension, batch_number: product.batch_number, number_of_packages: product.number_of_packages, total_packing_carton: product.total_packing_carton, unit_of_billing: product.unit_of_billing, complete_packaging_profile: product.complete_packaging_profile, minimum_batch_size: product.minimum_batch_size,
                            maximum_batch_size: product.maximum_batch_size, registration_no: product.registration_no, registration_date: product.registration_date, shelf_life: product.shelf_life, expiry_date: expiry_date, frieght: (product.frieght != null) ? product.frieght : 0, insurance: (product.insurance != null) ? product.insurance : 0, quantity: product.quantity, net_weight: product.net_weight, gross_weight: product.gross_weight, pi_uid: product.pi_uid, pi_uid_date: product.pi_uid_date, client_po_number: product.client_po_number, date_of_issue_of_po: product.date_of_issue_of_po, manufacturing_date: product.manufacturing_date, price: price, amount: amount
                        };
                        productlist.push(productobj);
                        callback ? callback(null) : null;
                    }
                },
                    async function (err) {
                        try {
                            if (!err) {
                                const packinglistObj = new packinglistModel({
                                    terms_of_delivery: body.terms_of_delivery,
                                    packing_list_uid: packing_list_uid,
                                    packing_list_number: packing_list_number,
                                    client: body.client,
                                    mode_of_shipping: body.mode_of_shipping,
                                    supplier_id: body.supplier_id,
                                    place_of_receipt: body.place_of_receipt,
                                    port_of_loading: body.port_of_loading,
                                    port_of_discharge: body.port_of_discharge,
                                    products: productlist,
                                    shipping: body.shipping,
                                    special_comments: body.special_comments
                                });
                                let packingList = await packinglistObj.save();
                                await generateandsentpackinglistdoc(res, packingList.packing_list_uid);
                                resolve(packingList)
                            } else {
                                throw new Error(err);
                            }
                        } catch (err) {
                            reject(err)
                        } finally {
                        }
                    }
                );
            } else {
                const packinglistObj = new packinglistModel({
                    packing_list_uid: packing_list_uid,
                    packing_list_number: packing_list_number,
                    client: body.client,
                    mode_of_shipping: body.mode_of_shipping,
                    supplier_id: body.supplier_id,
                    place_of_receipt: body.place_of_receipt,
                    port_of_loading: body.port_of_loading,
                    port_of_discharge: body.port_of_discharge,
                    products: productlist,
                    shipping: body.shipping,
                    special_comments: body.special_comments
                });
                let packingList = await packinglistObj.save();
                await generateandsentpackinglistdoc(res, packingList.packing_list_uid);
                resolve(packingList)
            }
        } catch (err) {
            reject(err)
        }
    })
}

exports.update_packinglist = ({ res, body, packinglistid }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let productlist = [];
            if (body.products.length > 0) {
                async.each(body.products, function (product, callback) {
                    if (!product.product_id && !product.batch_id && !product.pi_uid) {
                        callback ? callback(`${!product.pi_uid ? 'pi_uid' : !product.product_id ? 'product_uid' : !product.batch_id ? 'batch_id' : ''} is required!.`) : null;
                    } else {
                        let price = parseFloat(product.price);
                        if (product.flatDiscount) {
                            price = price - parseFloat(product.flatDiscount)
                        }
                        let amount = price * parseFloat(product.quantity);
                        let expiry_date = new Date(product.manufacturing_date);
                        if (product.shelf_life.includes("Months")) {
                            let shelf_life = parseFloat(product.shelf_life.split(' ')[0])
                            expiry_date.setMonth(expiry_date.getMonth() + (shelf_life - 1))
                            expiry_date = expiry_date.toString('dd-MM-yyyy');
                        }
                        let productobj = {
                            product_id: product.product_id, batch_id: product.batch_id, client_product_code: product.client_product_code, brand_name: product.brand_name, secondary_carton_packing: product.secondary_carton_packing, customerCodeNumber: product.customerCodeNumber, hsn_code: product.hsn_code, shipper_size_inner_dimension: product.shipper_size_inner_dimension, batch_number: product.batch_number, number_of_packages: product.number_of_packages, total_packing_carton: product.total_packing_carton, unit_of_billing: product.unit_of_billing, complete_packaging_profile: product.complete_packaging_profile, minimum_batch_size: product.minimum_batch_size,
                            maximum_batch_size: product.maximum_batch_size, registration_no: product.registration_no, registration_date: product.registration_date, shelf_life: product.shelf_life, expiry_date: expiry_date, frieght: (product.frieght != null) ? product.frieght : 0, insurance: (product.insurance != null) ? product.insurance : 0, quantity: product.quantity, net_weight: product.net_weight, gross_weight: product.gross_weight, pi_uid: product.pi_uid, pi_uid_date: product.pi_uid_date, client_po_number: product.client_po_number, date_of_issue_of_po: product.date_of_issue_of_po, manufacturing_date: product.manufacturing_date, price: price, amount: amount
                        };
                        productlist.push(productobj);
                        callback ? callback(null) : null;
                    }
                },
                    async function (err) {
                        try {
                            if (!err) {
                                const packinglistObj = {
                                    client: body.client,
                                    mode_of_shipping: body.mode_of_shipping,
                                    supplier_id: body.supplier_id,
                                    place_of_receipt: body.place_of_receipt,
                                    port_of_loading: body.port_of_loading,
                                    port_of_discharge: body.port_of_discharge,
                                    products: productlist,
                                    shipping: body.shipping,
                                    special_comments: body.special_comments
                                };
                                let packingList = await packinglistModel.findByIdAndUpdate(packinglistid, { $set: packinglistObj }, { new: true })
                                if (!packingList || packingList == null) {
                                    throw new Error('Packinglist does not exists.');
                                }
                                await generateandsentpackinglistdoc(res, packingList.packing_list_uid);
                                resolve(packingList)
                            } else {
                                throw new Error(err);
                            }
                        } catch (err) {
                            reject(err)
                        } finally {
                        }
                    }
                );
            } else {
                const packinglistObj = {
                    client: body.client,
                    mode_of_shipping: body.mode_of_shipping,
                    supplier_id: body.supplier_id,
                    place_of_receipt: body.place_of_receipt,
                    port_of_loading: body.port_of_loading,
                    port_of_discharge: body.port_of_discharge,
                    products: productlist,
                    shipping: body.shipping,
                    special_comments: body.special_comments
                };
                let packingList = await packinglistModel.findByIdAndUpdate(packinglistid, { $set: packinglistObj }, { new: true })
                await generateandsentpackinglistdoc(res, packingList.packing_list_uid);
                resolve(packingList)
            }
        } catch (err) {
            reject(err)
        }
    })
}

exports.get_packinglist = ({ query }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let { pageNo, pageSize, search } = query; let querydata = {};
            let _pageNo = parseInt(isNaN(pageNo) ? 1 : pageNo);
            let _pageSize = parseInt(isNaN(pageSize) ? 10 : pageSize);
            let skip = parseInt((_pageNo - 1) * _pageSize);
            if (search && search != '') {
                querydata = { $text: { $search: `\"${search}\"` } };
            }
            let _count = await packinglistModel.countDocuments(querydata);
            let packinglist = await packinglistModel.find(querydata).sort({ "createdAt": -1 }).skip(skip).limit(_pageSize);
            resolve({ success: true, data: packinglist, count: _count, message: 'Packing lists data fetched Successfully' })
        } catch (err) {
            reject(err)
        }
    })
}

exports.get_packinglist_id = ({ }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let packinglist = await packinglistModel.find({ invoice_uid: { $exists: false } }, { packing_list_uid: 1 });
            resolve(packinglist)
        } catch (err) {
            reject(err)
        }
    })
}

exports.create_portofloading = ({ body }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let portofloadingObj = new portofloadingModel({
                port_code: body.port_code ? body.port_code.toUpperCase() : body.port_code,
                port_name: body.port_name ? body.port_name.toUpperCase() : body.port_name,
                country_name: body.country_name ? body.country_name.toUpperCase() : body.country_name
            })
            let portofloading = await portofloadingObj.save();
            resolve(portofloading)
        } catch (err) {
            reject(err)
        }
    })
}

exports.getportofloadings = ({ }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let portofloadingList = await portofloadingModel.find({}, { _id: 0, port_code: 1, port_name: 1, country_name: 1 })
            resolve(portofloadingList)
        } catch (err) {
            reject(err)
        }
    })
}

exports.create_placeofreceipt = ({ body }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let placeofreceiptObj = new placeofreceiptModel({
                place_name: body.place_name ? body.place_name.toUpperCase() : body.place_name,
                country_name: body.country_name ? body.country_name.toUpperCase() : body.country_name
            })
            let placeofreceipt = await placeofreceiptObj.save();
            resolve(placeofreceipt)
        } catch (err) {
            reject(err)
        }
    })
}

exports.getplaceofreceipts = ({ }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let placeofreceiptList = await placeofreceiptModel.find({}, { _id: 0, place_name: 1, country_name: 1 })
            resolve(placeofreceiptList)
        } catch (err) {
            reject(err)
        }
    })
}

exports.getproductlists = ({ client_uid }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let productlists = [];
            let pis = await piModel.aggregate([{ $match: { $and: [{ 'client.client_uid': client_uid }, { status: 'OpenOrder' }] } }, { $project: { pi_uid: 1, createdAt: 1, client_po_number: 1, date_of_issue_of_po: 1, 'client.client_uid': 1, "products": { "$filter": { "input": { "$map": { "input": "$products", "as": "product", "in": { "_id": "$$product._id", "product_uid": "$$product.product_uid", "client_product_code": "$$product.client_product_code", "price": "$$product.price", "flatDiscount": "$$product.flatDiscount", "customerCodeNumber": "$$product.customerCodeNumber", "brand_name": "$$product.brand_name", "secondary_carton_packing": "$$product.secondary_carton_packing", "complete_packaging_profile": "$$product.complete_packaging_profile", "unit_of_billing": "$$product.unit_of_billing", "registration_no": "$$product.registration_no", "registration_date": "$$product.registration_date", "shelf_life": "$$product.shelf_life", "frieght": "$$product.frieght", "insurance": "$$product.insurance", "hsn_code": "$$product.hsn_code", "minimum_batch_size": "$$product.minimum_batch_size", "maximum_batch_size": "$$product.maximum_batch_size", "shipper_size_inner_dimension": "$$product.shipper_size_inner_dimension", "batches": { "$filter": { "input": "$$product.batches", "as": "batch", "cond": { $eq: ['$$batch.testing.status', true] } } } } } }, "as": "product", "cond": { "$gt": [{ "$size": "$$product.batches" }, 0] } } } } }])
            console.log('data :---->', pis);
            if (pis && pis.length > 0) {
                async.each(pis, function (pi, cb1) {
                    let piobj = { pi_uid: pi.pi_uid, pi_uid_date: pi.createdAt, client_po_number: pi.client_po_number, date_of_issue_of_po: pi.date_of_issue_of_po }
                    if (pi.products && pi.products.length > 0) {
                        let products = [];
                        async.each(pi.products, function (product, cb2) {
                            async.each(product.batches, async function (batch, cb3) {
                                let packinglist = await packinglistModel.find({ 'products': { '$elemMatch': { 'pi_uid': piobj.pi_uid, 'product_id': product.product_uid, 'batch_id': batch._id } } })
                                if ((packinglist.length === 0) || (packinglist.length > 0 && packinglist[0].products.length === 0)) {
                                    let productobj = {
                                        _id: product._id, product_uid: product.product_uid, batch_id: batch._id, brand_name: product.brand_name, secondary_carton_packing: product.secondary_carton_packing, client_product_code: product.client_product_code,
                                        price: product.price, flatDiscount: product.flatDiscount, batchNo: batch.batchNo, amount: batch.amount, quantity: batch.quantity, customerCodeNumber: product.customerCodeNumber,
                                        complete_packaging_profile: product.complete_packaging_profile, unit_of_billing: product.unit_of_billing, registration_no: product.registration_no,
                                        registration_date: product.registration_date, shelf_life: product.shelf_life, frieght: product.frieght, insurance: product.insurance, hsn_code: product.hsn_code,
                                        minimum_batch_size: product.minimum_batch_size, maximum_batch_size: product.maximum_batch_size, shipper_size_inner_dimension: product.shipper_size_inner_dimension
                                    }
                                    products.push(productobj)
                                }
                                cb3 ? cb3(null) : null;
                            }, function (err) {
                                cb2 ? cb2(null) : null;
                            })
                        }, function (err) {
                            if (!err) {
                                if (products.length > 0) {
                                    piobj = Object.assign({ products: products }, piobj);
                                    productlists.push(piobj);
                                }
                            }
                            cb1 ? cb1(null) : null;
                        })
                    } else {
                        cb1 ? cb1(null) : null;
                    }
                }, function (err) {
                    if (!err) {
                        resolve(productlists)
                    } else {
                        reject(err)
                    }
                })
            } else {
                resolve(productlists)
            }
        } catch (err) {
            reject(err)
        }
    })
}

exports.getOne_packinglist = ({ packingid }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let packingList = await packinglistModel.findById(packingid);
            let supplierdata = await supplierModel.find({ _id: { $in: packingList.supplier_id } }, { _id: 1, registered_name: 1 })
            const supplier_id = supplierdata.map(data => { return { value: data._id, label: data.registered_name } })
            packingList = JSON.parse(JSON.stringify(packingList));
            packingList = { ...packingList, supplier_id }
            resolve(packingList)
        } catch (err) {
            reject(err)
        }
    })
}

exports.getallpri_post_order = ({ }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let pri_orders = await piModel.aggregate([{ $match: { $or: [{ status: 'OpenOrder' }, { status: 'ShippedOrder' }] } }, { "$group": { "_id": 0, "openorder_count": { "$sum": { "$cond": [{ "$eq": ["$status", "OpenOrder"] }, 1, 0] } }, "shippedorder_count": { "$sum": { "$cond": [{ "$eq": ["$status", "ShippedOrder"] }, 1, 0] } } } }, { "$project": { "_id": 0, "total_priorder": { "$add": ["$openorder_count", "$shippedorder_count"] } } }]);
            let total_orders = { pri_order: pri_orders[0].total_priorder, post_order: 0 };
            if (!pri_orders || pri_orders == null) {
                throw new Error('Order does not exists.');
            }
            resolve(total_orders)
        } catch (err) {
            reject(err)
        }
    })
}

exports.delete_packinglist = ({ packinglistid }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let packinglist = await packinglistModel.findOne({ $and: [{ invoice_uid: { $exists: true } }, { _id: packinglistid }, { status: 'CloseInvoice' }] });
            if (packinglist) {
                throw new Error("Packinglist con't be removed, because invoice is already closed.");
            } else {
                let packingdata = await packinglistModel.findOne({ $and: [{ invoice_uid: { $exists: true } }, { _id: packinglistid }] });
                if (packingdata) {
                    let payments = await paymentModel.find({ invoice_uid: packingdata.invoice_uid });
                    let creditnotes = await CreditNoteModel.find({ invoice_uid: packingdata.invoice_uid });
                    if (payments.length > 0 || creditnotes.length > 0) {
                        throw new Error("Packinglist con't be removed, because Packinglist is already in use.");
                    } else {
                        let packinglistdata = await packinglistModel.deleteOne({ _id: packinglistid })
                        if (packinglistdata.deletedCount == 0) {
                            throw new Error("Packinglist is not removed");
                        } else {
                            resolve()
                        }
                    }
                } else {
                    let packinglistdata = await packinglistModel.deleteOne({ _id: packinglistid })
                    if (packinglistdata.deletedCount == 0) {
                        throw new Error("Packinglist is not removed");
                    } else {
                        resolve()
                    }
                }
            }
        } catch (err) {
            reject(err)
        }
    })
}


function generateandsentpackinglistdoc(res, packing_uid) {
    return new Promise(async (resolve, reject) => {
        try {
            let packinglistdata = await packinglistModel.findOne({ packing_list_uid: packing_uid })
            if (!packinglistdata || packinglistdata == null) {
                throw new Error('Packinglist does not exists.');
            }
            let customerdata = await customerModel.findOne({ customer_uid: packinglistdata.client.client_uid })
            if (!customerdata || customerdata == null) {
                throw new Error('customer does not exists.');
            }
            let dir = `${path.dirname(process.mainModule.filename)}/upload/packinglist`
            let packinglist_document = `packinglist_${packinglistdata.packing_list_uid}_${new Date().getTime()}.pdf`
            let invoice = `${dir}/${packinglist_document}`;
            let supplierdata = await supplierModel.find({ _id: { $in: packinglistdata.supplier_id } })
            let total_val = await generatepackinglistproducts(packinglistdata);
            await packinglistModel.findOneAndUpdate({ packing_list_uid: packing_uid }, { $set: { 'admin_document.packinglist_doc': packinglist_document } }, { new: true })
            res.render('packinglist', { packinglist: packinglistdata, customerdata: customerdata, total_val: total_val, moment: moment, supplier: supplierdata }, async (err, html) => {
                if (err) {
                    throw new Error('html is not generated.');
                } else {
                    console.log(html);  
                    PDFConverter({ html: html, printDelay: 0, fitToPage: false, allowLocalFilesAccess: true }, async (err, pdf) => {
                        if (err) {
                            throw new Error('pdf is not generated.');
                        } else {
                            if (!fs.existsSync(dir)) {
                                fs.mkdirSync(dir);
                            }
                            let stream = fs.createWriteStream(invoice);
                            pdf.stream.pipe(stream);
                            attachments = [{
                                filename: packinglist_document,
                                contentType: 'application/pdf',
                                path: `${path.dirname(process.mainModule.filename)}/upload/packinglist/${packinglist_document}`
                            }]
                            await Mailer.send(process.env.ADMINEMAIL, 'Packing list', `Please find the below attachment for ${packinglistdata.packing_list_uid}`, attachments);
                            resolve(packinglistdata)
                        }
                    })
                }
            })
        } catch (err) {
            reject(err)
        }
    })
}

async function generatepackinglistproducts(packinglist) {
    return new Promise(async (resolve, reject) => {
        try {
            let supplierorderno = []; let supplierorderdate = []; let buyerorderno = []; let buyerorderdate = []; let hsncodes = []; let total_tablets = 0; let total_quantity = 0; let total_net_weight = 0; let total_gross_weight = 0; let units = []; let products = []; let cartonsNo = 1; let total_carton = 0;
            async.each(packinglist.products, function (product, callback) {
                if (supplierorderno.indexOf(product.pi_uid) < 0) {
                    supplierorderno.push(product.pi_uid);
                    supplierorderdate.push(moment(product.pi_uid_date).format('DD/MM/YYYY'));
                }
                if (buyerorderno.indexOf(product.client_po_number) < 0) {
                    buyerorderno.push(product.client_po_number);
                    buyerorderdate.push(moment(product.date_of_issue_of_po).format('DD/MM/YYYY'));
                }
                if (units.indexOf(product.unit_of_billing) < 0) {
                    units.push(product.unit_of_billing);
                }
                if (hsncodes.indexOf(product.hsn_code) < 0) {
                    hsncodes.push(product.hsn_code);
                }
                total_quantity += product.quantity;
                total_net_weight += ((product.net_weight) * (product.number_of_packages));
                total_gross_weight += ((product.gross_weight) * (product.number_of_packages));
                let no_of_pkgs = '';
                total_carton += product.number_of_packages;
                no_of_pkgs = no_of_pkgs.concat(cartonsNo).concat(' To ').concat(total_carton);
                cartonsNo += product.number_of_packages;
                if (product.secondary_carton_packing) {
                    total_tablets = product.quantity * (product.secondary_carton_packing.split('*').reduce(function (mul, current) { return mul * current; }, 1))
                } else {
                    total_tablets = product.quantity * 1
                }
                products.push({ total_tablets: total_tablets, total_packing_carton: product.number_of_packages, no_of_pkgs: no_of_pkgs, complete_packaging_profile: product.complete_packaging_profile, client_product_code: product.client_product_code, brand_name: product.brand_name, batch_number: product.batch_number, shipper_size_inner_dimension: product.shipper_size_inner_dimension, expiry_date: product.expiry_date, hsn_code: product.hsn_code, quantity: product.quantity, unit_of_billing: product.unit_of_billing, price: product.price, amount: product.amount })
                callback ? callback(null) : null;
            }, async function (err) {
                if (!err) {
                    resolve({ products: products, total_carton: total_carton, supplierorderno: supplierorderno.join(), supplierorderdate: supplierorderdate.join(), units: units.join('/'), buyerorderno: buyerorderno.join(), buyerorderdate: buyerorderdate.join(), hsncodes: hsncodes.join(), total_quantity: total_quantity, total_net_weight: total_net_weight, total_gross_weight: total_gross_weight })
                } else {
                    reject(err)
                }
            });
            resolve(invoice)
        } catch (err) {
            reject(err)
        }
    })
}
