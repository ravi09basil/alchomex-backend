const paymentModel = require("../models/payment");
const generateuid = require("../services/generateuid");
const invoiceModel = require("../models/packinglist");

exports.create_payment = ({ body }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let payment_slip_data = await paymentModel
        .find({ payment_slip_UID: { $exists: true, $ne: null } })
        .sort("-payment_number")
        .limit(1)
        .exec();
      let {
        payment_slip_UID,
        payment_UID,
        payment_number
      } = await generateuid.generateuidtypes({
        uiddata: payment_slip_data,
        uidtype: "paymentslip"
      });
      const paymentObj = new paymentModel({
        payment_slip_UID: payment_slip_UID,
        payment_UID: payment_UID,
        payment_number: payment_number,
        client: body.client,
        invoice_uid: body.invoice_uid,
        amount_received: body.amount_received,
        received_date: body.received_date,
        UTR_number: body.UTR_number
      });
      let payment = await paymentObj.save();
      if (!payment || payment == null) {
        throw new Error("Payment not added...");
      } else {
        if (body.paymentComplete == true) {
          await invoiceModel.findOneAndUpdate(
            {
              $and: [
                { invoice_uid: body.invoice_uid },
                { invoice_uid: { $exists: true } }
              ]
            },
            { $set: { status: "CloseInvoice", closeinvoice_date: new Date() } }
          );
        }
        resolve(payment);
      }
    } catch (err) {
      reject(err);
    }
  });
};

exports.getallpayments = ({ query }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let { pageNo, pageSize, search } = query;
      let querydata = {};
      let _pageNo = parseInt(isNaN(pageNo) ? 1 : pageNo);
      let _pageSize = parseInt(isNaN(pageSize) ? 100 : pageSize);
      let skip = parseInt((_pageNo - 1) * _pageSize);
      let sort = { createdAt: -1 };
      if (search && search != "") {
        querydata = { $text: { $search: `\"${search}\"` } };
      }
      let _count = await paymentModel.countDocuments(querydata);
      let payments = await paymentModel
        .find(querydata)
        .sort(sort)
        .skip(skip)
        .limit(_pageSize);
      resolve({
        success: true,
        data: payments,
        count: _count,
        message: "Payment Slips fetched Successfully"
      });
    } catch (err) {
      reject(err);
    }
  });
};

exports.update_payment = ({ paymentid, body }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let data = {
        client: body.client,
        invoice_uid: body.invoice_uid,
        amount_received: body.amount_received,
        received_date: body.received_date,
        SWIFT_number: body.SWIFT_number,
        UTR_number: body.UTR_number
      };
      let payment = await paymentModel.findByIdAndUpdate(
        paymentid,
        { $set: data },
        { new: true }
      );
      if (!payment) {
        throw new Error("Payment does not exists.");
      } else {
        resolve(payment);
      }
    } catch (err) {
      reject(err);
    }
  });
};

exports.getpayment_details = ({ paymentid }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let paymentdetails = await paymentModel.findOne({
        payment_slip_UID: paymentid
      });
      resolve(paymentdetails);
    } catch (err) {
      reject(err);
    }
  });
};

exports.delete_paymentslip = ({ invoiceid, paymentslipid }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let invoices = await invoiceModel.findOne({
        $and: [
          { invoice_uid: { $exists: true } },
          { invoice_uid: invoiceid },
          { status: "CloseInvoice" }
        ]
      });
      if (invoices) {
        throw new Error(
          "Payment slip con't be removed, because invoice is already closed."
        );
      } else {
        let payment = await paymentModel.deleteOne({
          $and: [
            { invoice_uid: invoiceid },
            { payment_slip_UID: paymentslipid }
          ]
        });
        if (payment.deletedCount == 0) {
          throw new Error("Payment slip is not removed");
        } else {
          resolve();
        }
      }
    } catch (err) {
      reject(err);
    }
  });
};

exports.get_invoiceuid_payments = ({ invoiceuid }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let sort = { createdAt: 1 };
      let payments = await paymentModel
        .find({ invoice_uid: invoiceuid })
        .sort(sort);
      resolve({
        success: true,
        data: payments,
        message: "Invoice payment slips fetched successfully."
      });
    } catch (err) {
      reject(err);
    }
  });
};
