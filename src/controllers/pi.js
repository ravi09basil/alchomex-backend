const piModel = require("../models/pi");
const packinglistModel = require("../models/packinglist");
const customlogModel = require("../models/customlog");
const PDFConverter = require("phantom-html-to-pdf")({});
const customerModel = require("../models/customer");
const productModel = require("../models/product");
const Mailer = require("../services/mailer");
const generateuid = require("../services/generateuid");
var moment = require("moment");
const fs = require("fs");
const path = require("path");
const async = require("async");
var mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;

exports.create_proformainvoice = ({ res, body, file }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let data = JSON.parse(body.data);
      let pi_data = await piModel
        .find()
        .sort("-pi_number")
        .limit(1)
        .exec();
      let { pi_uid, pi_number } = await generateuid.generateuidtypes({
        uiddata: pi_data,
        uidtype: "pi"
      });
      let products = [];
      if (data.product.length > 0) {
        async.eachSeries(
          data.product,
          (product, callback) => {
            if (
              (product.minimum_batch_size == 0 &&
                product.maximum_batch_size == 0) ||
              (product.minimum_batch_size == 0 &&
                product.maximum_batch_size > 0 &&
                product.quantity < product.maximum_batch_size)
            ) {
              let batchdata = {
                quantity: product.quantity,
                amount: product.amount
              };
              product = Object.assign({ batches: batchdata }, product);
              products.push(product);
              callback ? callback(null) : null;
            } else {
              if (
                product.minimum_batch_size &&
                product.quantity >= product.minimum_batch_size &&
                product.quantity < product.maximum_batch_size
              ) {
                if (product.quantity % product.minimum_batch_size == 0) {
                  let len =
                    parseInt(product.quantity) /
                    parseInt(product.minimum_batch_size);
                  let batchPromiseArray = [];
                  for (var i = 0; i < len; i++) {
                    let amount = {
                      price: product.price,
                      quantity: product.minimum_batch_size,
                      flatDiscount: product.flatDiscount
                    };
                    batchPromiseArray.push(doPushingInArray(amount));
                  }
                  Promise.all(batchPromiseArray).then(batchdata => {
                    product = Object.assign({ batches: batchdata }, product);
                    products.push(product);
                    callback ? callback(null) : null;
                  });
                } else {
                  callback
                    ? callback(
                      `quantity should be multiple of ${product.minimum_batch_size}`
                    )
                    : null;
                }
              } else if (
                product.maximum_batch_size &&
                product.quantity >= product.maximum_batch_size
              ) {
                if (product.quantity % product.maximum_batch_size == 0) {
                  let len =
                    parseInt(product.quantity) /
                    parseInt(product.maximum_batch_size);
                  let batchPromiseArray = [];
                  for (var i = 0; i < len; i++) {
                    let amount = {
                      price: product.price,
                      quantity: product.maximum_batch_size,
                      flatDiscount: product.flatDiscount
                    };
                    batchPromiseArray.push(doPushingInArray(amount));
                  }
                  Promise.all(batchPromiseArray).then(batchdata => {
                    product = Object.assign({ batches: batchdata }, product);
                    products.push(product);
                    callback ? callback(null) : null;
                  });
                } else {
                  callback
                    ? callback(
                      `quantity should be multiple of ${product.maximum_batch_size}`
                    )
                    : null;
                }
              } else {
                let batchdata = {
                  quantity: product.quantity,
                  amount: product.amount
                };
                product = Object.assign({ batches: batchdata }, product);
                products.push(product);
                callback ? callback(null) : null;
              }
            }
          },
          async err => {
            try {
              if (!err) {
                const piObj = new piModel({
                  pi_uid: pi_uid,
                  pi_number: pi_number,
                  client_po_document: file.filename,
                  client_po_number: data.client_po_number,
                  date_of_issue_of_po: data.doi_of_po,
                  client: data.client,
                  products: products,
                  gross_price: data.gross_price,
                  discount_type: data.discount_type,
                  discount_on_gross_price: data.discount_on_total_price,
                  total_price: data.total_price,
                  estimated_dispatch_date: data.estimated_dispatch_date,
                  mode_of_shipping: data.mode_of_shipping,
                  port_of_discharge: data.port_of_discharge,
                  comments: data.comments
                });
                let pi = await piObj.save();
                await generateandsentPIdoc(res, pi.pi_uid, "Admin");
                resolve(pi);
              } else {
                throw new Error(err);
              }
            } catch (err) {
              reject(err);
            } finally {
            }
          }
        );
      }
    } catch (err) {
      reject(err);
    }
  });
};

function doPushingInArray(product) {
  return new Promise(resolve => {
    let amount = product.quantity * (product.price - product.flatDiscount);
    let batch = { quantity: product.quantity, amount: amount };
    resolve(batch);
  });
}

exports.sendEmailProformaInvoice = ({ res, pi_uid }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let pidoc = await generateandsentPIdoc(res, pi_uid, "User");
      resolve(pidoc);
    } catch (err) {
      reject(err);
    }
  });
};

exports.uploadsignedpidoc = ({ pi_uid, file }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let pidata = await piModel.findOne({ pi_uid: pi_uid }, { status: 1 });
      if (pidata.status == "Pending") {
        throw new Error("You first need to approve PI.");
      } else {
        let pi = await piModel.findOneAndUpdate(
          { pi_uid: pi_uid },
          { $set: { signed_pi_document: file.filename, status: "OpenOrder" } },
          { new: true }
        );
        if (!pi || pi == null) {
          throw new Error("PI does not exists.");
        }
        resolve(pi);
      }
    } catch (err) {
      reject(err);
    }
  });
};

exports.getallPIs = ({ query, req }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let piquery = await piQuery(query, req);
      let pi = await piModel.aggregate(piquery);
      let { pilisttype } = query;
      if (pilisttype && pilisttype != "" && pilisttype == "orderlistcount") {
        resolve({
          success: true,
          data: pi,
          message: "All PIs returned Successfully"
        });
      } else {
        if (pi[0].edges.length > 0) {
          resolve({
            success: true,
            data: pi[0].edges,
            count: pi[0].pageInfo[0].count,
            message: "All PIs returned Successfully"
          });
        } else {
          resolve({
            success: true,
            data: pi[0].edges,
            count: 0,
            message: "All PIs returned Successfully"
          });
        }
      }
    } catch (err) {
      reject(err);
    }
  });
};

exports.pri_shipping_order_pi = ({ query, req }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let { pageNo, pageSize, search } = query;
      let data_query;
      let _pageNo = parseInt(isNaN(pageNo) ? 1 : pageNo);
      let _pageSize = parseInt(isNaN(pageSize) ? 100 : pageSize);
      let skip = parseInt((_pageNo - 1) * _pageSize);
      if (req.decoded.userType == "Admin") {
        if (search && search != "") {
          data_query = {
            $and: [
              { $text: { $search: `\"${search}\"` } },
              { $or: [{ status: "OpenOrder" }, { status: "ShippedOrder" }] }
            ]
          };
        } else {
          data_query = {
            $or: [{ status: "OpenOrder" }, { status: "ShippedOrder" }]
          };
        }
      } else {
        if (search && search != "") {
          data_query = {
            $and: [
              { $text: { $search: `\"${search}\"` } },
              { "client.client_uid": req.decoded.customer_uid },
              { $or: [{ status: "OpenOrder" }, { status: "ShippedOrder" }] }
            ]
          };
        } else {
          data_query = {
            $and: [
              { "client.client_uid": req.decoded.customer_uid },
              { $or: [{ status: "OpenOrder" }, { status: "ShippedOrder" }] }
            ]
          };
        }
      }
      let pi = await piModel.aggregate([
        { $match: data_query },
        {
          $project: {
            client: 1,
            client_po_number: 1,
            client_po_document: 1,
            pi_uid: 1,
            signed_pi_document: 1,
            createdAt: 1,
            total_products: { $size: "$products" },
            status: {
              $cond: {
                if: { $eq: ["$status", "OpenOrder"] },
                then: "Open",
                else: "Closed"
              }
            }
          }
        },
        {
          $facet: {
            edges: [
              { $sort: { createdAt: -1 } },
              { $skip: skip },
              { $limit: _pageSize }
            ],
            pageInfo: [{ $group: { _id: null, count: { $sum: 1 } } }]
          }
        }
      ]);
      if (pi[0].edges.length > 0) {
        resolve({
          success: true,
          data: pi[0].edges,
          count: pi[0].pageInfo[0].count,
          message: "All PIs returned Successfully"
        });
      } else {
        resolve({
          success: true,
          data: pi[0].edges,
          count: 0,
          message: "All PIs returned Successfully"
        });
      }
    } catch (err) {
      reject(err);
    }
  });
};

exports.pri_shipping_order_pi_products = ({ pi_uid, req }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let data_query;
      let results = [];
      if (req.decoded.userType == "Admin") {
        data_query = {
          $and: [
            { $or: [{ status: "OpenOrder" }, { status: "ShippedOrder" }] },
            { pi_uid: pi_uid }
          ]
        };
      } else {
        data_query = {
          $and: [
            { "client.client_uid": req.decoded.customer_uid },
            { $or: [{ status: "OpenOrder" }, { status: "ShippedOrder" }] },
            { pi_uid: pi_uid }
          ]
        };
      }
      let pi = await piModel.aggregate([
        { $match: data_query },
        {
          $project: {
            client: 1,
            pi_uid: 1,
            products: {
              $map: {
                input: "$products",
                as: "product",
                in: {
                  _id: "$$product._id",
                  product_uid: "$$product.product_uid",
                  customerCodeNumber: "$$product.customerCodeNumber",
                  brand_name: "$$product.brand_name",
                  quantity: "$$product.quantity",
                  pending_quantity: {
                    $sum: {
                      $map: {
                        input: "$$product.batches",
                        as: "batch",
                        in: {
                          $cond: [
                            {
                              $or: [
                                {
                                  $and: [
                                    { $eq: ["$$batch.artwork.status", false] },
                                    {
                                      $eq: ["$$batch.manufacture.status", false]
                                    },
                                    { $eq: ["$$batch.testing.status", false] },
                                    { $eq: ["$$batch.dispatch.status", false] },
                                    { $eq: ["$$batch.shipped.status", false] }
                                  ]
                                },
                                {
                                  $and: [
                                    { $eq: ["$$batch.artwork.status", true] },
                                    {
                                      $eq: ["$$batch.manufacture.status", false]
                                    },
                                    { $eq: ["$$batch.testing.status", false] },
                                    { $eq: ["$$batch.dispatch.status", false] },
                                    { $eq: ["$$batch.shipped.status", false] }
                                  ]
                                },
                                {
                                  $and: [
                                    { $eq: ["$$batch.artwork.status", true] },
                                    {
                                      $eq: ["$$batch.manufacture.status", true]
                                    },
                                    { $eq: ["$$batch.testing.status", false] },
                                    { $eq: ["$$batch.dispatch.status", false] },
                                    { $eq: ["$$batch.shipped.status", false] }
                                  ]
                                },
                                {
                                  $and: [
                                    { $eq: ["$$batch.artwork.status", true] },
                                    {
                                      $eq: ["$$batch.manufacture.status", true]
                                    },
                                    { $eq: ["$$batch.testing.status", true] },
                                    { $eq: ["$$batch.dispatch.status", false] },
                                    { $eq: ["$$batch.shipped.status", false] }
                                  ]
                                }
                              ]
                            },
                            "$$batch.quantity",
                            0
                          ]
                        }
                      }
                    }
                  },
                  artwork: {
                    $map: {
                      input: "$$product.batches",
                      as: "batch",
                      in: {
                        $cond: [
                          { $and: [{ $eq: ["$$batch.artwork.status", true] }] },
                          "$$batch.artwork.file_name",
                          null
                        ]
                      }
                    }
                  }
                }
              }
            }
          }
        }
      ]);
      if (pi[0].products.length > 0) {
        async.each(
          pi[0].products,
          async (product, cb) => {
            var piproduct = {
              _id: product._id,
              product_uid: product.product_uid,
              customerCodeNumber: product.customerCodeNumber,
              brand_name: product.brand_name,
              quantity: product.quantity,
              pending_quantity: product.pending_quantity,
              artwork: product.artwork
            };
            let dispatched_quantity = 0;
            let packinglist = await packinglistModel.aggregate([
              {
                $match: {
                  $and: [
                    { invoice_uid: { $exists: true } },
                    { "products.pi_uid": pi[0].pi_uid },
                    { "products.product_id": product.product_uid }
                  ]
                }
              },
              {
                $project: {
                  quantity: {
                    $sum: {
                      $map: {
                        input: "$products",
                        as: "product",
                        in: {
                          $cond: [
                            {
                              $and: [
                                { $eq: ["$$product.pi_uid", pi[0].pi_uid] },
                                {
                                  $eq: [
                                    "$$product.product_id",
                                    product.product_uid
                                  ]
                                }
                              ]
                            },
                            "$$product.quantity",
                            0
                          ]
                        }
                      }
                    }
                  },
                  _id: 0
                }
              }
            ]);
            async.each(
              packinglist,
              (packing, cb2) => {
                dispatched_quantity += packing.quantity;
                cb2 ? cb2(null) : null;
              },
              err => {
                piproduct = Object.assign(
                  { dispatched_quantity: dispatched_quantity },
                  piproduct
                );
                results.push(piproduct);
                cb ? cb(null) : null;
              }
            );
          },
          err => {
            resolve({
              success: true,
              data: [
                {
                  _id: pi[0]._id,
                  pi_uid: pi[0].pi_uid,
                  client: pi[0].client,
                  products: results
                }
              ],
              message: "PI products returned Successfully"
            });
          }
        );
      } else {
        resolve({
          success: true,
          data: pi,
          message: "PI products returned Successfully"
        });
      }
    } catch (err) {
      reject(err);
    }
  });
};

exports.pri_shipping_order_pi_product_batches = ({
  pi_uid,
  productid,
  req
}) => {
  return new Promise(async (resolve, reject) => {
    try {
      let batches = await get_pi_product_batches({ pi_uid, productid, req });
      resolve({
        success: true,
        data: batches,
        message: "PI product batches returned Successfully"
      });
    } catch (err) {
      reject(err);
    }
  });
};

async function get_pi_product_batches({ pi_uid, productid, req }) {
  return new Promise(async (resolve, reject) => {
    try {
      let data_query;
      let batches = [];
      if (req.decoded.userType == "Admin") {
        data_query = {
          $or: [
            { status: "OpenOrder" },
            { status: "ShippedOrder" },
            { pi_uid: pi_uid }
          ]
        };
      } else {
        data_query = {
          $and: [
            { "client.client_uid": req.decoded.customer_uid },
            {
              $or: [
                { status: "OpenOrder" },
                { status: "ShippedOrder" },
                { pi_uid: pi_uid }
              ]
            }
          ]
        };
      }
      let pibatches = await piModel.aggregate([
        { $match: data_query },
        { $unwind: { path: "$products", preserveNullAndEmptyArrays: true } },
        { $match: { "products._id": ObjectId(productid) } },
        {
          $unwind: {
            path: "$products.batches",
            preserveNullAndEmptyArrays: true
          }
        },
        {
          $lookup: {
            from: "packinglists",
            localField: "products.batches._id",
            foreignField: "products.batch_id",
            as: "invoice"
          }
        }
      ]);
      async.each(
        pibatches,
        (batch, cb) => {
          let batchobj = {
            pi_uid: batch.pi_uid,
            productId: batch.products._id,
            batchId: batch.products.batches._id,
            batch_size: batch.products.batches.quantity,
            batchNo: batch.products.batches.batchNo,
            manufacturing: batch.products.batches.manufacture.status,
            testing: batch.products.batches.testing.status,
            CoA_doc: batch.products.batches.testing.CoA_doc,
            po_date: batch.date_of_issue_of_po,
            status: batch.products.batches.shipped.status
              ? "Delivered"
              : batch.products.batches.dispatch.status
                ? batch.invoice[0]
                  ? batch.invoice[0].shipped_onboard_date
                    ? "Shipped on board"
                    : "Dispatched"
                  : ""
                : batch.products.batches.testing.status
                  ? "Ready for dispatch"
                  : batch.products.batches.manufacture.status
                    ? "Under Testing"
                    : batch.products.batches.artwork.status
                      ? batch.products.batches.batchNo
                        ? "Under Manufacturing"
                        : "Artwork Ok"
                      : "Under Artwork",
            invoice_uid: batch.invoice[0] ? batch.invoice[0].invoice_uid : "",
            invoice_date: batch.invoice[0] ? batch.invoice[0].invoice_date : ""
          };
          if (batch.invoice[0] && batch.invoice[0].invoice_uid) {
            let dispatchedquantity = 0;
            async.each(
              batch.invoice[0].products,
              (product, cb2) => {
                if (
                  batch.pi_uid == product.pi_uid &&
                  batch.products.product_uid == product.product_id &&
                  batch.products.batches._id.toString() ==
                  product.batch_id.toString()
                ) {
                  dispatchedquantity += product.quantity;
                }
                cb2 ? cb2(null) : null;
              },
              err => {
                batchobj = Object.assign(
                  {
                    invoice_doc:
                      batch.invoice[0].admin_document.productinvoice_doc,
                    dispatched_quantity: dispatchedquantity
                  },
                  batchobj
                );
                batches.push(batchobj);
                cb ? cb(null) : null;
              }
            );
          } else {
            batchobj = Object.assign(
              { invoice_doc: "", dispatched_quantity: 0 },
              batchobj
            );
            batches.push(batchobj);
            cb ? cb(null) : null;
          }
        },
        err => {
          resolve(batches);
        }
      );
    } catch (err) {
      reject(err);
    }
  });
}

exports.get_product_product_history = ({ pi_uid, productid }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let producthistory = await customlogModel.aggregate([
        {
          $match: {
            $and: [
              { action_type: "PI" },
              { action_uid: pi_uid },
              { type: "Product" },
              { type_id: ObjectId(productid) }
            ]
          }
        },
        {
          $lookup: {
            from: "admins",
            let: { adminid: "$updated_by" },
            pipeline: [
              { $match: { $expr: { $and: [{ $eq: ["$$adminid", "$_id"] }] } } },
              { $project: { _id: 0, firstName: 1, lastName: 1 } }
            ],
            as: "updated_by"
          }
        },
        {
          $project: { _id: 0, updated_by: 1, updated_date: 1, description: 1 }
        },
        { $sort: { updated_date: 1 } }
      ]);
      resolve({
        success: true,
        data: producthistory,
        message: "PI product history returned successfully"
      });
    } catch (err) {
      reject(err);
    }
  });
};

exports.get_product_batch_history = ({ pi_uid, batchid }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let batchhistory = await customlogModel.aggregate([
        {
          $match: {
            $and: [
              { action_type: "PI" },
              { action_uid: pi_uid },
              { type: "Batch" },
              { type_id: ObjectId(batchid) }
            ]
          }
        },
        {
          $lookup: {
            from: "admins",
            let: { adminid: "$updated_by" },
            pipeline: [
              { $match: { $expr: { $and: [{ $eq: ["$$adminid", "$_id"] }] } } },
              { $project: { _id: 0, firstName: 1, lastName: 1 } }
            ],
            as: "updated_by"
          }
        },
        {
          $project: { _id: 0, updated_by: 1, updated_date: 1, description: 1 }
        },
        { $sort: { updated_date: 1 } }
      ]);
      resolve({
        success: true,
        data: batchhistory,
        message: "PI product batches history returned successfully"
      });
    } catch (err) {
      reject(err);
    }
  });
};

exports.change_product_manufacturingandtesting_status = ({
  body,
  actionType,
  file,
  coa_doc,
  req
}) => {
  return new Promise(async (resolve, reject) => {
    try {
      let { pi_uid, productId, batchId, batchNo } = JSON.parse(body.data);
      let description = "";
      if (actionType == "Batch") {
        await piModel.findOneAndUpdate(
          {
            pi_uid: pi_uid,
            products: {
              $elemMatch: {
                _id: ObjectId(productId),
                "batches._id": ObjectId(batchId)
              }
            }
          },
          { $set: { "products.$[outer].batches.$[inner].batchNo": batchNo } },
          {
            arrayFilters: [
              { "outer._id": ObjectId(productId) },
              { "inner._id": ObjectId(batchId) }
            ]
          }
        );
        description = `Batch No '${batchNo}' entered`;
      } else if (actionType == "Manufacturing") {
        await piModel.findOneAndUpdate(
          {
            pi_uid: pi_uid,
            products: {
              $elemMatch: {
                _id: ObjectId(productId),
                "batches._id": ObjectId(batchId)
              }
            }
          },
          {
            $set: {
              "products.$[outer].batches.$[inner].manufacture.status": true,
              "products.$[outer].batches.$[inner].manufacture.uploaded_date": new Date(),
              "products.$[outer].batches.$[inner].manufacture.uploaded_by":
                req.decoded.id,
              "products.$[outer].batches.$[inner].workstatus": "Manufacture",
              "products.$[outer].batches.$[inner].greenTick": 3,
              "products.$[outer].batches.$[inner].redTick": 4,
              "products.$[outer].batches.$[inner].manufacture.file_name": file
            }
          },
          {
            arrayFilters: [
              { "outer._id": ObjectId(productId) },
              { "inner._id": ObjectId(batchId) }
            ]
          }
        );
        description = "Manufacturing done";
      } else if (actionType == "Testing") {
        await piModel.findOneAndUpdate(
          {
            pi_uid: pi_uid,
            products: {
              $elemMatch: {
                _id: ObjectId(productId),
                "batches._id": ObjectId(batchId)
              }
            }
          },
          {
            $set: {
              "products.$[outer].batches.$[inner].testing.status": true,
              "products.$[outer].batches.$[inner].testing.uploaded_date": new Date(),
              "products.$[outer].batches.$[inner].testing.uploaded_by":
                req.decoded.id,
              "products.$[outer].batches.$[inner].workstatus": "Testing",
              "products.$[outer].batches.$[inner].greenTick": 4,
              "products.$[outer].batches.$[inner].redTick": 3,
              "products.$[outer].batches.$[inner].testing.file_name": file,
              "products.$[outer].batches.$[inner].testing.CoA_doc": coa_doc
            }
          },
          {
            arrayFilters: [
              { "outer._id": ObjectId(productId) },
              { "inner._id": ObjectId(batchId) }
            ]
          }
        );
        description = "Testing done";
      }
      const customlogObj = new customlogModel({
        action_type: "PI",
        action_uid: pi_uid,
        type: "Batch",
        type_id: batchId,
        description: description,
        updated_by: req.decoded.id,
        updated_date: new Date()
      });
      await customlogObj.save();
      let batches = await get_pi_product_batches({
        pi_uid,
        productid: productId,
        req
      });
      resolve({
        success: true,
        data: batches,
        message: "PI product batches status changed Successfully"
      });
    } catch (err) {
      reject(err);
    }
  });
};

exports.get_product_artworkdoc = ({ product_uid }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let productartworkdoc = await productModel.findOne(
        { product_uid: product_uid },
        { _id: 0, artwork_doc: 1 }
      );
      resolve(productartworkdoc);
    } catch (err) {
      reject(err);
    }
  });
};

exports.copy_productartworkdor_to_piartworkdoc = ({ query, req }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let { pi_uid, productid, product_uid } = query;
      let data_query;
      let product = await productModel.findOne(
        { product_uid: product_uid },
        { artwork_doc: 1, customer_code_number: 1 }
      );
      if (product && product.artwork_doc) {
        const srcdir = `${path.dirname(
          process.mainModule.filename
        )}/upload/product_artwork_doc`;
        var srcdocpath = srcdir + "/" + product.artwork_doc;
        const destdir = `${path.dirname(
          process.mainModule.filename
        )}/upload/orderstatusfiles`;
        var destdocpath = destdir + "/" + product.artwork_doc;
        fs.copyFileSync(srcdocpath, destdocpath);
        await piModel.findOneAndUpdate(
          {
            pi_uid: pi_uid,
            products: {
              $elemMatch: { _id: ObjectId(productid), product_uid: product_uid }
            }
          },
          {
            $set: {
              "products.$[outer].batches.$[].artwork.status": true,
              "products.$[outer].batches.$[].artwork.uploaded_date": new Date(),
              "products.$[outer].batches.$[].artwork.uploaded_by":
                req.decoded.id,
              "products.$[outer].batches.$[].workstatus": "Artwork",
              "products.$[outer].batches.$[].greenTick": 2,
              "products.$[outer].batches.$[].redTick": 5,
              "products.$[outer].batches.$[].artwork.file_name":
                product.artwork_doc
            }
          },
          { arrayFilters: [{ "outer._id": ObjectId(productid) }] }
        );
        const customlogObj = new customlogModel({
          action_type: "PI",
          action_uid: pi_uid,
          type: "Product",
          type_id: productid,
          description: `Artwork complete for product '${product.customer_code_number}'`,
          updated_by: req.decoded.id,
          updated_date: new Date()
        });
        await customlogObj.save();
        if (req.decoded.userType == "Admin") {
          data_query = {
            $and: [
              { $or: [{ status: "OpenOrder" }, { status: "ShippedOrder" }] },
              { pi_uid: pi_uid }
            ]
          };
        } else {
          data_query = {
            $and: [
              { "client.client_uid": req.decoded.customer_uid },
              { $or: [{ status: "OpenOrder" }, { status: "ShippedOrder" }] },
              { pi_uid: pi_uid }
            ]
          };
        }
        let pi = await piModel.aggregate([
          { $match: data_query },
          {
            $project: {
              client: 1,
              pi_uid: 1,
              products: {
                $map: {
                  input: "$products",
                  as: "product",
                  in: {
                    _id: "$$product._id",
                    product_uid: "$$product.product_uid",
                    customerCodeNumber: "$$product.customerCodeNumber",
                    brand_name: "$$product.brand_name",
                    quantity: "$$product.quantity",
                    dispatched_quantity: {
                      $sum: {
                        $map: {
                          input: "$$product.batches",
                          as: "batch",
                          in: {
                            $cond: [
                              {
                                $or: [
                                  {
                                    $and: [
                                      { $eq: ["$$batch.artwork.status", true] },
                                      {
                                        $eq: [
                                          "$$batch.manufacture.status",
                                          true
                                        ]
                                      },
                                      { $eq: ["$$batch.testing.status", true] },
                                      {
                                        $eq: ["$$batch.dispatch.status", true]
                                      },
                                      { $eq: ["$$batch.shipped.status", false] }
                                    ]
                                  },
                                  {
                                    $and: [
                                      { $eq: ["$$batch.artwork.status", true] },
                                      {
                                        $eq: [
                                          "$$batch.manufacture.status",
                                          true
                                        ]
                                      },
                                      { $eq: ["$$batch.testing.status", true] },
                                      {
                                        $eq: ["$$batch.dispatch.status", true]
                                      },
                                      { $eq: ["$$batch.shipped.status", true] }
                                    ]
                                  }
                                ]
                              },
                              "$$batch.quantity",
                              0
                            ]
                          }
                        }
                      }
                    },
                    pending_quantity: {
                      $sum: {
                        $map: {
                          input: "$$product.batches",
                          as: "batch",
                          in: {
                            $cond: [
                              {
                                $or: [
                                  {
                                    $and: [
                                      {
                                        $eq: ["$$batch.artwork.status", false]
                                      },
                                      {
                                        $eq: [
                                          "$$batch.manufacture.status",
                                          false
                                        ]
                                      },
                                      {
                                        $eq: ["$$batch.testing.status", false]
                                      },
                                      {
                                        $eq: ["$$batch.dispatch.status", false]
                                      },
                                      { $eq: ["$$batch.shipped.status", false] }
                                    ]
                                  },
                                  {
                                    $and: [
                                      { $eq: ["$$batch.artwork.status", true] },
                                      {
                                        $eq: [
                                          "$$batch.manufacture.status",
                                          false
                                        ]
                                      },
                                      {
                                        $eq: ["$$batch.testing.status", false]
                                      },
                                      {
                                        $eq: ["$$batch.dispatch.status", false]
                                      },
                                      { $eq: ["$$batch.shipped.status", false] }
                                    ]
                                  },
                                  {
                                    $and: [
                                      { $eq: ["$$batch.artwork.status", true] },
                                      {
                                        $eq: [
                                          "$$batch.manufacture.status",
                                          true
                                        ]
                                      },
                                      {
                                        $eq: ["$$batch.testing.status", false]
                                      },
                                      {
                                        $eq: ["$$batch.dispatch.status", false]
                                      },
                                      { $eq: ["$$batch.shipped.status", false] }
                                    ]
                                  },
                                  {
                                    $and: [
                                      { $eq: ["$$batch.artwork.status", true] },
                                      {
                                        $eq: [
                                          "$$batch.manufacture.status",
                                          true
                                        ]
                                      },
                                      { $eq: ["$$batch.testing.status", true] },
                                      {
                                        $eq: ["$$batch.dispatch.status", false]
                                      },
                                      { $eq: ["$$batch.shipped.status", false] }
                                    ]
                                  }
                                ]
                              },
                              "$$batch.quantity",
                              0
                            ]
                          }
                        }
                      }
                    },
                    artwork: {
                      $map: {
                        input: "$$product.batches",
                        as: "batch",
                        in: {
                          $cond: [
                            {
                              $and: [{ $eq: ["$$batch.artwork.status", true] }]
                            },
                            "$$batch.artwork.file_name",
                            null
                          ]
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        ]);
        resolve({
          success: true,
          data: pi,
          message: "PI products returned Successfully"
        });
      } else {
        throw new Error("Product artwork doc does not exists.");
      }
    } catch (err) {
      reject(err);
    }
  });
};

exports.getallpri_post_order = ({ req }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let pri_order = 0,
        post_order = 0;
      pri_orders_query = {};
      post_orders_query = {};
      if (req.decoded.userType == "Admin") {
        pri_orders_query = {
          $or: [{ status: "OpenOrder" }, { status: "ShippedOrder" }]
        };
        post_orders_query = {
          $or: [{ status: "OpenInvoice" }, { status: "CloseInvoice" }]
        };
      } else {
        pri_orders_query = {
          $and: [
            { "client.client_uid": req.decoded.customer_uid },
            { $or: [{ status: "OpenOrder" }, { status: "ShippedOrder" }] }
          ]
        };
        post_orders_query = {
          $and: [
            { "client.client_uid": req.decoded.customer_uid },
            { $or: [{ status: "OpenInvoice" }, { status: "CloseInvoice" }] }
          ]
        };
      }
      let pri_orders = await piModel.aggregate([
        { $match: pri_orders_query },
        {
          $group: {
            _id: 0,
            openorder_count: {
              $sum: { $cond: [{ $eq: ["$status", "OpenOrder"] }, 1, 0] }
            },
            shippedorder_count: {
              $sum: {
                $cond: {
                  if: { $eq: ["$status", "ShippedOrder"] },
                  then: {
                    $cond: [
                      {
                        $eq: [
                          {
                            $sum: {
                              $map: {
                                input: "$products",
                                as: "product",
                                in: {
                                  $sum: {
                                    $map: {
                                      input: "$$product.batches",
                                      as: "batch",
                                      in: {
                                        $cond: [
                                          {
                                            $and: [
                                              {
                                                $eq: [
                                                  "$$batch.dispatch.status",
                                                  true
                                                ]
                                              },
                                              {
                                                $eq: [
                                                  "$$batch.shipped.status",
                                                  false
                                                ]
                                              }
                                            ]
                                          },
                                          1,
                                          0
                                        ]
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          },
                          {
                            $sum: {
                              $map: {
                                input: "$products",
                                as: "product",
                                in: {
                                  $sum: {
                                    $map: {
                                      input: "$$product.batches",
                                      as: "batch",
                                      in: {
                                        $cond: [
                                          { $ifNull: ["$$batch._id", true] },
                                          1,
                                          0
                                        ]
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        ]
                      },
                      1,
                      0
                    ]
                  },
                  else: 0
                }
              }
            }
          }
        },
        {
          $project: {
            _id: 0,
            total_priorder: {
              $add: ["$openorder_count", "$shippedorder_count"]
            }
          }
        }
      ]);
      if (pri_orders && pri_orders[0]) {
        pri_order = pri_orders[0].total_priorder;
      }
      let post_orders = await packinglistModel.aggregate([
        { $match: post_orders_query },
        {
          $group: {
            _id: 0,
            openinvoice_count: {
              $sum: { $cond: [{ $eq: ["$status", "OpenInvoice"] }, 1, 0] }
            },
            closeinvoice_count: {
              $sum: { $cond: [{ $eq: ["$status", "CloseInvoice"] }, 1, 0] }
            }
          }
        },
        {
          $project: {
            _id: 0,
            total_postorder: {
              $add: ["$openinvoice_count", "$closeinvoice_count"]
            }
          }
        }
      ]);
      if (post_orders && post_orders[0]) {
        post_order = post_orders[0].total_postorder;
      }
      let total_orders = { pri_order: pri_order, post_order: post_order };
      resolve(total_orders);
    } catch (err) {
      reject(err);
    }
  });
};

exports.getPIdetails = ({ pi_uid, piordertype, req }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let pi;
      let _query = {};
      if (req.decoded.userType == "Admin") {
        _query = { pi_uid: pi_uid };
      } else {
        _query = {
          $and: [
            { "client.client_uid": req.decoded.customer_uid },
            { pi_uid: pi_uid }
          ]
        };
      }
      if (piordertype == "OpenOrder") {
        pi = await piModel.aggregate([
          { $match: _query },
          {
            $project: {
              _id: 1,
              client: 1,
              createdAt: 1,
              customerCodeNumber: 1,
              date_of_issue_of_po: 1,
              gross_price: 1,
              discount_type: 1,
              discount_on_gross_price: 1,
              total_price: 1,
              status: 1,
              pi_uid: 1,
              client_po_document: 1,
              client_po_number: 1,
              estimated_dispatch_date: 1,
              mode_of_shipping: 1,
              comments: 1,
              approved_date: 1,
              signed_pi_document: 1,
              products: {
                $filter: {
                  input: {
                    $map: {
                      input: "$products",
                      as: "product",
                      in: {
                        _id: "$$product._id",
                        product_uid: "$$product.product_uid",
                        client_product_code: "$$product.client_product_code",
                        quantity: "$$product.quantity",
                        price: "$$product.price",
                        flatDiscount: "$$product.flatDiscount",
                        customerCodeNumber: "$$product.customerCodeNumber",
                        complete_packaging_profile:
                          "$$product.complete_packaging_profile",
                        unit_of_billing: "$$product.unit_of_billing",
                        registration_no: "$$product.registration_no",
                        registration_date: "$$product.registration_date",
                        shelf_life: "$$product.shelf_life",
                        frieght: "$$product.frieght",
                        insurance: "$$product.insurance",
                        hsn_code: "$$product.hsn_code",
                        minimum_batch_size: "$$product.minimum_batch_size",
                        maximum_batch_size: "$$product.maximum_batch_size",
                        shipper_size_inner_dimension:
                          "$$product.shipper_size_inner_dimension",
                        batches: {
                          $filter: {
                            input: "$$product.batches",
                            as: "batch",
                            cond: {
                              $or: [
                                {
                                  $and: [
                                    { $eq: ["$$batch.artwork.status", false] },
                                    {
                                      $eq: ["$$batch.manufacture.status", false]
                                    },
                                    { $eq: ["$$batch.testing.status", false] },
                                    { $eq: ["$$batch.dispatch.status", false] },
                                    { $eq: ["$$batch.shipped.status", false] }
                                  ]
                                },
                                {
                                  $and: [
                                    { $eq: ["$$batch.artwork.status", true] },
                                    {
                                      $eq: ["$$batch.manufacture.status", false]
                                    },
                                    { $eq: ["$$batch.testing.status", false] },
                                    { $eq: ["$$batch.dispatch.status", false] },
                                    { $eq: ["$$batch.shipped.status", false] }
                                  ]
                                },
                                {
                                  $and: [
                                    { $eq: ["$$batch.artwork.status", true] },
                                    {
                                      $eq: ["$$batch.manufacture.status", true]
                                    },
                                    { $eq: ["$$batch.testing.status", false] },
                                    { $eq: ["$$batch.dispatch.status", false] },
                                    { $eq: ["$$batch.shipped.status", false] }
                                  ]
                                },
                                {
                                  $and: [
                                    { $eq: ["$$batch.artwork.status", true] },
                                    {
                                      $eq: ["$$batch.manufacture.status", true]
                                    },
                                    { $eq: ["$$batch.testing.status", true] },
                                    { $eq: ["$$batch.dispatch.status", false] },
                                    { $eq: ["$$batch.shipped.status", false] }
                                  ]
                                }
                              ]
                            }
                          }
                        }
                      }
                    }
                  },
                  as: "product",
                  cond: { $gt: [{ $size: "$$product.batches" }, 0] }
                }
              },
              artworkcount: {
                $sum: {
                  $map: {
                    input: "$products",
                    as: "product",
                    in: {
                      $sum: {
                        $map: {
                          input: "$$product.batches",
                          as: "batch",
                          in: {
                            $cond: [
                              {
                                $and: [
                                  { $eq: ["$$batch.artwork.status", false] },
                                  {
                                    $eq: ["$$batch.manufacture.status", false]
                                  },
                                  { $eq: ["$$batch.testing.status", false] },
                                  { $eq: ["$$batch.dispatch.status", false] },
                                  { $eq: ["$$batch.shipped.status", false] }
                                ]
                              },
                              1,
                              0
                            ]
                          }
                        }
                      }
                    }
                  }
                }
              },
              manufacturecount: {
                $sum: {
                  $map: {
                    input: "$products",
                    as: "product",
                    in: {
                      $sum: {
                        $map: {
                          input: "$$product.batches",
                          as: "batch",
                          in: {
                            $cond: [
                              {
                                $and: [
                                  { $eq: ["$$batch.artwork.status", true] },
                                  {
                                    $eq: ["$$batch.manufacture.status", false]
                                  },
                                  { $eq: ["$$batch.testing.status", false] },
                                  { $eq: ["$$batch.dispatch.status", false] },
                                  { $eq: ["$$batch.shipped.status", false] }
                                ]
                              },
                              1,
                              0
                            ]
                          }
                        }
                      }
                    }
                  }
                }
              },
              testingcount: {
                $sum: {
                  $map: {
                    input: "$products",
                    as: "product",
                    in: {
                      $sum: {
                        $map: {
                          input: "$$product.batches",
                          as: "batch",
                          in: {
                            $cond: [
                              {
                                $and: [
                                  { $eq: ["$$batch.artwork.status", true] },
                                  { $eq: ["$$batch.manufacture.status", true] },
                                  { $eq: ["$$batch.testing.status", false] },
                                  { $eq: ["$$batch.dispatch.status", false] },
                                  { $eq: ["$$batch.shipped.status", false] }
                                ]
                              },
                              1,
                              0
                            ]
                          }
                        }
                      }
                    }
                  }
                }
              },
              dispatchcount: {
                $sum: {
                  $map: {
                    input: "$products",
                    as: "product",
                    in: {
                      $sum: {
                        $map: {
                          input: "$$product.batches",
                          as: "batch",
                          in: {
                            $cond: [
                              {
                                $and: [
                                  { $eq: ["$$batch.artwork.status", true] },
                                  { $eq: ["$$batch.manufacture.status", true] },
                                  { $eq: ["$$batch.testing.status", true] },
                                  { $eq: ["$$batch.dispatch.status", false] },
                                  { $eq: ["$$batch.shipped.status", false] }
                                ]
                              },
                              1,
                              0
                            ]
                          }
                        }
                      }
                    }
                  }
                }
              },
              shippedcount: {
                $sum: {
                  $map: {
                    input: "$products",
                    as: "product",
                    in: {
                      $sum: {
                        $map: {
                          input: "$$product.batches",
                          as: "batch",
                          in: {
                            $cond: [
                              {
                                $and: [
                                  { $eq: ["$$batch.artwork.status", true] },
                                  { $eq: ["$$batch.manufacture.status", true] },
                                  { $eq: ["$$batch.testing.status", true] },
                                  { $eq: ["$$batch.dispatch.status", true] },
                                  { $eq: ["$$batch.shipped.status", false] }
                                ]
                              },
                              1,
                              0
                            ]
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        ]);
      } else if (piordertype == "ShippedOrder") {
        pi = await piModel.aggregate([
          { $match: _query },
          {
            $project: {
              _id: 1,
              client: 1,
              createdAt: 1,
              customerCodeNumber: 1,
              date_of_issue_of_po: 1,
              gross_price: 1,
              discount_type: 1,
              discount_on_gross_price: 1,
              total_price: 1,
              status: 1,
              pi_uid: 1,
              client_po_document: 1,
              client_po_number: 1,
              estimated_dispatch_date: 1,
              mode_of_shipping: 1,
              comments: 1,
              approved_date: 1,
              signed_pi_document: 1,
              products: {
                $filter: {
                  input: {
                    $map: {
                      input: "$products",
                      as: "product",
                      in: {
                        _id: "$$product._id",
                        product_uid: "$$product.product_uid",
                        client_product_code: "$$product.client_product_code",
                        quantity: "$$product.quantity",
                        price: "$$product.price",
                        flatDiscount: "$$product.flatDiscount",
                        customerCodeNumber: "$$product.customerCodeNumber",
                        complete_packaging_profile:
                          "$$product.complete_packaging_profile",
                        unit_of_billing: "$$product.unit_of_billing",
                        registration_no: "$$product.registration_no",
                        registration_date: "$$product.registration_date",
                        shelf_life: "$$product.shelf_life",
                        frieght: "$$product.frieght",
                        insurance: "$$product.insurance",
                        hsn_code: "$$product.hsn_code",
                        minimum_batch_size: "$$product.minimum_batch_size",
                        maximum_batch_size: "$$product.maximum_batch_size",
                        shipper_size_inner_dimension:
                          "$$product.shipper_size_inner_dimension",
                        batches: {
                          $filter: {
                            input: "$$product.batches",
                            as: "batch",
                            cond: {
                              $and: [
                                { $eq: ["$$batch.artwork.status", true] },
                                { $eq: ["$$batch.manufacture.status", true] },
                                { $eq: ["$$batch.testing.status", true] },
                                { $eq: ["$$batch.dispatch.status", true] },
                                { $eq: ["$$batch.shipped.status", false] }
                              ]
                            }
                          }
                        }
                      }
                    }
                  },
                  as: "product",
                  cond: { $gt: [{ $size: "$$product.batches" }, 0] }
                }
              },
              artworkcount: {
                $sum: {
                  $map: {
                    input: "$products",
                    as: "product",
                    in: {
                      $sum: {
                        $map: {
                          input: "$$product.batches",
                          as: "batch",
                          in: {
                            $cond: [
                              {
                                $and: [
                                  { $eq: ["$$batch.artwork.status", false] },
                                  {
                                    $eq: ["$$batch.manufacture.status", false]
                                  },
                                  { $eq: ["$$batch.testing.status", false] },
                                  { $eq: ["$$batch.dispatch.status", false] },
                                  { $eq: ["$$batch.shipped.status", false] }
                                ]
                              },
                              1,
                              0
                            ]
                          }
                        }
                      }
                    }
                  }
                }
              },
              manufacturecount: {
                $sum: {
                  $map: {
                    input: "$products",
                    as: "product",
                    in: {
                      $sum: {
                        $map: {
                          input: "$$product.batches",
                          as: "batch",
                          in: {
                            $cond: [
                              {
                                $and: [
                                  { $eq: ["$$batch.artwork.status", true] },
                                  {
                                    $eq: ["$$batch.manufacture.status", false]
                                  },
                                  { $eq: ["$$batch.testing.status", false] },
                                  { $eq: ["$$batch.dispatch.status", false] },
                                  { $eq: ["$$batch.shipped.status", false] }
                                ]
                              },
                              1,
                              0
                            ]
                          }
                        }
                      }
                    }
                  }
                }
              },
              testingcount: {
                $sum: {
                  $map: {
                    input: "$products",
                    as: "product",
                    in: {
                      $sum: {
                        $map: {
                          input: "$$product.batches",
                          as: "batch",
                          in: {
                            $cond: [
                              {
                                $and: [
                                  { $eq: ["$$batch.artwork.status", true] },
                                  { $eq: ["$$batch.manufacture.status", true] },
                                  { $eq: ["$$batch.testing.status", false] },
                                  { $eq: ["$$batch.dispatch.status", false] },
                                  { $eq: ["$$batch.shipped.status", false] }
                                ]
                              },
                              1,
                              0
                            ]
                          }
                        }
                      }
                    }
                  }
                }
              },
              dispatchcount: {
                $sum: {
                  $map: {
                    input: "$products",
                    as: "product",
                    in: {
                      $sum: {
                        $map: {
                          input: "$$product.batches",
                          as: "batch",
                          in: {
                            $cond: [
                              {
                                $and: [
                                  { $eq: ["$$batch.artwork.status", true] },
                                  { $eq: ["$$batch.manufacture.status", true] },
                                  { $eq: ["$$batch.testing.status", true] },
                                  { $eq: ["$$batch.dispatch.status", false] },
                                  { $eq: ["$$batch.shipped.status", false] }
                                ]
                              },
                              1,
                              0
                            ]
                          }
                        }
                      }
                    }
                  }
                }
              },
              shippedcount: {
                $sum: {
                  $map: {
                    input: "$products",
                    as: "product",
                    in: {
                      $sum: {
                        $map: {
                          input: "$$product.batches",
                          as: "batch",
                          in: {
                            $cond: [
                              {
                                $and: [
                                  { $eq: ["$$batch.artwork.status", true] },
                                  { $eq: ["$$batch.manufacture.status", true] },
                                  { $eq: ["$$batch.testing.status", true] },
                                  { $eq: ["$$batch.dispatch.status", true] },
                                  { $eq: ["$$batch.shipped.status", false] }
                                ]
                              },
                              1,
                              0
                            ]
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        ]);
      } else {
        pi = await piModel.aggregate([
          { $match: _query },
          {
            $project: {
              _id: 1,
              client: 1,
              createdAt: 1,
              date_of_issue_of_po: 1,
              gross_price: 1,
              discount_type: 1,
              discount_on_gross_price: 1,
              total_price: 1,
              status: 1,
              pi_uid: 1,
              client_po_document: 1,
              client_po_number: 1,
              port_of_discharge: 1,
              estimated_dispatch_date: 1,
              mode_of_shipping: 1,
              comments: 1,
              approved_date: 1,
              signed_pi_document: 1,
              products: 1
            }
          }
        ]);
      }
      if (!pi[0] || pi[0] == null) {
        throw new Error("PI does not exists.");
      } else {
        resolve(pi[0]);
      }
    } catch (err) {
      reject(err);
    }
  });
};

exports.update_proformainvoice = ({ res, body, file, pi_uid }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let data = JSON.parse(body.data);
      let products = [];
      if (data.product.length > 0) {
        async.eachSeries(
          data.product,
          (product, callback) => {
            if (
              (product.minimum_batch_size == 0 &&
                product.maximum_batch_size == 0) ||
              (product.minimum_batch_size == 0 &&
                product.maximum_batch_size > 0 &&
                product.quantity < product.maximum_batch_size)
            ) {
              let batchdata = {
                quantity: product.quantity,
                amount: product.amount
              };
              product = Object.assign({ batches: batchdata }, product);
              products.push(product);
              callback ? callback(null) : null;
            } else {
              if (
                product.minimum_batch_size &&
                product.quantity >= product.minimum_batch_size &&
                product.quantity < product.maximum_batch_size
              ) {
                if (product.quantity % product.minimum_batch_size == 0) {
                  let len =
                    parseInt(product.quantity) /
                    parseInt(product.minimum_batch_size);
                  let batchPromiseArray = [];
                  for (var i = 0; i < len; i++) {
                    let amount = {
                      price: product.price,
                      quantity: product.minimum_batch_size,
                      flatDiscount: product.flatDiscount
                    };
                    batchPromiseArray.push(doPushingInArray(amount));
                  }
                  Promise.all(batchPromiseArray).then(batchdata => {
                    product = Object.assign({ batches: batchdata }, product);
                    products.push(product);
                    callback ? callback(null) : null;
                  });
                } else {
                  callback
                    ? callback(
                      `quantity should be multiple of ${product.minimum_batch_size}`
                    )
                    : `quantity should be multiple of ${product.minimum_batch_size}`;
                }
              } else if (
                product.maximum_batch_size &&
                product.quantity >= product.maximum_batch_size
              ) {
                if (product.quantity % product.maximum_batch_size == 0) {
                  let len =
                    parseInt(product.quantity) /
                    parseInt(product.maximum_batch_size);
                  let batchPromiseArray = [];
                  for (var i = 0; i < len; i++) {
                    let amount = {
                      price: product.price,
                      quantity: product.maximum_batch_size,
                      flatDiscount: product.flatDiscount
                    };
                    batchPromiseArray.push(doPushingInArray(amount));
                  }
                  Promise.all(batchPromiseArray).then(batchdata => {
                    product = Object.assign({ batches: batchdata }, product);
                    products.push(product);
                    callback ? callback(null) : null;
                  });
                } else {
                  callback
                    ? callback(
                      `quantity should be multiple of ${product.maximum_batch_size}`
                    )
                    : `quantity should be multiple of ${product.maximum_batch_size}`;
                }
              } else {
                let batchdata = {
                  quantity: product.quantity,
                  amount: product.amount
                };
                product = Object.assign({ batches: batchdata }, product);
                products.push(product);
                callback ? callback(null) : null;
              }
            }
          },
          async err => {
            try {
              if (!err) {
                let pidata = {
                  client_po_number: data.client_po_number,
                  date_of_issue_of_po: data.doi_of_po,
                  client: data.client,
                  gross_price: data.gross_price,
                  discount_type: data.discount_type,
                  discount_on_gross_price: data.discount_on_total_price,
                  total_price: data.total_price,
                  estimated_dispatch_date: data.estimated_dispatch_date,
                  mode_of_shipping: data.mode_of_shipping,
                  port_of_discharge: data.port_of_discharge,
                  comments: data.comments,
                  products: products
                };
                if (data.status) {
                  pidata = Object.assign(
                    { status: "Approved", approved_date: new Date() },
                    pidata
                  );
                }
                if (file && file.filename) {
                  let oldpi = await piModel
                    .findOne({ _id: ObjectId(pi_uid) })
                    .select("client_po_document");
                  if (oldpi) {
                    const dir = `${path.dirname(
                      process.mainModule.filename
                    )}/upload/podoc`;
                    var imagepath = dir + "/" + oldpi.client_po_document;
                    fs.unlinkSync(imagepath);
                    pidata = Object.assign(
                      { client_po_document: file.filename },
                      pidata
                    );
                  }
                }
                let pi = await piModel.findByIdAndUpdate(
                  pi_uid,
                  { $set: pidata },
                  { new: true }
                );
                if (!pi || pi == null) {
                  throw new Error("PI does not exists.");
                } else {
                  await generateandsentPIdoc(res, pi.pi_uid, "Admin");
                  resolve(pi);
                }
              } else {
                throw new Error(err);
              }
            } catch (err) {
              reject(err);
            } finally {
            }
          }
        );
      }
    } catch (err) {
      reject(err);
    }
  });
};

exports.updateOrderStatus = ({ body, files, coa_doc, query }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let { productId, actiontype, batchId } = query;
      const {
        pi_uid,
        product_uid,
        shipped_onboard_date,
        payment_due_date,
        estimated_arrival_date,
        redTick,
        greenTick,
        remark
      } = JSON.parse(body.data);
      let workstatus = "";
      let statusfiles = [];
      let greentickType = "";
      let status = "";
      let artwork = false,
        manufacturing = false,
        testing = false,
        dispatch = false,
        shipped = false,
        shipped_status = false;
      if (greenTick == 1) {
        (artwork = false),
          (manufacturing = false),
          (testing = false),
          (dispatch = false),
          (shipped = false),
          (workstatus = "IssuedOn"),
          (greentickType = "issuedon"),
          (status = "OpenOrder");
      }
      if (greenTick == 2) {
        (artwork = true),
          (manufacturing = false),
          (testing = false),
          (dispatch = false),
          (shipped = false),
          (workstatus = "Artwork"),
          (greentickType = "artwork"),
          (status = "OpenOrder");
        if (files) {
          for (const file of files) {
            statusfiles.push({
              file_name: file.filename,
              uploaded_date: new Date()
            });
          }
        }
      }
      if (greenTick == 3) {
        (artwork = true),
          (manufacturing = true),
          (testing = false),
          (dispatch = false),
          (shipped = false),
          (workstatus = "Manufacture"),
          (greentickType = "manufacture"),
          (status = "OpenOrder");
        if (files) {
          for (const file of files) {
            statusfiles.push({
              file_name: file.filename,
              uploaded_date: new Date()
            });
          }
        }
      }
      if (greenTick == 4) {
        (artwork = true),
          (manufacturing = true),
          (testing = true),
          (dispatch = false),
          (shipped = false),
          (workstatus = "Testing"),
          (greentickType = "testing"),
          (status = "OpenOrder");
        if (files) {
          for (const file of files) {
            statusfiles.push({
              file_name: file.filename,
              uploaded_date: new Date()
            });
          }
        }
      }
      if (greenTick == 5) {
        let pidata = await piModel.aggregate([
          { $match: { $and: [{ status: "OpenOrder" }, { pi_uid: pi_uid }] } },
          {
            $project: {
              client: 1,
              total_batches: {
                $sum: {
                  $map: {
                    input: "$products",
                    as: "product",
                    in: {
                      $sum: {
                        $map: {
                          input: "$$product.batches",
                          as: "batch",
                          in: {
                            $cond: [
                              { $and: [{ $ne: ["$batch._id", null] }] },
                              1,
                              0
                            ]
                          }
                        }
                      }
                    }
                  }
                }
              },
              dispatchcount: {
                $sum: {
                  $map: {
                    input: "$products",
                    as: "product",
                    in: {
                      $sum: {
                        $map: {
                          input: "$$product.batches",
                          as: "batch",
                          in: {
                            $cond: [
                              {
                                $and: [
                                  { $eq: ["$$batch.artwork.status", true] },
                                  { $eq: ["$$batch.manufacture.status", true] },
                                  { $eq: ["$$batch.testing.status", true] },
                                  { $eq: ["$$batch.dispatch.status", true] }
                                ]
                              },
                              1,
                              0
                            ]
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        ]);
        let packinglist = await packinglistModel.findOne({
          $and: [
            { "client.client_uid": pidata[0].client.client_uid },
            { invoice_uid: { $exists: true } },
            { "products.pi_uid": pi_uid },
            { "products.product_id": product_uid },
            { "products.batch_id": batchId }
          ]
        });
        if (!packinglist) {
          throw new Error("Please generate packinglist and invoice");
        } else {
          if (pidata && pidata[0]) {
            await packinglistModel.findOneAndUpdate(
              {
                $and: [
                  { "client.client_uid": pidata[0].client.client_uid },
                  { "products.pi_uid": pi_uid },
                  { "products.product_id": product_uid }
                ]
              },
              { $set: { "admin_document.CoA_doc": coa_doc } },
              { new: true }
            );
            if (
              parseInt(pidata[0].total_batches) ==
              parseInt(pidata[0].dispatchcount) + 1
            ) {
              status = "ShippedOrder";
              shipped_status = false;
            } else {
              shipped_status = true;
              status = "OpenOrder";
            }
          } else {
            shipped_status = true;
            status = "OpenOrder";
          }
          (artwork = true),
            (manufacturing = true),
            (testing = true),
            (dispatch = true),
            (shipped = false),
            (workstatus = "Dispatch"),
            (greentickType = "dispatch");
          if (files) {
            for (const file of files) {
              statusfiles.push({
                file_name: file.filename,
                uploaded_date: new Date()
              });
            }
          }
        }
      }
      if (greenTick == 6) {
        let pidata = await piModel.aggregate([
          {
            $match: {
              $and: [
                { $or: [{ status: "ShippedOrder" }, { status: "OpenOrder" }] },
                { pi_uid: pi_uid },
                { "products.product_uid": product_uid }
              ]
            }
          },
          {
            $project: {
              client: 1,
              status: 1,
              products: {
                $filter: {
                  input: {
                    $map: {
                      input: "$products",
                      as: "product",
                      in: {
                        batches: {
                          $filter: {
                            input: "$$product.batches",
                            as: "batch",
                            cond: {
                              $and: [
                                { $eq: ["$$batch._id", ObjectId(batchId)] },
                                { $eq: ["$$batch.artwork.status", true] },
                                { $eq: ["$$batch.manufacture.status", true] },
                                { $eq: ["$$batch.testing.status", true] },
                                { $eq: ["$$batch.dispatch.status", true] },
                                { $eq: ["$$batch.shipped.status", false] }
                              ]
                            }
                          }
                        }
                      }
                    }
                  },
                  as: "product",
                  cond: { $gt: [{ $size: "$$product.batches" }, 0] }
                }
              }
            }
          }
        ]);
        if (pidata && pidata[0]) {
          if (pidata[0].products.length > 0) {
            await packinglistModel.findOneAndUpdate(
              {
                $and: [
                  { "client.client_uid": pidata[0].client.client_uid },
                  { "products.pi_uid": pi_uid },
                  { "products.product_id": product_uid },
                  { "products.batch_id": batchId }
                ]
              },
              {
                $set: {
                  status: "OpenInvoice",
                  shipped_onboard_date: shipped_onboard_date,
                  estimated_arrival_date: estimated_arrival_date,
                  payment_due_date: payment_due_date,
                  "workstatus_date.artwork_uploaded_date":
                    pidata[0].products[0].batches[0].artwork.uploaded_date,
                  "workstatus_date.manufacture_uploaded_date":
                    pidata[0].products[0].batches[0].manufacture.uploaded_date,
                  "workstatus_date.testing_uploaded_date":
                    pidata[0].products[0].batches[0].testing.uploaded_date,
                  "workstatus_date.dispatch_uploaded_date":
                    pidata[0].products[0].batches[0].dispatch.uploaded_date,
                  "workstatus_date.shipped_uploaded_date": new Date()
                }
              },
              { new: true }
            );
          } else {
            await packinglistModel.findOneAndUpdate(
              {
                $and: [
                  { "client.client_uid": pidata[0].client.client_uid },
                  { "products.pi_uid": pi_uid },
                  { "products.product_id": product_uid },
                  { "products.batch_id": batchId }
                ]
              },
              {
                $set: {
                  status: "OpenInvoice",
                  shipped_onboard_date: shipped_onboard_date,
                  estimated_arrival_date: estimated_arrival_date,
                  payment_due_date: payment_due_date
                }
              },
              { new: true }
            );
          }
          if (pidata[0].status == "ShippedOrder") {
            shipped_status = false;
            status = "ShippedOrder";
          } else {
            shipped_status = true;
            status = "OpenOrder";
          }
        }
        (artwork = true),
          (manufacturing = true),
          (testing = true),
          (dispatch = true),
          (shipped = true),
          (workstatus = "Shipped"),
          (greentickType = "shipped");
        if (files) {
          for (const file of files) {
            statusfiles.push({
              file_name: file.filename,
              uploaded_date: new Date()
            });
          }
        }
      }
      let pi;
      if (actiontype == "viewdetails") {
        if (statusfiles.length > 0) {
          pi = await piModel.findOneAndUpdate(
            {
              pi_uid: pi_uid,
              products: {
                $elemMatch: {
                  _id: ObjectId(productId),
                  "batches._id": ObjectId(batchId)
                }
              }
            },
            {
              $set: {
                [`products.$[outer].batches.$[inner].${greentickType}.remark`]: remark,
                [`products.$[outer].batches.$[inner].${greentickType}.CoA_doc`]: coa_doc,
                [`products.$[outer].batches.$[inner].$.${greentickType}.shipped_onboard_date`]: shipped_onboard_date
              },
              $push: {
                [`products.$[outer].batches.$[inner].${greentickType}.files`]: statusfiles
              }
            },
            {
              arrayFilters: [
                { "outer._id": ObjectId(productId) },
                { "inner._id": ObjectId(batchId) }
              ]
            }
          );
        } else {
          pi = await piModel.findOneAndUpdate(
            {
              pi_uid: pi_uid,
              products: {
                $elemMatch: {
                  _id: ObjectId(productId),
                  "batches._id": ObjectId(batchId)
                }
              }
            },
            {
              $set: {
                [`products.$[outer].batches.$[inner].${greentickType}.remark`]: remark,
                [`products.$[outer].batches.$[inner].${greentickType}.CoA_doc`]: coa_doc,
                [`products.$[outer].batches.$[inner].$.${greentickType}.shipped_onboard_date`]: shipped_onboard_date
              }
            },
            {
              arrayFilters: [
                { "outer._id": ObjectId(productId) },
                { "inner._id": ObjectId(batchId) }
              ]
            }
          );
        }
      } else if (actiontype == "back") {
        pi = await piModel.findOneAndUpdate(
          {
            pi_uid: pi_uid,
            products: {
              $elemMatch: {
                _id: ObjectId(productId),
                "batches._id": ObjectId(batchId)
              }
            }
          },
          {
            $set: {
              "products.$[outer].batches.$[inner].artwork.status": artwork,
              "products.$[outer].batches.$[inner].manufacture.status": manufacturing,
              "products.$[outer].batches.$[inner].testing.status": testing,
              "products.$[outer].batches.$[inner].dispatch.status": dispatch,
              "products.$[outer].batches.$[inner].shipped.status": shipped,
              [`products.$[outer].batches.$[inner].${greentickType}.uploaded_date`]: new Date(),
              "products.$[outer].batches.$[inner].workstatus": workstatus,
              "products.$[outer].batches.$[inner].greenTick": greenTick,
              "products.$[outer].batches.$[inner].redTick": redTick,
              status: status,
              shipped_status: shipped_status
            }
          },
          {
            arrayFilters: [
              { "outer._id": ObjectId(productId) },
              { "inner._id": ObjectId(batchId) }
            ]
          }
        );
      } else {
        pi = await piModel.findOneAndUpdate(
          {
            pi_uid: pi_uid,
            products: {
              $elemMatch: {
                _id: ObjectId(productId),
                "batches._id": ObjectId(batchId)
              }
            }
          },
          {
            $set: {
              "products.$[outer].batches.$[inner].artwork.status": artwork,
              "products.$[outer].batches.$[inner].manufacture.status": manufacturing,
              "products.$[outer].batches.$[inner].testing.status": testing,
              "products.$[outer].batches.$[inner].dispatch.status": dispatch,
              "products.$[outer].batches.$[inner].shipped.status": shipped,
              [`products.$[outer].batches.$[inner].${greentickType}.remark`]: remark,
              [`products.$[outer].batches.$[inner].${greentickType}.uploaded_date`]: new Date(),
              "products.$[outer].batches.$[inner].workstatus": workstatus,
              "products.$[outer].batches.$[inner].greenTick": greenTick,
              "products.$[outer].batches.$[inner].redTick": redTick,
              [`products.$[outer].batches.$[inner].${greentickType}.CoA_doc`]: coa_doc,
              [`products.$[outer].batches.$[inner].${greentickType}.shipped_onboard_date`]: shipped_onboard_date,
              status: status,
              shipped_status: shipped_status
            },
            $push: {
              [`products.$[outer].batches.$[inner].${greentickType}.files`]: statusfiles
            }
          },
          {
            arrayFilters: [
              { "outer._id": ObjectId(productId) },
              { "inner._id": ObjectId(batchId) }
            ]
          }
        );
      }
      if (!pi || pi == null) {
        throw new Error("Performa Invoice not updated");
      }
      resolve(pi);
    } catch (err) {
      reject(err);
    }
  });
};

exports.pri_shipping_order_report = ({ customerUid }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let results = [];
      let data_query = {
        $and: [
          { "client.client_uid": customerUid },
          { $or: [{ status: "OpenOrder" }, { status: "ShippedOrder" }] }
        ]
      };
      let productsdata = await piModel.aggregate([
        { $match: data_query },
        { $unwind: { path: "$products", preserveNullAndEmptyArrays: true } },
        {
          $project: {
            client: 1,
            client_po_number: 1,
            date_of_issue_of_po: 1,
            pi_uid: 1,
            createdAt: 1,
            product_uid: "$products.product_uid",
            brand_name: "$products.brand_name",
            dosage_form: "$products.dosage_form",
            supplier: "$products.supplier",
            quantity: "$products.quantity",
            pending_quantity: {
              $sum: {
                $map: {
                  input: "$products.batches",
                  as: "batch",
                  in: {
                    $cond: [
                      {
                        $or: [
                          {
                            $and: [
                              { $eq: ["$$batch.artwork.status", false] },
                              { $eq: ["$$batch.manufacture.status", false] },
                              { $eq: ["$$batch.testing.status", false] },
                              { $eq: ["$$batch.dispatch.status", false] },
                              { $eq: ["$$batch.shipped.status", false] }
                            ]
                          },
                          {
                            $and: [
                              { $eq: ["$$batch.artwork.status", true] },
                              { $eq: ["$$batch.manufacture.status", false] },
                              { $eq: ["$$batch.testing.status", false] },
                              { $eq: ["$$batch.dispatch.status", false] },
                              { $eq: ["$$batch.shipped.status", false] }
                            ]
                          },
                          {
                            $and: [
                              { $eq: ["$$batch.artwork.status", true] },
                              { $eq: ["$$batch.manufacture.status", true] },
                              { $eq: ["$$batch.testing.status", false] },
                              { $eq: ["$$batch.dispatch.status", false] },
                              { $eq: ["$$batch.shipped.status", false] }
                            ]
                          },
                          {
                            $and: [
                              { $eq: ["$$batch.artwork.status", true] },
                              { $eq: ["$$batch.manufacture.status", true] },
                              { $eq: ["$$batch.testing.status", true] },
                              { $eq: ["$$batch.dispatch.status", false] },
                              { $eq: ["$$batch.shipped.status", false] }
                            ]
                          }
                        ]
                      },
                      "$$batch.quantity",
                      0
                    ]
                  }
                }
              }
            },
            rates: "$products.price",
            amount: "$products.amount",
            rates: "$products.price",
            amount: "$products.amount",
            customerCodeNumber: "$products.customerCodeNumber",
            batches: {
              $map: {
                input: "$products.batches",
                as: "batch",
                in: "$$batch.batchNo"
              }
            },
            remarks: {
              $map: {
                input: "$products.batches",
                as: "batch",
                in: { workstatus: "$$batch.workstatus", batchNo: "$$batch.batchNo", shipped_onboard_date: "$$batch.shipped.shipped_onboard_date" }
              }
            }
          }
        }
      ]);
      if (productsdata.length > 0) {
        async.each(
          productsdata,
          async (product, cb) => {
            var piproduct = {
              _id: product._id,
              client_po_number: product.client_po_number,
              client_po_date: product.date_of_issue_of_po,
              pi_number: product.pi_uid,
              pi_date: product.createdAt,
              dosage_form: product.dosage_form,
              supplier: product.supplier,
              product_name: `${product.customerCodeNumber}-${product.brand_name}`,
              client: product.client,
              product_uid: product.product_uid,
              po_qty: product.quantity,
              balance_qty: product.pending_quantity,
              rates: product.rates,
              amount: product.amount,
              remarks: product.remarks,
              batches: product.batches
            };
            let remarksdata = [];
            for (const data of piproduct.remarks) {
              remarksdata.push(data.workstatus === 'Shipped' ? 'Delivered' : data.workstatus === 'Dispatch' ? (data.shipped_onboard_date ? 'Shipped on board' : 'Dispatched') : data.workstatus === 'Testing' ? 'Ready for dispatch' : data.workstatus === 'Manufacture' ? 'Under Testing' : data.workstatus === 'Artwork' ? (data.batchNo ? 'Under Manufacturing' : 'Artwork Ok') : 'Under Artwork')
            }
            piproduct = { ...piproduct, remarks: remarksdata }
            let packinglist = await packinglistModel.aggregate([
              {
                $match: {
                  $and: [
                    { invoice_uid: { $exists: true } },
                    { "products.pi_uid": piproduct.pi_number },
                    { "products.product_id": piproduct.product_uid }
                  ]
                }
              },
              {
                $project: {
                  quantity: {
                    $sum: {
                      $map: {
                        input: "$products",
                        as: "product",
                        in: {
                          $cond: [
                            {
                              $and: [
                                {
                                  $eq: ["$$product.pi_uid", piproduct.pi_number]
                                },
                                {
                                  $eq: [
                                    "$$product.product_id",
                                    piproduct.product_uid
                                  ]
                                }
                              ]
                            },
                            "$$product.quantity",
                            0
                          ]
                        }
                      }
                    }
                  },
                  _id: 0,
                  invoice_date: 1,
                  invoice_uid: 1
                }
              }
            ]);
            let dispatched_quantity = 0,
              invoice_uid = [],
              invoice_date = [];
            async.each(
              packinglist,
              (packing, cb2) => {
                dispatched_quantity += packing.quantity;
                invoice_uid.push(packing.invoice_uid);
                invoice_date.push(packing.invoice_date);
                cb2 ? cb2(null) : null;
              },
              err => {
                piproduct = Object.assign(
                  {
                    dispatched_qty: dispatched_quantity,
                    invoice_no: invoice_uid.join(","),
                    invoice_date: invoice_date.join(",")
                  },
                  piproduct
                );
                results.push(piproduct);
                cb ? cb(null) : null;
              }
            );
          },
          err => {
            resolve({
              success: true,
              data: results,
              message: "Client products returned Successfully"
            });
          }
        );
      } else {
        resolve({
          success: false,
          data: productsdata,
          message: "Client products not returned"
        });
      }
    } catch (err) {
      reject(err);
    }
  });
};

exports.deletepi = ({ pi_uid }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let packinglist = await packinglistModel.findOne({
        "products.pi_uid": pi_uid
      });
      if (packinglist && packinglist.products.length > 0) {
        throw new Error("PI con't be removed, already in used");
      } else {
        await piModel.deleteOne({ pi_uid: pi_uid });
        resolve();
      }
    } catch (err) {
      reject(err);
    }
  });
};

function generateandsentPIdoc(res, pi_uid, sender_type) {
  return new Promise(async (resolve, reject) => {
    try {
      let pidata = await piModel.findOne({ pi_uid: pi_uid });
      if (!pidata || pidata == null) {
        throw new Error("PI does not exists.");
      }
      let customerdata = await customerModel.findOne({
        customer_uid: pidata.client.client_uid
      });
      if (!customerdata || customerdata == null) {
        throw new Error("customer does not exists.");
      }
      let dir = `${path.dirname(
        process.mainModule.filename
      )}/upload/proformainvoice`;
      let pi_document = `proformainvoice_${
        pidata.pi_uid
        }_${new Date().getTime()}.pdf`;
      let invoice = `${dir}/${pi_document}`;
      let total_price = await inWords(pidata.total_price.toFixed(2));
      if (sender_type == "Admin") {
        await piModel.findOneAndUpdate(
          { pi_uid: pi_uid },
          { $set: { pi_document: pi_document } },
          { new: true }
        );
      } else {
        await piModel.findOneAndUpdate(
          { pi_uid: pi_uid },
          { $set: { pi_document: pi_document, sent_pi_to_client: true } },
          { new: true }
        );
      }
      res.render(
        "proformainvoice",
        {
          pi: pidata,
          customerdata: customerdata,
          total_price: total_price,
          moment: moment
        },
        async (err, html) => {
          if (err) {
            console.log(err);
            throw new Error("html is not generated.");
          } else {
            console.log("HTML :", html);
            PDFConverter(
              {
                html: html,
                printDelay: 0,
                fitToPage: false,
                allowLocalFilesAccess: true
              },
              async (err, pdf) => {
                if (err) {
                  console.log("PDF Err :---->", err);
                  throw new Error("pdf is not generated.");
                } else {
                  if (!fs.existsSync(dir)) {
                    fs.mkdirSync(dir);
                  }
                  let stream = fs.createWriteStream(invoice);
                  pdf.stream.pipe(stream);
                  attachments = [
                    {
                      filename: pi_document,
                      contentType: "application/pdf",
                      path: `${path.dirname(
                        process.mainModule.filename
                      )}/upload/proformainvoice/${pi_document}`
                    }
                  ];
                  if (sender_type == "Admin") {
                    await Mailer.send(
                      process.env.ADMINEMAIL,
                      "Proforma Invoice",
                      `Please find the below attachment for ${pidata.pi_uid}`,
                      attachments
                    );
                  } else {
                    await Mailer.send(
                      customerdata.basic_details.email,
                      "Proforma Invoice",
                      `Please find the below attachment for ${pidata.pi_uid}`,
                      attachments
                    );
                  }
                  resolve(pidata);
                }
              }
            );
          }
        }
      );
    } catch (error) {
      reject(error);
    }
  });
}

async function piQuery(query, req) {
  return new Promise(async (resolve, reject) => {
    try {
      let { pilisttype, listviewtype, pageNo, pageSize, search } = query;
      let querydata;
      let data_query = {};
      let _pageNo = parseInt(isNaN(pageNo) ? 1 : pageNo);
      let _pageSize = parseInt(isNaN(pageSize) ? 100 : pageSize);
      let skip = parseInt((_pageNo - 1) * _pageSize);
      if (pilisttype && pilisttype != "" && pilisttype == "pilist") {
        if (search && search != "") {
          querydata = [
            {
              $match: {
                $and: [
                  { $text: { $search: `\"${search}\"` } },
                  { $or: [{ status: "Pending" }, { status: "Approved" }] }
                ]
              }
            },
            {
              $project: {
                client_po_number: 1,
                sent_pi_to_client: 1,
                createdAt: 1,
                pi_uid: 1,
                client: 1,
                date_of_issue_of_po: 1,
                approved_date: 1,
                total_products: { $size: "$products" },
                status: 1,
                pi_document: 1
              }
            },
            {
              $facet: {
                edges: [
                  { $sort: { createdAt: -1 } },
                  { $skip: skip },
                  { $limit: skip + _pageSize }
                ],
                pageInfo: [{ $group: { _id: null, count: { $sum: 1 } } }]
              }
            }
          ];
        } else {
          querydata = [
            {
              $match: { $or: [{ status: "Pending" }, { status: "Approved" }] }
            },
            {
              $project: {
                client_po_number: 1,
                sent_pi_to_client: 1,
                createdAt: 1,
                pi_uid: 1,
                client: 1,
                date_of_issue_of_po: 1,
                approved_date: 1,
                total_products: { $size: "$products" },
                status: 1,
                pi_document: 1
              }
            },
            {
              $facet: {
                edges: [
                  { $sort: { createdAt: -1 } },
                  { $skip: skip },
                  { $limit: skip + _pageSize }
                ],
                pageInfo: [{ $group: { _id: null, count: { $sum: 1 } } }]
              }
            }
          ];
        }
      } else if (
        pilisttype &&
        pilisttype != "" &&
        pilisttype == "orderlistcount"
      ) {
        if (req.decoded.userType == "Admin") {
          data_query = {
            $or: [{ status: "OpenOrder" }, { status: "ShippedOrder" }]
          };
        } else {
          data_query = {
            $and: [
              { "client.client_uid": req.decoded.customer_uid },
              { $or: [{ status: "OpenOrder" }, { status: "ShippedOrder" }] }
            ]
          };
        }
        querydata = [
          { $match: data_query },
          {
            $group: {
              _id: 0,
              openorder_count: {
                $sum: { $cond: [{ $eq: ["$status", "OpenOrder"] }, 1, 0] }
              },
              shippedorder_count: {
                $sum: {
                  $cond: {
                    if: { $eq: ["$status", "ShippedOrder"] },
                    then: {
                      $cond: [
                        {
                          $gt: [
                            {
                              $sum: {
                                $map: {
                                  input: "$products",
                                  as: "product",
                                  in: {
                                    $sum: {
                                      $map: {
                                        input: "$$product.batches",
                                        as: "batch",
                                        in: {
                                          $cond: [
                                            {
                                              $and: [
                                                {
                                                  $eq: [
                                                    "$$batch.dispatch.status",
                                                    true
                                                  ]
                                                },
                                                {
                                                  $eq: [
                                                    "$$batch.shipped.status",
                                                    false
                                                  ]
                                                }
                                              ]
                                            },
                                            1,
                                            0
                                          ]
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            0
                          ]
                        },
                        1,
                        0
                      ]
                    },
                    else: 0
                  }
                }
              },
              shippedcount: {
                $sum: {
                  $cond: {
                    if: { $eq: ["$status", "OpenOrder"] },
                    then: {
                      $cond: [
                        {
                          $gt: [
                            {
                              $sum: {
                                $map: {
                                  input: "$products",
                                  as: "product",
                                  in: {
                                    $sum: {
                                      $map: {
                                        input: "$$product.batches",
                                        as: "batch",
                                        in: {
                                          $cond: [
                                            {
                                              $and: [
                                                {
                                                  $eq: [
                                                    "$$batch.dispatch.status",
                                                    true
                                                  ]
                                                },
                                                {
                                                  $eq: [
                                                    "$$batch.shipped.status",
                                                    false
                                                  ]
                                                }
                                              ]
                                            },
                                            1,
                                            0
                                          ]
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            0
                          ]
                        },
                        1,
                        0
                      ]
                    },
                    else: 0
                  }
                }
              }
            }
          },
          {
            $project: {
              _id: 0,
              OpenOrder: "$openorder_count",
              ShippedOrder: { $sum: ["$shippedorder_count", "$shippedcount"] }
            }
          }
        ];
      } else if (
        pilisttype &&
        pilisttype != "" &&
        pilisttype == "openorderlist" &&
        listviewtype &&
        listviewtype != "" &&
        listviewtype == "poview"
      ) {
        if (req.decoded.userType == "Admin") {
          data_query = { status: "OpenOrder" };
        } else {
          data_query = {
            $and: [
              { "client.client_uid": req.decoded.customer_uid },
              { status: "OpenOrder" }
            ]
          };
        }
        querydata = [
          { $match: data_query },
          {
            $project: {
              client: 1,
              client_product_code: 1,
              customerCodeNumber: 1,
              client_po_number: 1,
              pi_uid: 1,
              createdAt: 1,
              date_of_issue_of_po: 1,
              estimated_dispatch_date: 1,
              total_products: { $size: "$products" },
              status: 1,
              artworkcount: {
                $sum: {
                  $map: {
                    input: "$products",
                    as: "product",
                    in: {
                      $sum: {
                        $map: {
                          input: "$$product.batches",
                          as: "batch",
                          in: {
                            $cond: [
                              {
                                $and: [
                                  { $eq: ["$$batch.artwork.status", false] },
                                  {
                                    $eq: ["$$batch.manufacture.status", false]
                                  },
                                  { $eq: ["$$batch.testing.status", false] },
                                  { $eq: ["$$batch.dispatch.status", false] },
                                  { $eq: ["$$batch.shipped.status", false] }
                                ]
                              },
                              1,
                              0
                            ]
                          }
                        }
                      }
                    }
                  }
                }
              },
              manufacturecount: {
                $sum: {
                  $map: {
                    input: "$products",
                    as: "product",
                    in: {
                      $sum: {
                        $map: {
                          input: "$$product.batches",
                          as: "batch",
                          in: {
                            $cond: [
                              {
                                $and: [
                                  { $eq: ["$$batch.artwork.status", true] },
                                  {
                                    $eq: ["$$batch.manufacture.status", false]
                                  },
                                  { $eq: ["$$batch.testing.status", false] },
                                  { $eq: ["$$batch.dispatch.status", false] },
                                  { $eq: ["$$batch.shipped.status", false] }
                                ]
                              },
                              1,
                              0
                            ]
                          }
                        }
                      }
                    }
                  }
                }
              },
              testingcount: {
                $sum: {
                  $map: {
                    input: "$products",
                    as: "product",
                    in: {
                      $sum: {
                        $map: {
                          input: "$$product.batches",
                          as: "batch",
                          in: {
                            $cond: [
                              {
                                $and: [
                                  { $eq: ["$$batch.artwork.status", true] },
                                  { $eq: ["$$batch.manufacture.status", true] },
                                  { $eq: ["$$batch.testing.status", false] },
                                  { $eq: ["$$batch.dispatch.status", false] },
                                  { $eq: ["$$batch.shipped.status", false] }
                                ]
                              },
                              1,
                              0
                            ]
                          }
                        }
                      }
                    }
                  }
                }
              },
              dispatchcount: {
                $sum: {
                  $map: {
                    input: "$products",
                    as: "product",
                    in: {
                      $sum: {
                        $map: {
                          input: "$$product.batches",
                          as: "batch",
                          in: {
                            $cond: [
                              {
                                $and: [
                                  { $eq: ["$$batch.artwork.status", true] },
                                  { $eq: ["$$batch.manufacture.status", true] },
                                  { $eq: ["$$batch.testing.status", true] },
                                  { $eq: ["$$batch.dispatch.status", false] },
                                  { $eq: ["$$batch.shipped.status", false] }
                                ]
                              },
                              1,
                              0
                            ]
                          }
                        }
                      }
                    }
                  }
                }
              },
              shippedcount: {
                $sum: {
                  $map: {
                    input: "$products",
                    as: "product",
                    in: {
                      $sum: {
                        $map: {
                          input: "$$product.batches",
                          as: "batch",
                          in: {
                            $cond: [
                              {
                                $and: [
                                  { $eq: ["$$batch.artwork.status", true] },
                                  { $eq: ["$$batch.manufacture.status", true] },
                                  { $eq: ["$$batch.testing.status", true] },
                                  { $eq: ["$$batch.dispatch.status", true] },
                                  { $eq: ["$$batch.shipped.status", false] }
                                ]
                              },
                              1,
                              0
                            ]
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          },
          {
            $facet: {
              edges: [
                { $sort: { createdAt: -1 } },
                { $skip: skip },
                { $limit: _pageSize }
              ],
              pageInfo: [{ $group: { _id: null, count: { $sum: 1 } } }]
            }
          }
        ];
      } else if (
        pilisttype &&
        pilisttype != "" &&
        pilisttype == "openorderlist" &&
        listviewtype &&
        listviewtype != "" &&
        listviewtype == "productview"
      ) {
        if (req.decoded.userType == "Admin") {
          data_query = { status: "OpenOrder" };
        } else {
          data_query = {
            $and: [
              { "client.client_uid": req.decoded.customer_uid },
              { status: "OpenOrder" }
            ]
          };
        }
        querydata = [
          { $match: data_query },
          {
            $project: {
              client: 1,
              pi_uid: 1,
              createdAt: 1,
              client_po_number: 1,
              estimated_dispatch_date: 1,
              status: 1,
              products: {
                $filter: {
                  input: {
                    $map: {
                      input: "$products",
                      as: "product",
                      in: {
                        _id: "$$product._id",
                        product_uid: "$$product.product_uid",
                        quantity: "$$product.quantity",
                        price: "$$product.price",
                        flatDiscount: "$$product.flatDiscount",
                        amount: "$$product.amount",
                        client_product_code: "$$product.client_product_code",
                        customerCodeNumber: "$$product.customerCodeNumber",
                        batches: {
                          $filter: {
                            input: "$$product.batches",
                            as: "batch",
                            cond: {
                              $or: [
                                {
                                  $and: [
                                    { $eq: ["$$batch.artwork.status", false] },
                                    {
                                      $eq: ["$$batch.manufacture.status", false]
                                    },
                                    { $eq: ["$$batch.testing.status", false] },
                                    { $eq: ["$$batch.dispatch.status", false] },
                                    { $eq: ["$$batch.shipped.status", false] }
                                  ]
                                },
                                {
                                  $and: [
                                    { $eq: ["$$batch.artwork.status", true] },
                                    {
                                      $eq: ["$$batch.manufacture.status", false]
                                    },
                                    { $eq: ["$$batch.testing.status", false] },
                                    { $eq: ["$$batch.dispatch.status", false] },
                                    { $eq: ["$$batch.shipped.status", false] }
                                  ]
                                },
                                {
                                  $and: [
                                    { $eq: ["$$batch.artwork.status", true] },
                                    {
                                      $eq: ["$$batch.manufacture.status", true]
                                    },
                                    { $eq: ["$$batch.testing.status", false] },
                                    { $eq: ["$$batch.dispatch.status", false] },
                                    { $eq: ["$$batch.shipped.status", false] }
                                  ]
                                },
                                {
                                  $and: [
                                    { $eq: ["$$batch.artwork.status", true] },
                                    {
                                      $eq: ["$$batch.manufacture.status", true]
                                    },
                                    { $eq: ["$$batch.testing.status", true] },
                                    { $eq: ["$$batch.dispatch.status", false] },
                                    { $eq: ["$$batch.shipped.status", false] }
                                  ]
                                }
                              ]
                            }
                          }
                        }
                      }
                    }
                  },
                  as: "product",
                  cond: { $gt: [{ $size: "$$product.batches" }, 0] }
                }
              }
            }
          },
          {
            $facet: {
              edges: [
                { $sort: { createdAt: -1 } },
                { $skip: skip },
                { $limit: skip + _pageSize }
              ],
              pageInfo: [{ $group: { _id: null, count: { $sum: 1 } } }]
            }
          }
        ];
      } else if (
        pilisttype &&
        pilisttype != "" &&
        pilisttype == "shippedorderlist" &&
        listviewtype &&
        listviewtype != "" &&
        listviewtype == "poview"
      ) {
        if (req.decoded.userType == "Admin") {
          data_query = {
            $or: [{ status: "ShippedOrder" }, { status: "OpenOrder" }]
          };
        } else {
          data_query = {
            $and: [
              { "client.client_uid": req.decoded.customer_uid },
              { $or: [{ status: "ShippedOrder" }, { status: "OpenOrder" }] }
            ]
          };
        }
        querydata = [
          { $match: data_query },
          {
            $project: {
              client: 1,
              client_product_code: 1,
              customerCodeNumber: 1,
              createdAt: 1,
              client_po_number: 1,
              pi_uid: 1,
              date_of_issue_of_po: 1,
              estimated_dispatch_date: 1,
              total_products: { $size: "$products" },
              status: 1,
              products: {
                $filter: {
                  input: {
                    $map: {
                      input: "$products",
                      as: "product",
                      in: {
                        _id: "$$product._id",
                        product_uid: "$$product.product_uid",
                        client_product_code: "$$product.client_product_code",
                        quantity: "$$product.quantity",
                        price: "$$product.price",
                        flatDiscount: "$$product.flatDiscount",
                        customerCodeNumber: "$$product.customerCodeNumber",
                        complete_packaging_profile:
                          "$$product.complete_packaging_profile",
                        unit_of_billing: "$$product.unit_of_billing",
                        registration_no: "$$product.registration_no",
                        registration_date: "$$product.registration_date",
                        shelf_life: "$$product.shelf_life",
                        frieght: "$$product.frieght",
                        insurance: "$$product.insurance",
                        hsn_code: "$$product.hsn_code",
                        minimum_batch_size: "$$product.minimum_batch_size",
                        maximum_batch_size: "$$product.maximum_batch_size",
                        shipper_size_inner_dimension:
                          "$$product.shipper_size_inner_dimension",
                        batches: {
                          $filter: {
                            input: "$$product.batches",
                            as: "batch",
                            cond: {
                              $and: [
                                { $eq: ["$$batch.artwork.status", true] },
                                { $eq: ["$$batch.manufacture.status", true] },
                                { $eq: ["$$batch.testing.status", true] },
                                { $eq: ["$$batch.dispatch.status", true] },
                                { $eq: ["$$batch.shipped.status", false] }
                              ]
                            }
                          }
                        }
                      }
                    }
                  },
                  as: "product",
                  cond: { $gt: [{ $size: "$$product.batches" }, 0] }
                }
              }
            }
          },
          { $match: { $expr: { $gt: [{ $size: "$products" }, 0] } } },
          {
            $facet: {
              edges: [
                { $sort: { createdAt: -1 } },
                { $skip: skip },
                { $limit: skip + _pageSize }
              ],
              pageInfo: [{ $group: { _id: null, count: { $sum: 1 } } }]
            }
          }
        ];
      } else if (
        pilisttype &&
        pilisttype != "" &&
        pilisttype == "shippedorderlist" &&
        listviewtype &&
        listviewtype != "" &&
        listviewtype == "productview"
      ) {
        if (req.decoded.userType == "Admin") {
          data_query = {
            $or: [{ status: "ShippedOrder" }, { status: "OpenOrder" }]
          };
        } else {
          data_query = {
            $and: [
              { "client.client_uid": req.decoded.customer_uid },
              { $or: [{ status: "ShippedOrder" }, { status: "OpenOrder" }] }
            ]
          };
        }
        querydata = [
          { $match: data_query },
          {
            $project: {
              client: 1,
              pi_uid: 1,
              createdAt: 1,
              client_po_number: 1,
              estimated_dispatch_date: 1,
              status: 1,
              products: {
                $filter: {
                  input: {
                    $map: {
                      input: "$products",
                      as: "product",
                      in: {
                        _id: "$$product._id",
                        product_uid: "$$product.product_uid",
                        quantity: "$$product.quantity",
                        price: "$$product.price",
                        flatDiscount: "$$product.flatDiscount",
                        amount: "$$product.amount",
                        client_product_code: "$$product.client_product_code",
                        customerCodeNumber: "$$product.customerCodeNumber",
                        batches: {
                          $filter: {
                            input: "$$product.batches",
                            as: "batch",
                            cond: {
                              $and: [
                                { $eq: ["$$batch.artwork.status", true] },
                                { $eq: ["$$batch.manufacture.status", true] },
                                { $eq: ["$$batch.testing.status", true] },
                                { $eq: ["$$batch.dispatch.status", true] },
                                { $eq: ["$$batch.shipped.status", false] }
                              ]
                            }
                          }
                        }
                      }
                    }
                  },
                  as: "product",
                  cond: { $gt: [{ $size: "$$product.batches" }, 0] }
                }
              }
            }
          },
          { $match: { $expr: { $gt: [{ $size: "$products" }, 0] } } },
          {
            $facet: {
              edges: [
                { $sort: { createdAt: -1 } },
                { $skip: skip },
                { $limit: skip + _pageSize }
              ],
              pageInfo: [{ $group: { _id: null, count: { $sum: 1 } } }]
            }
          }
        ];
      } else {
        querydata = [
          { $match: { $or: [{ status: "Pending" }, { status: "Approved" }] } },
          {
            $project: {
              client_po_number: 1,
              pi_uid: 1,
              createdAt: 1,
              client: 1,
              date_of_issue_of_po: 1,
              total_products: { $size: "$products" },
              status: 1
            }
          },
          {
            $facet: {
              edges: [
                { $sort: { createdAt: -1 } },
                { $skip: skip },
                { $limit: skip + _pageSize }
              ],
              pageInfo: [{ $group: { _id: null, count: { $sum: 1 } } }]
            }
          }
        ];
      }
      resolve(querydata);
    } catch (err) {
      reject(err);
    }
  });
}

async function inWords(num) {
  var a = [
    "",
    "One ",
    "Two ",
    "Three ",
    "Four ",
    "Five ",
    "Six ",
    "Seven ",
    "Eight ",
    "Nine ",
    "Ten ",
    "Eleven ",
    "Twelve ",
    "Thirteen ",
    "Fourteen ",
    "Fifteen ",
    "Sixteen ",
    "Seventeen ",
    "Sighteen ",
    "Nineteen "
  ];
  var b = [
    "",
    "",
    "Twenty",
    "Thirty",
    "Forty",
    "Fifty",
    "Sixty",
    "Seventy",
    "Eighty",
    "Ninety"
  ];
  if ((num = num.toString()).length > 9) return "overflow";
  var numarray = num.toString().split(".");
  num = numarray[0];
  dot = numarray[1];
  n = ("000000000" + num)
    .substr(-9)
    .match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
  d = ("000" + dot).substr(-2).match(/^(\d{2})$/);
  if (!n) return;
  var str = "";
  str +=
    n[1] != 0
      ? (a[Number(n[1])] || b[n[1][0]] + " " + a[n[1][1]]) + "crore "
      : "";
  str +=
    n[2] != 0
      ? (a[Number(n[2])] || b[n[2][0]] + " " + a[n[2][1]]) + "lakh "
      : "";
  str +=
    n[3] != 0
      ? (a[Number(n[3])] || b[n[3][0]] + " " + a[n[3][1]]) + "thousand "
      : "";
  str +=
    n[4] != 0
      ? (a[Number(n[4])] || b[n[4][0]] + " " + a[n[4][1]]) + "hundred "
      : "";
  str += n[5] != 0 ? a[Number(n[5])] || b[n[5][0]] + " " + a[n[5][1]] : "";
  str +=
    d[1] != 0
      ? (d[1] != "" ? "And " : "") +
      (a[Number(d[1])] || b[d[1][0]] + " " + a[d[1][1]]) +
      "Cent Only "
      : "Only";
  return str;
}
