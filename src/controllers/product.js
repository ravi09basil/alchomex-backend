const productModel = require('../models/product');
const generateuid = require('../services/generateuid')
const piModel = require('../models/pi');
const async = require('async');
const path = require('path');
const fs = require('fs');
const XLSX = require('xlsx');
var mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;


exports.create_registered_products = ({ file }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let product_number = 0;
      const dir = `${path.dirname(process.mainModule.filename)}/upload/other`;
      const coolPath = dir + '/' + file;
      const workbook = XLSX.readFile(coolPath);
      const sheet_name_list = workbook.SheetNames;
      XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);
      let jsonArray = XLSX.utils.sheet_to_json(
        workbook.Sheets[sheet_name_list[0]]
      );
      jsonArray = jsonArray.splice(1);
      let product_data = await productModel.find().sort('-product_number').limit(1).exec();
      if (product_data.length > 0) {
        product_number = product_data[0].product_number;
      }
      let docs = await addProductToDatabase(jsonArray, product_number);
      if (!docs || docs == null) {
        throw new Error('Data not loaded.');
      }
      resolve(docs)
    } catch (err) {
      reject(err)
    }
  })
}


const addProductToDatabase = async (jsonArray, product_number) => {
  let newJsonArray = []; let product_uid = 'RP-00001';
  await Promise.all(
    jsonArray.map(item => {
      if ((product_number + '').length < 6) {
        product_number = product_number + 1;
        product_uid = 'RP-' + pad(product_number, 5);
      } else {
        product_number = product_number + 1;
        product_uid = 'RP-' + product_number;
      }
      newJsonArray.push({
        product_number, product_uid, serial_no: item['Sr. No.'],
        type_of_product: item['Pharma or Food'], brand_name: item['Brand Name'], generic_name: item['Generic Name'],
        packaging: item['Packing'], unit_of_billing: item['Unit of billing'], active_ingredients: item['Active ingredients'],
        label_claim: item['Label claim'], therapeutic_category: item['Therapeutic Category '], speciality: item['Speciality'],
        dosage_form: item['Dosage form '], supplier: item['Supplier'], customer_uid: item['Customer UID'],
        customer_code_number: item['Customer Code Number'], complete_packaging_profile: item['Complete Packing Profile'],
        primary_packing: item['Primary packing '], secondary_carton_packing: (item['Secondary packing / Carton packing']) ? item['Secondary packing / Carton packing'] : 1,
        inner_carton_shrink_packing: item['Inner Carton / Shrink packing'], shipping_packing: item['Shipper packing'],
        shipper_size_inner_dimension: item['Shipper size (Inner Dimension)'], purchase_rs: item['PURCHASE Rs'],
        insurance: item['INSURANCE'] ? item['INSURANCE'] : 0, frieght: item['FREIGHT'], interest: item['INTEREST'],
        admin_cost: item['ADMIN COST'], total_rs: item['TOTAL Rs'], ex_rate: item['EX RATE'], cost_price: item['Cost Price'],
        sales_price: item['Sales Price'], hsn_code: item['HSN CODE'], registration_no: item['Registration No.'], registration_date: item['Registration date'],
        shelf_life: item['Shelf Life'], biequivalvence: item['Bioequ-ivalence'], minimum_batch_size: (item['Minimum Batch Size']) ? item['Minimum Batch Size'] : 0,
        maximum_batch_size: (item['Maximum Batch Size']) ? item['Maximum Batch Size'] : 0, first_supply: item['First Supply'], first_year: {
          projection: item['1st year'], sales: item['__EMPTY']
        },
        second_year: { projection: item['2nd year'], sales: item['__EMPTY_1'] },
        third_year: { projection: item['3rd year'], sales: item['__EMPTY_2'] },
        artwork_barcode: { tube_label_foil: item['Artwork Barcode / date of approval'], carton: item['__EMPTY_3'], leaflet: item['__EMPTY_4'], shipper: item['__EMPTY_5'] }
      });
    })
  );
  return productModel.insertMany(newJsonArray);
};

function pad(num, size) {
  return ('000000000' + num).substr(-size);
}

exports.update_registered_product = ({ file }) => {
  return new Promise(async (resolve, reject) => {
    try {
      const dir = `${path.dirname(process.mainModule.filename)}/upload/other`;
      const coolPath = dir + '/' + file;
      const workbook = XLSX.readFile(coolPath);
      const sheet_name_list = workbook.SheetNames;
      XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);
      let jsonArray = XLSX.utils.sheet_to_json(
        workbook.Sheets[sheet_name_list[0]]
      );
      jsonArray = jsonArray.splice(1);
      let docs = await updateProductToDatabase(jsonArray);
      if (!docs || docs == null) {
        throw new Error('Data not loaded.');
      }
      resolve(docs)
    } catch (err) {
      reject(err)
    }
  });
}

const updateProductToDatabase = async (jsonArray) => {
  return new Promise(async (resolve, reject) => {
    try {
      async.eachSeries(jsonArray, async (product, cb) => {
        let productdata = {
          type_of_product: product['Pharma or Food'], brand_name: product['Brand Name'], generic_name: product['Generic Name'],
          packaging: product['Packing'], unit_of_billing: product['Unit of billing'], active_ingredients: product['Active ingredients'],
          label_claim: product['Label claim'], therapeutic_category: product['Therapeutic Category '], speciality: product['Speciality'],
          dosage_form: product['Dosage form '], supplier: product['Supplier'], customer_uid: product['Customer UID'],
          customer_code_number: product['Customer Code Number'], complete_packaging_profile: product['Complete Packing Profile'],
          primary_packing: product['Primary packing '], secondary_carton_packing: (product['Secondary packing / Carton packing']) ? product['Secondary packing / Carton packing'] : 1,
          inner_carton_shrink_packing: product['Inner Carton / Shrink packing'], shipping_packing: product['Shipper packing'],
          shipper_size_inner_dimension: product['Shipper size (Inner Dimension)'], purchase_rs: product['PURCHASE Rs'],
          insurance: product['INSURANCE'] ? product['INSURANCE'] : 0, frieght: product['FREIGHT'], interest: product['INTEREST'],
          admin_cost: product['ADMIN COST'], total_rs: product['TOTAL Rs'], ex_rate: product['EX RATE'], cost_price: product['Cost Price'],
          sales_price: product['Sales Price'], hsn_code: product['HSN CODE'], registration_no: product['Registration No.'], registration_date: product['Registration date'],
          shelf_life: product['Shelf Life'], biequivalvence: product['Bioequ-ivalence'], minimum_batch_size: (product['Minimum Batch Size']) ? product['Minimum Batch Size'] : 0,
          maximum_batch_size: (product['Maximum Batch Size']) ? product['Maximum Batch Size'] : 0, first_supply: product['First Supply'],
          first_year: { projection: product['1st year'], sales: product['__EMPTY'] },
          second_year: { projection: product['2nd year'], sales: product['__EMPTY_1'] },
          third_year: { projection: product['3rd year'], sales: product['__EMPTY_2'] },
          artwork_barcode: { tube_label_foil: product['Artwork Barcode / date of approval'], carton: product['__EMPTY_3'], leaflet: product['__EMPTY_4'], shipper: product['__EMPTY_5'] }
        }
        let updatedProduct = await productModel.update({ serial_no: product['Sr. No.'] }, { $set: productdata });
        if (updatedProduct && updatedProduct.nModified === 0) {
          let pro_data = await productModel.find().sort('-product_number').limit(1).exec();
          let { product_uid, product_number } = await generateuid.generateuidtypes({ uiddata: pro_data, uidtype: 'product' })
          productdata = { product_number, product_uid, product_type: 1, serial_no: product['Sr. No.'], ...productdata }
          const productObj = new productModel(productdata);
          await productObj.save();
        }
        cb ? cd(null) : null;
      }, (err) => {
        if (err) {
          throw new Error(err);
        } else {
          resolve(jsonArray);
        }
      })
    } catch (err) {
      reject(err);
    }
  });
};

exports.createSingle_Product = ({ productbody, file }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let serial = await productModel.find().sort('-serial_no').limit(1).exec();
      let pro_data = await productModel.find().sort('-product_number').limit(1).exec();
      let { product_uid, product_number } = await generateuid.generateuidtypes({ uiddata: pro_data, uidtype: 'product' })
      let body = JSON.parse(productbody.data);
      const productObj = new productModel({
        product_number, product_uid, product_type: 1,
        serial_no: (serial[0].serial_no) ? serial[0].serial_no : 1,
        type_of_product: body.type_of_product,
        brand_name: body.brand_name,
        generic_name: body.generic_name,
        packaging: body.packaging,
        unit_of_billing: body.unit_of_billing,
        active_ingredients: body.active_ingredients,
        label_claim: body.label_claim,
        therapeutic_category: body.therapeutic_category,
        speciality: body.speciality,
        dosage_form: body.dosage_form,
        supplier: body.supplier,
        customer_uid: body.customer_uid,
        customer_code_number: body.customer_code_number,
        complete_packaging_profile: body.complete_packaging_profile,
        primary_packing: body.primary_packing,
        secondary_carton_packing: body.secondary_carton_packing,
        inner_carton_shrink_packing: body.inner_carton_shrink_packing,
        shipping_packing: body.shipping_packing,
        shipper_size_inner_dimension: body.shipper_size_inner_dimension,
        purchase_rs: body.purchase_rs,
        shipping_volume_in_cbm: body.shipping_volume_in_cbm,
        minimum_batch_size: (body.minimum_batch_size) ? body.minimum_batch_size : 0,
        maximum_batch_size: (body.maximum_batch_size) ? body.maximum_batch_size : 0,
        frieght: body.frieght,
        interest: body.interest,
        insurance: body.insurance,
        admin_cost: body.admin_cost,
        total_rs: body.total_rs,
        ex_rate: body.ex_rate,
        cost_price: body.cost_price,
        sales_price: body.sales_price,
        hsn_code: body.hsn_code,
        registration_no: body.registration_no,
        registration_date: body.registration_date,
        shelf_life: body.shelf_life,
        biequivalvence: body.biequivalvence,
        first_supply: body.first_supply,
        first_year: body.first_year,
        second_year: body.second_year,
        third_year: body.third_year,
        artwork_barcode: body.artwork_barcode,
        artwork_doc: file.filename
      });
      let products = await productModel.findOne({ customer_code_number: body.customer_code_number }, { product_uid: 1 });
      if (products) {
        throw new Error('Product already exists')
      }
      let product = await productObj.save()
      if (!product || product == null) {
        throw new Error('Product not saved.')
      }
      resolve(product);
    } catch (err) {
      reject(err)
    }
  });
}

exports.get_all_products = ({ query }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let { customeruid, pageNo, pageSize, search } = query; let querydata = {};
      let _pageNo = parseInt(isNaN(pageNo) ? 1 : pageNo);
      let _pageSize = parseInt(isNaN(pageSize) ? 100 : pageSize);
      let skip = parseInt((_pageNo - 1) * _pageSize);
      let sort = { customer_code_number: 1 };
      if (customeruid) {
        if (search && search != '') {
          querydata = { $and: [{ $text: { $search: `\"${search}\"` } }, { customer_uid: customeruid }] };
        } else {
          querydata = { customer_uid: customeruid };
        }
      } else if (search && search != '') {
        querydata = { $text: { $search: `\"${search}\"` } };
      }
      let _count = await productModel.countDocuments(querydata);
      let products = await productModel.find(querydata)
        .sort(sort)
        .skip(skip)
        .limit(_pageSize);
      resolve({ success: true, data: products, count: _count, message: 'Products fetched Successfully' });
    } catch (err) {
      reject(err);
    }
  });
}

exports.getOne_product = ({ product_uid }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let product = await productModel.findOne({ product_uid: product_uid });
      if (!product || product == null) {
        throw new Error('Product does not exist.');
      }
      resolve(product);
    } catch (err) {
      reject(err);
    }
  });
}

exports.update_product = ({ productid, productbody, file }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let body = JSON.parse(productbody.data);
      let product_data = {
        type_of_product: body.type_of_product,
        brand_name: body.brand_name,
        generic_name: body.generic_name,
        packaging: body.packaging,
        unit_of_billing: body.unit_of_billing,
        active_ingredients: body.active_ingredients,
        label_claim: body.label_claim,
        therapeutic_category: body.therapeutic_category,
        speciality: body.speciality,
        dosage_form: body.dosage_form,
        supplier: body.supplier,
        customer_uid: body.customer_uid,
        customer_code_number: body.customer_code_number,
        complete_packaging_profile: body.complete_packaging_profile,
        primary_packing: body.primary_packing,
        secondary_carton_packing: body.secondary_carton_packing,
        inner_carton_shrink_packing: body.inner_carton_shrink_packing,
        shipping_packing: body.shipping_packing,
        shipper_size_inner_dimension: body.shipper_size_inner_dimension,
        purchase_rs: body.purchase_rs,
        shipping_volume_in_cbm: body.shipping_volume_in_cbm,
        minimum_batch_size: (body.minimum_batch_size) ? body.minimum_batch_size : 0,
        maximum_batch_size: (body.maximum_batch_size) ? body.maximum_batch_size : 0,
        frieght: body.frieght,
        interest: body.interest,
        insurance: (body.insurance) ? body.insurance : 0,
        admin_cost: body.admin_cost,
        total_rs: body.total_rs,
        ex_rate: body.ex_rate,
        cost_price: body.cost_price,
        sales_price: body.sales_price,
        hsn_code: body.hsn_code,
        registration_no: body.registration_no,
        registration_date: body.registration_date,
        shelf_life: body.shelf_life,
        biequivalvence: body.biequivalvence,
        first_supply: body.first_supply,
        first_year: body.first_year,
        second_year: body.second_year,
        third_year: body.third_year,
        artwork_barcode: body.artwork_barcode
      };
      if (file && file.filename) {
        let oldproduct = await productModel.findOne({ _id: ObjectId(productid) }).select('artwork_doc')
        if (oldproduct && oldproduct.artwork_doc) {
          const dir = `${path.dirname(process.mainModule.filename)}/upload/product_artwork_doc`
          var docpath = dir + '/' + oldproduct.artwork_doc;
          fs.unlinkSync(docpath);
        }
        product_data = Object.assign({ artwork_doc: file.filename }, product_data);
      }
      let product = await productModel.findByIdAndUpdate(productid, { $set: product_data }, { new: true })
      if (!product || product == null) {
        throw new Error('Product not found.');
      }
      resolve(product);
    } catch (err) {
      reject(err);
    }
  });
};

exports.delete_product = ({ product_uid }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let pi_data = await piModel.find({ 'products.product_uid': product_uid });
      if (pi_data.length > 0) {
        throw new Error("Product con't be removed, already in used");
      } else {
        let product = await productModel.deleteOne({ product_uid: product_uid })
        if (product.deletedCount == 0) {
          throw new Error("Product is not removed");
        } else {
          resolve()
        }
      }
    } catch (err) {
      reject(err);
    }
  });
};
