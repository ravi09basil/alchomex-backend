const ServiceInvoiceModel = require("../models/serviceinvoice");
const generateuid = require("../services/generateuid");
// const PDFConverter = require("phantom-html-to-pdf")({});

var PDFConverter = require("html-pdf");
let width = 794,
  height = 1122;

var options = {
  width: `${width}px`,
  height: `${height}px`,
  border: "5px",
  viewportSize: {
    width,
    height
  }
};

const customerModel = require("../models/customer");
const Mailer = require("../services/mailer");
var moment = require("moment");
const path = require("path");
const fs = require("fs");

exports.create_service_invoice = ({ res, body }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let si_data = await ServiceInvoiceModel.find({})
        .sort("-si_number")
        .limit(1)
        .exec();
      let { si_uid, si_number } = await generateuid.generateuidtypes({
        uiddata: si_data,
        uidtype: "si"
      });
      const serviceInvoiceObj = new ServiceInvoiceModel({
        si_uid: si_uid,
        si_number: si_number,
        client: body.client,
        bill_no: body.bill_no,
        description_of_incident: body.description_of_incident,
        quantity: body.quantity,
        rate: body.rate,
        invoice_amount: body.invoice_amount
      });
      let si = await serviceInvoiceObj.save();
      if (!si) {
        throw new Error("Service Invoice not created ...");
      } else {
        await generateandsentSIdoc(res, si.si_uid, "Admin");
      }
      resolve(si);
    } catch (err) {
      reject(err);
    }
  });
};

exports.getallserviceinvoices = ({ query, req }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let { pageNo, pageSize, search, actionType } = query; let querydata = {};
      let _pageNo = parseInt(isNaN(pageNo) ? 1 : pageNo);
      let _pageSize = parseInt(isNaN(pageSize) ? 10 : pageSize);
      let skip = parseInt((_pageNo - 1) * _pageSize);
      let sort = { createdAt: -1 };
      if (req.decoded.userType == "Admin") {
        if (search && search != "") {
          querydata = {
            $and: [{ $text: { $search: `\"${search}\"` } }, {
              status: actionType == "Edit" ? "Pending" : actionType == "PostInvoice" ? "Approved" : "Pending"
            }]
          };
        } else {
          querydata = {
            status: actionType == "Edit" ? "Pending" : actionType == "PostInvoice" ? "Approved" : "Pending"
          };
        }
      } else {
        if (search && search != "") {
          querydata = {
            $and: [{ $text: { $search: `\"${search}\"` } },
            { "client.client_uid": req.decoded.customer_uid },
            { status: actionType == "Edit" ? "Pending" : actionType == "PostInvoice" ? "Approved" : "Pending" }]
          };
        } else {
          querydata = {
            $and: [{ "client.client_uid": req.decoded.customer_uid },
            {
              status: actionType == "Edit" ? "Pending" : actionType == "PostInvoice" ? "Approved" : "Pending"
            }]
          };
        }
      }
      let _count = await ServiceInvoiceModel.countDocuments(querydata);
      let serviceinvoices = await ServiceInvoiceModel.aggregate([
        { $match: querydata },
        {
          $lookup: {
            from: "service_invoice_payments",
            let: {
              si_uid: "$si_uid",
              payment_UID: "$payment_UID",
              payment_number: "$payment_number",
              createdAt: "$createdAt"
            },
            pipeline: [
              {
                $match: {
                  $expr: { $eq: ["$si_uid", "$$si_uid"] }
                }
              },
              {
                $group: {
                  _id: "$si_uid",
                  payment_UID: { $first: "$payment_UID" },
                  payment_number: { $first: "$payment_number" },
                  UTR_number: { $first: "$UTR_number" },
                  createdAt: { $first: "$createdAt" },
                  totalDonePayment: { $sum: "$amount_received" }
                }
              }
            ],
            as: "siPayment_data"
          }
        },
        {
          $project: {
            invoice_amount: 1,
            quantity: 1,
            rate: 1,
            status: 1,
            sent_si_to_client: 1,
            si_uid: 1,
            si_number: 1,
            client: 1,
            bill_no: 1,
            description_of_incident: 1,
            createdAt: 1,
            si_document: 1,
            siPayment_data: 1,
            signed_serviceinvoice_doc: 1,
            payment_done: {
              $arrayElemAt: ["$siPayment_data.totalDonePayment", 0]
            }
          }
        },
        {
          $project: {
            invoice_amount: 1,
            quantity: 1,
            rate: 1,
            status: 1,
            sent_si_to_client: 1,
            si_uid: 1,
            si_number: 1,
            client: 1,
            bill_no: 1,
            description_of_incident: 1,
            createdAt: 1,
            si_document: 1,
            siPayment_data: 1,
            payment_done: 1,
            signed_serviceinvoice_doc: 1,
            total_pending_amount: {
              $subtract: ["$invoice_amount", "$payment_done"]
            }
          }
        }
      ])
        .sort(sort)
        .skip(skip)
        .limit(_pageSize);
      resolve({
        success: true,
        data: serviceinvoices,
        count: _count,
        message: "Service invoices fetched successfully."
      });
    } catch (err) {
      reject(err);
    }
  });
};

exports.uploadsignedsidoc = ({ si_uid, file }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let si = await ServiceInvoiceModel.findOneAndUpdate(
        { si_uid: si_uid },
        {
          $set: { signed_serviceinvoice_doc: file.filename, status: "Approved" }
        },
        { new: true }
      );
      if (!si) {
        throw new Error("SI does not exists.");
      } else {
        resolve(si);
      }
    } catch (err) {
      reject(err);
    }
  });
};

exports.send_si_email_to_client = ({ res, si_uid }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let sidoc = await generateandsentSIdoc(res, si_uid, "User");
      resolve(sidoc);
    } catch (err) {
      reject(err);
    }
  });
};

exports.deletesi = ({ si_uid }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let sidata = await ServiceInvoiceModel.findOne({
        $and: [{ si_uid: si_uid }, { status: "Approved" }]
      });
      if (sidata) {
        throw new Error("SI con't be removed, it's approved.");
      } else {
        await ServiceInvoiceModel.deleteOne({ si_uid: si_uid });
        resolve();
      }
    } catch (err) {
      reject(err);
    }
  });
};

function generateandsentSIdoc(res, si_uid, sender_type) {
  return new Promise(async (resolve, reject) => {
    try {
      let sidata = await ServiceInvoiceModel.findOne({ si_uid: si_uid });
      if (!sidata) {
        throw new Error("SI does not exists.");
      }
      let customerdata = await customerModel.findOne({
        customer_uid: sidata.client.client_uid
      });
      if (!customerdata || customerdata == null) {
        throw new Error("customer does not exists.");
      }
      let dir = `${path.dirname(
        process.mainModule.filename
      )}/upload/service_invoice_doc`;
      let si_document = `serviceinvoice_${
        sidata.si_uid
        }_${new Date().getTime()}.pdf`;
      let service_invoice = `${dir}/${si_document}`;
      let invoice_amount = await inWords(sidata.invoice_amount.toFixed(2));
      if (sender_type == "Admin") {
        await ServiceInvoiceModel.findOneAndUpdate(
          { si_uid: si_uid },
          { $set: { si_document: si_document } },
          { new: true }
        );
      } else {
        await ServiceInvoiceModel.findOneAndUpdate(
          { si_uid: si_uid },
          { $set: { si_document: si_document, sent_si_to_client: true } },
          { new: true }
        );
      }
      res.render(
        "serviceinvoice",
        {
          si: sidata,
          customerdata: customerdata,
          invoice_amount: invoice_amount,
          moment: moment
        },
        async (err, html) => {
          if (err) {
            throw new Error("html is not generated.");
          } else {
            PDFConverter.create(html, options).toStream(async (err, pdf) => {
              if (err) {
                throw new Error("pdf is not generated.");
              } else {
                if (!fs.existsSync(dir)) {
                  fs.mkdirSync(dir);
                }
                let stream = fs.createWriteStream(service_invoice);
                pdf.pipe(stream);
                attachments = [
                  {
                    filename: si_document,
                    contentType: "application/pdf",
                    path: `${path.dirname(
                      process.mainModule.filename
                    )}/upload/service_invoice_doc/${si_document}`
                  }
                ];
                if (sender_type == "Admin") {
                  await Mailer.send(
                    process.env.ADMINEMAIL,
                    "Service Invoice",
                    `Please find the below attachment for ${sidata.si_uid}`,
                    attachments
                  );
                } else {
                  await Mailer.send(
                    customerdata.basic_details.email,
                    "Service Invoice",
                    `Please find the below attachment for ${sidata.si_uid}`,
                    attachments
                  );
                }
                resolve(sidata);
              }
            });
          }
        }
      );
    } catch (error) {
      reject(error);
    }
  });
}

async function inWords(num) {
  var a = [
    "",
    "One ",
    "Two ",
    "Three ",
    "Four ",
    "Five ",
    "Six ",
    "Seven ",
    "Eight ",
    "Nine ",
    "Ten ",
    "Eleven ",
    "Twelve ",
    "Thirteen ",
    "Fourteen ",
    "Fifteen ",
    "Sixteen ",
    "Seventeen ",
    "Sighteen ",
    "Nineteen "
  ];
  var b = [
    "",
    "",
    "Twenty",
    "Thirty",
    "Forty",
    "Fifty",
    "Sixty",
    "Seventy",
    "Eighty",
    "Ninety"
  ];
  if ((num = num.toString()).length > 9) return "overflow";
  var numarray = num.toString().split(".");
  num = numarray[0];
  dot = numarray[1];
  n = ("000000000" + num)
    .substr(-9)
    .match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
  d = ("000" + dot).substr(-2).match(/^(\d{2})$/);
  if (!n) return;
  var str = "";
  str +=
    n[1] != 0
      ? (a[Number(n[1])] || b[n[1][0]] + " " + a[n[1][1]]) + "crore "
      : "";
  str +=
    n[2] != 0
      ? (a[Number(n[2])] || b[n[2][0]] + " " + a[n[2][1]]) + "lakh "
      : "";
  str +=
    n[3] != 0
      ? (a[Number(n[3])] || b[n[3][0]] + " " + a[n[3][1]]) + "thousand "
      : "";
  str +=
    n[4] != 0
      ? (a[Number(n[4])] || b[n[4][0]] + " " + a[n[4][1]]) + "hundred "
      : "";
  str += n[5] != 0 ? a[Number(n[5])] || b[n[5][0]] + " " + a[n[5][1]] : "";
  str +=
    d[1] != 0
      ? (d[1] != "" ? "And " : "") +
      (a[Number(d[1])] || b[d[1][0]] + " " + a[d[1][1]]) +
      "Cent Only "
      : "Only";
  return str;
}
