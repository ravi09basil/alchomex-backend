const paymentModel = require("../models/serviceinvoicepayment");
const generateuid = require("../services/generateuid");
const serviceInvoiceModel = require("../models/serviceinvoice");

exports.create_payment = ({ body }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let sip_data = await paymentModel
        .find({ payment_UID: { $exists: true, $ne: null } })
        .sort("-payment_number")
        .limit(1)
        .exec();
      let { payment_UID, payment_number } = await generateuid.generateuidtypes({
        uiddata: sip_data,
        uidtype: "paymentslip"
      });
      const paymentObj = new paymentModel({
        si_uid: body.si_uid,
        payment_UID: payment_UID,
        payment_number: payment_number,
        client: body.client,
        amount_received: body.amount_received,
        received_date: body.received_date,
        UTR_number: body.UTR_number
      });
      let payment = await paymentObj.save();
      if (!payment || payment == null) {
        throw new Error("Payment not added...");
      } else {
        if (body.paymentComplete == true) {
          await serviceInvoiceModel.findOneAndUpdate(
            {
              $and: [{ si_uid: body.si_uid }, { si_uid: { $exists: true } }]
            },
            {
              $set: {
                payment_status: "CloseServiceInvoice",
                closeServiceInvoice_date: new Date()
              }
            }
          );
        }
        resolve(payment);
      }
    } catch (err) {
      reject(err);
    }
  });
};

exports.get_si_uid_payments = ({ si_uid }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let sort = { createdAt: 1 };
      let payments = await paymentModel.find({ si_uid: si_uid }).sort(sort);
      resolve({
        success: true,
        data: payments,
        message: "Service Invoice payment fetched successfully."
      });
    } catch (err) {
      reject(err);
    }
  });
};

exports.delete_payment = ({ si_uid, payment_UID }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let serviceInvoice = await serviceInvoiceModel.findOne({
        $and: [
          { si_uid: { $exists: true } },
          { si_uid: si_uid },
          { payment_status: "CloseServiceInvoice" }
        ]
      });
      if (serviceInvoice) {
        throw new Error(
          "Payment can't be removed, because service invoice is already closed."
        );
      } else {
        let payment = await paymentModel.deleteOne({
          $and: [{ si_uid: si_uid }, { payment_UID: payment_UID }]
        });
        if (payment.deletedCount == 0) {
          throw new Error("Payment slip is not removed");
        } else {
          resolve();
        }
      }
    } catch (err) {
      reject(err);
    }
  });
};
