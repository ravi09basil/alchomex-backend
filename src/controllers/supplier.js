const supplierModel = require('../models/supplier');
const packinglistModel = require('../models/packinglist');

exports.create_supplier = ({ body }) => {
    return new Promise(async (resolve, reject) => {
        try {
            const supplierobj = new supplierModel({
                registered_name: body.registered_name,
                email: body.email,
                mobileNo: body.mobileNo,
                gst_no: body.gst_no,
                cin_no: body.cin_no,
                address: body.address,
                pre_carriage_by: body.pre_carriage_by
            })
            let supplier = await supplierobj.save();
            if (!supplier || supplier == null) {
                throw new Error('supplier not added.');
            }
            resolve(supplier)
        } catch (err) {
            reject(err)
        }
    })
}

exports.getallsuppliers = ({ query }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let { pageNo, pageSize, search } = query; let querydata;
            let _pageNo = parseInt(isNaN(pageNo) ? 1 : pageNo);
            let _pageSize = parseInt(isNaN(pageSize) ? 100 : pageSize);
            let skip = parseInt((_pageNo - 1) * _pageSize);
            let sort = { "createdAt": 1 };
            if (search && search != '') {
                querydata = { $and: [{ $text: { $search: `\"${search}\"` } }, { status: 'Active' }] }
            } else {
                querydata = { status: 'Active' }
            }
            let _count = await supplierModel.countDocuments(querydata);
            let suppliers = await supplierModel.find(querydata)
                .sort(sort)
                .skip(skip)
                .limit(_pageSize);
            resolve({ success: true, data: suppliers, count: _count, message: 'Suppliers returned successfully.' });
        } catch (err) {
            reject(err)
        }
    })
}

exports.getOne_supplier = ({ supplierid }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let supplierdata = await supplierModel.findById(supplierid);
            if (!supplierdata) {
                throw new Error('Supplier does not exists.');
            } else {
                resolve(supplierdata)
            }
        } catch (err) {
            reject(err);
        }
    });
}

exports.update_supplier = ({ supplierid, body }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let data = {
                registered_name: body.registered_name,
                email: body.email,
                mobileNo: body.mobileNo,
                gst_no: body.gst_no,
                cin_no: body.cin_no,
                address: body.address,
                pre_carriage_by: body.pre_carriage_by
            }
            let supplierdata = await supplierModel.findByIdAndUpdate(supplierid, { $set: data }, { new: true })
            if (!supplierdata) {
                throw new Error('Supplier does not exists.');
            } else {
                resolve(supplierdata)
            }
        } catch (err) {
            reject(err);
        }
    });
}

exports.delete_supplier = ({ supplierid }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let packinglist = await packinglistModel.find({ supplier_id: supplierid }, { packing_list_uid: 1 });
            if (packinglist.length > 0) {
                throw new Error("Supplier con't be removed, already in used");
            } else {
                let supplier = await supplierModel.deleteOne({ _id: supplierid })
                if (supplier.deletedCount == 0) {
                    throw new Error("Supplier is not removed");
                } else {
                    resolve()
                }
            }
        } catch (err) {
            reject(err);
        }
    });
}
