const mongoose = require('mongoose');

const CreditNote = mongoose.Schema({
    credit_note_uid: { type: String },
    credit_note_number: { type: Number },
    client: {
        client_name: { type: String },
        client_uid: { type: String }
    },
    credit_note_no: { type: String },
    invoice_uid: { type: String },
    product_uid: { type: String },
    batch_number: { type: String },
    reason_for_creditnote: { type: String },
    qauntity: { type: Number, default: 0 },
    price: { type: Number, default: 0 },
    percentage: { type: Number, default: 0 },
    credit_amount: { type: Number, default: 0 },
    status: { type: String, enum: ['Pending', 'Approved'], default: 'Pending' },
    creditnote_document: { type: String },
    signed_creditnote_doc: { type: String },
    sent_cn_to_client: { type: Boolean, default: false }
}, {
        timestamps: true
    })
module.exports = mongoose.model('creditnote', CreditNote)
