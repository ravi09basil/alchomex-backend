const mongoose = require('mongoose');
let Schema = mongoose.Schema;

const customlogSchema = new Schema({
    action_type: { type: String },
    action_uid: { type: String },
    type: { type: String },
    type_id: { type: Schema.Types.ObjectId },
    description: { type: String },
    updated_by: { type: Schema.Types.ObjectId, ref: 'admins' },
    updated_date: { type: Date, default: new Date() }
}, { timestamps: true });

module.exports = mongoose.model('customlogs', customlogSchema);