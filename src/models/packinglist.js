const mongoose = require("mongoose");
const { Schema } = mongoose;

const PackingListModel = mongoose.Schema(
  {
    packing_list_uid: { type: String, unique: true },
    packing_list_number: { type: Number, unique: true },
    client: {
      client_name: { type: String },
      client_uid: { type: String }
    },
    mode_of_shipping: { type: String },
    place_of_receipt: { type: String },
    port_of_loading: { type: String },
    port_of_discharge: { type: String },
    terms_of_delivery: { type: String },
    products: [
      {
        product_id: { type: String },
        batch_id: { type: Schema.Types.ObjectId, ref: "PIs" },
        brand_name: { type: String },
        client_product_code: { type: String },
        customerCodeNumber: { type: String },
        hsn_code: { type: Number, default: 0 },
        secondary_carton_packing: { type: String },
        shipper_size_inner_dimension: { type: String },
        batch_number: { type: String },
        manufacturing_date: { type: Date },
        expiry_date: { type: Date },
        number_of_packages: { type: Number, default: 0 },
        total_packing_carton: { type: Number, default: 0 },
        unit_of_billing: { type: String },
        complete_packaging_profile: { type: String },
        minimum_batch_size: { type: Number, default: 0 },
        maximum_batch_size: { type: Number, default: 0 },
        registration_no: { type: String },
        registration_date: { type: Date },
        shelf_life: { type: String },
        frieght: { type: Number, default: 0 },
        insurance: { type: Number, default: 0 },
        quantity: { type: Number, default: 0 },
        price: { type: Number, default: 0 },
        amount: { type: Number, default: 0 },
        net_weight: { type: Number, default: 0 },
        gross_weight: { type: Number, default: 0 },
        pi_uid: { type: String },
        pi_uid_date: { type: Date },
        client_po_number: { type: String },
        date_of_issue_of_po: { type: Date }
      }
    ],
    shipping: {
      container_type: { type: String },
      container_number: { type: String },
      shipping_agency: { type: String },
      advanced_received: { type: Number, default: 0 }
    },
    workstatus_date: {
      artwork_uploaded_date: { type: Date },
      manufacture_uploaded_date: { type: Date },
      testing_uploaded_date: { type: Date },
      dispatch_uploaded_date: { type: Date },
      shipped_uploaded_date: { type: Date }
    },
    shipped_onboard_date: { type: Date },
    estimated_arrival_date: { type: Date },
    payment_due_date: { type: Date },
    material_delivery_date: { type: Date },
    admin_document: {
      packinglist_doc: { type: String },
      productinvoice_doc: { type: String },
      CoA_doc: { type: Array },
      certificate_of_origin_doc: { type: String },
      bill_of_lading_doc: { type: String },
      insurance_doc: { type: String },
      approval_status: { type: Boolean, default: false },
      send_to_client: { type: Boolean, default: false },
      send_to_client_date: { type: Date }
    },
    shipped_info: {
      tracking_number: { type: String },
      date_of_courier: { type: Date },
      courires_agency: { type: String },
      delivery_status: { type: Boolean, default: false },
      delivery_date: { type: Date }
    },
    test_of_goods_info: [
      {
        product_id: { type: String },
        client_product_code: { type: String },
        batch_number: { type: String },
        status: {
          type: String,
          enum: ["Pending", "Approved", "Rejected"],
          default: "Pending"
        },
        rejection_files: [
          {
            file_name: { type: String },
            uploaded_date: { type: Date, default: new Date() }
          }
        ],
        signed_creditnote_info: {
          credit_note_uid: { type: String },
          doc_name: { type: String },
          uploaded_date: { type: Date, default: new Date() }
        }
      }
    ],
    supplier_id: [{ type: Schema.Types.ObjectId, ref: "suppliers" }],
    CAPA_sheet_doc: { type: String },
    special_comments: { type: String },
    invoice_number: { type: Number },
    invoice_uid: { type: String },
    invoice_date: { type: Date },
    invoice_format: { type: String, enum: ["IGST", "LUT"], default: "IGST" },
    LUT_number: { type: String },
    gross_price: { type: Number, default: 0 },
    insurance: { type: Number, default: 0 },
    freight: { type: Number, default: 0 },
    insurance_usd: { type: Number, default: 0 },
    freight_usd: { type: Number, default: 0 },
    air_freight: { type: Number, default: 0 },
    total_fob_value: { type: Number, default: 0 },
    total_price: { type: Number, default: 0 },
    credit_price: { type: Number, default: 0 },
    status: {
      type: String,
      enum: ["Pending", "OpenInvoice", "CloseInvoice"],
      default: "Pending"
    },
    shipped_at_port: { type: Boolean, default: false },
    closeinvoice_date: { type: Date }
  },
  {
    timestamps: true
  }
);
module.exports = mongoose.model(
  "packinglist",
  PackingListModel,
  "packinglists"
);
