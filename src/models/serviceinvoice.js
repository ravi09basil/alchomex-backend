const mongoose = require("mongoose");

const ServiceInvoiceSchema = mongoose.Schema(
  {
    si_uid: { type: String, unique: true },
    si_number: { type: Number, unique: true },
    client: {
      client_name: { type: String },
      client_uid: { type: String }
    },
    bill_no: { type: String },
    description_of_incident: { type: String },
    quantity: { type: Number, default: 0 },
    rate: { type: Number, default: 0 },
    invoice_amount: { type: Number, default: 0 },
    status: { type: String, enum: ["Pending", "Approved"], default: "Pending" },
    payment_status: { type: String },
    closeServiceInvoice_date: { type: Date },
    approved_date: { type: Date },
    si_document: { type: String },
    signed_serviceinvoice_doc: { type: String },
    sent_si_to_client: { type: Boolean, default: false }
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("serviceinvoice", ServiceInvoiceSchema);
