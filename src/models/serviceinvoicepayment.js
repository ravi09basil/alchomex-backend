const mongoose = require("mongoose");

const ServiceInvoicePaymentSchema = mongoose.Schema(
  {
    payment_UID: { type: String, unique: true },
    payment_number: { type: Number, unique: true },
    client: {
      client_name: { type: String },
      client_uid: { type: String }
    },
    si_uid: { type: String },
    amount_received: { type: Number },
    received_date: { type: Date },
    SWIFT_number: { type: String },
    UTR_number: { type: String }
  },
  {
    timestamps: true
  }
);
module.exports = mongoose.model(
  "service_invoice_payments",
  ServiceInvoicePaymentSchema
);
