const mongoose = require('mongoose');

const SupplierSchema = mongoose.Schema({
    registered_name: { type: String, required: true, unique: true },
    email: { type: String, lowercase: true, trim: true },
    mobileNo: { type: String },
    gst_no: { type: String },
    cin_no: { type: String },
    address: { type: String },
    status: { type: String, enum: ['Active', 'Inactive'], default: "Active" },
    pre_carriage_by: { type: String },
}, {
        timestamps: true
    })
module.exports = mongoose.model('supplier', SupplierSchema)