const express = require('express');
const router = express.Router();
const adminUseCase = require('../../controllers/admin');
const { Fail } = require('../../services/response')
const auth = require('../../services/auth');
const uploadFile = require('../../services/uploadFile');

router
    .get('/admindetails', auth.authenticate, (req, res) => {
        adminUseCase
            .get_admin_details({ id: req.decoded.id })
            .then((data) => {
                res.json(data)
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

router
    .patch('/updateprofile', auth.authenticate, uploadFile.upload.single('profileimage'), (req, res) => {
        adminUseCase
            .update_adminprofile({ id: req.decoded.id, body: req.body, file: req.file })
            .then((data) => {
                res.json(data)
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

router
    .patch('/changepassword', auth.authenticate, (req, res) => {
        adminUseCase
            .change_adminpassword({ req, body: req.body })
            .then((data) => {
                res.json(data)
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

module.exports = router;    