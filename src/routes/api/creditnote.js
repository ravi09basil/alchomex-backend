const express = require('express');
const creditNoteController = require('../../controllers/creditnote');
const uploadFile = require('../../services/uploadFile');
const router = express.Router();
const { Success, Fail } = require('../../services/response')
const auth = require('../../services/auth');
const path = require('path');

router
    .post('/', auth.authenticate, (req, res) => {
        creditNoteController
            .create_creditnote({ res: res, body: req.body })
            .then((data) => {
                res.json(Success(data, 'Creditnote created Successfully'))
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

router
    .get('/creditnotelists', auth.authenticate, (req, res) => {
        creditNoteController
            .getallcreditnotes({ query: req.query })
            .then((data) => {
                res.json(data)
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

router
    .get('/creditnotedetails/:creditnoteid', auth.authenticate, (req, res) => {
        creditNoteController
            .getcreditnote_details({ creditnoteid: req.params.creditnoteid })
            .then((data) => {
                res.json(Success(data, 'Creditnote details fetched Successfully'))
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

router
    .patch('/:creditnoteid', auth.authenticate, (req, res) => {
        creditNoteController
            .update_creditnote({ creditnoteid: req.params.creditnoteid, body: req.body })
            .then((data) => {
                res.json(Success(data, 'Creditnote updated Successfully'))
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

router
    .delete('/delete/:invoiceuid/:creditnoteuid', auth.authenticate, (req, res) => {
        creditNoteController
            .delete_creditnote({ invoiceid: req.params.invoiceuid, creditnoteid: req.params.creditnoteuid })
            .then((data) => {
                res.json(Success(data, 'Creditnote deleted successfully'))
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

router
    .patch('/uploadsignedcreditnote/:invoiceid', auth.authenticate, uploadFile.upload.single('signed_creditnote_doc'), uploadFile.validate, (req, res) => {
        creditNoteController
            .uploadsigned_creditnote({ invoiceid: req.params.invoiceid, body: req.body, file: req.file })
            .then((data) => {
                res.json(Success(data, 'Signed creditnote doc uploaded Successfully'))
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

router
    .get('/sendcnemail/:credit_note_uid', auth.authenticate, (req, res) => {
        creditNoteController
            .send_cn_email_to_client({ res: res, credit_note_uid: req.params.credit_note_uid })
            .then((data) => {
                res.json(Success(data, 'Credit Note doc send successfully.'))
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

router
    .get('/download/:creditnote_file', (req, res) => {
        let pathname = './upload/creditnote_doc';
        const dir = path.join(path.dirname(require.main.filename), pathname)
        var docpath = dir + '/' + req.params.creditnote_file;
        res.sendFile(docpath);
    })

module.exports = router;