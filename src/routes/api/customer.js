const express = require("express");
const customerController = require("../../controllers/customer");
const router = express.Router();
const { Success, Fail } = require("../../services/response");
const auth = require("../../services/auth");

router.post("/", auth.authenticate, (req, res) => {
  customerController
    .create_customer({ body: req.body })
    .then(data => {
      res.json(Success(data, "Customer created Successfully"));
    })
    .catch(error => {
      if (error.code == 11000) {
        res.json(Fail("This e-mail address is already in use"));
      } else {
        res.json(Fail(error.message));
      }
    });
});

router.get("/getUids", auth.authenticate, (req, res) => {
  customerController
    .getcustomeruids({})
    .then(data => {
      res.json(Success(data, "Customers data fetched Successfully"));
    })
    .catch(error => {
      res.json(Fail(error.message));
    });
});

router.get("/", auth.authenticate, (req, res) => {
  customerController
    .getallcustomers({ query: req.query })
    .then(data => {
      res.json(data);
    })
    .catch(error => {
      res.json(Fail(error.message));
    });
});

router.get("/:customerid", auth.authenticate, (req, res) => {
  customerController
    .getOne_customer({ customerid: req.params.customerid })
    .then(data => {
      res.json(Success(data, "Customer data fetched Successfully"));
    })
    .catch(error => {
      res.json(Fail(error.message));
    });
});

router.patch("/changepassword", auth.authenticate, (req, res) => {
  customerController
    .change_password({ req, body: req.body })
    .then(data => {
      res.json(data);
    })
    .catch(error => {
      res.json(Fail(error.message));
    });
});

router.patch("/:customerid", auth.authenticate, (req, res) => {
  customerController
    .update_customer({ customerid: req.params.customerid, body: req.body })
    .then(data => {
      res.json(Success(data, "Customer updated Successfully"));
    })
    .catch(error => {
      res.json(Fail(error.message));
    });
});

router.delete("/delete/:customeruid", auth.authenticate, (req, res) => {
  customerController
    .delete_customer({ customeruid: req.params.customeruid })
    .then(data => {
      res.json(Success(data, "Customer deleted successfully"));
    })
    .catch(error => {
      res.json(Fail(error.message));
    });
});

module.exports = router;
