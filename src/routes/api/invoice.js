const express = require("express");
const invoiceController = require("../../controllers/invoice");
const router = express.Router();
const { Success, Fail } = require("../../services/response");
const uploadFile = require("../../services/uploadFile");
const auth = require("../../services/auth");

router.patch("/:id", auth.authenticate, (req, res) => {
  invoiceController
    .create_invoice({ req, res, packinglistid: req.params.id, body: req.body })
    .then(data => {
      res.json(Success(data, "Invoice generated successfully"));
    })
    .catch(error => {
      res.json(Fail(error.message));
    });
});

router.patch("/updateinvoice/:id", auth.authenticate, (req, res) => {
  invoiceController
    .update_invoice({ res: res, packinglistid: req.params.id, body: req.body })
    .then(data => {
      res.json(Success(data, "Invoice updated successfully"));
    })
    .catch(error => {
      res.json(Fail(error.message));
    });
});

router.get("/postshippingorderreport/:customerUid",
  auth.authenticate,
  (req, res) => {
    invoiceController
      .post_shipping_order_report({ customerUid: req.params.customerUid })
      .then(data => {
        res.json(data);
      })
      .catch(error => {
        res.json(Fail(error.message));
      });
  }
);

router.get("/invoicelists", auth.authenticate, (req, res) => {
  invoiceController
    .getallinvoices({ query: req.query })
    .then(data => {
      res.json(data);
    })
    .catch(error => {
      res.json(Fail(error.message));
    });
});

router.get("/postshippingorders", auth.authenticate, (req, res) => {
  invoiceController
    .get_post_shipping_orders({ query: req.query, req })
    .then(data => {
      res.json(data);
    })
    .catch(error => {
      res.json(Fail(error.message));
    });
});

router.get("/invoices", auth.authenticate, (req, res) => {
  invoiceController
    .get_invoices({ query: req.query, req: req })
    .then(data => {
      res.json(Success(data, "Invoice lists data fetched Successfully"));
    })
    .catch(error => {
      res.json(Fail(error.message));
    });
});

router.patch("/uploadadmindoc/:invoiceid", auth.authenticate, uploadFile.upload.fields([
  { name: "packing_doc", maxCount: 1 },
  { name: "invoice_doc", maxCount: 1 },
  { name: "coa_doc", maxCount: 1 },
  { name: "certificate_of_origin_doc", maxCount: 1 },
  { name: "bill_of_lading_doc", maxCount: 1 },
  { name: "insurance_doc", maxCount: 1 },
  { name: "airway_bill_doc", maxCount: 1 }
]),
  (req, res) => {
    let packing_doc = "",
      coa_doc = "",
      invoice_doc = "",
      certificate_of_origin_doc = "",
      bill_of_lading_doc = "",
      insurance_doc = "",
      airway_bill_doc = "";
    if (req.files && req.files["packing_doc"]) {
      packing_doc = req.files["packing_doc"][0].filename;
    }
    if (req.files && req.files["coa_doc"]) {
      coa_doc = req.files["coa_doc"][0].filename;
    }
    if (req.files && req.files["invoice_doc"]) {
      invoice_doc = req.files["invoice_doc"][0].filename;
    }
    if (req.files && req.files["certificate_of_origin_doc"]) {
      certificate_of_origin_doc =
        req.files["certificate_of_origin_doc"][0].filename;
    }
    if (req.files && req.files["bill_of_lading_doc"]) {
      bill_of_lading_doc = req.files["bill_of_lading_doc"][0].filename;
    }
    if (req.files && req.files["insurance_doc"]) {
      insurance_doc = req.files["insurance_doc"][0].filename;
    }
    if (req.files && req.files["airway_bill_doc"]) {
      airway_bill_doc = req.files["airway_bill_doc"][0].filename;
    }
    invoiceController
      .upload_admindoc({
        invoiceid: req.params.invoiceid,
        packing_doc: packing_doc,
        coa_doc: coa_doc,
        invoice_doc: invoice_doc,
        certificate_of_origin_doc: certificate_of_origin_doc,
        bill_of_lading_doc: bill_of_lading_doc,
        insurance_doc: insurance_doc,
        airway_bill_doc: airway_bill_doc
      })
      .then(data => {
        res.json(Success(data, "Documents uploaded Successfully."));
      })
      .catch(error => {
        res.json(Fail(error.message));
      });
  }
);

router.get("/sentdocs/:invoiceid", auth.authenticate, (req, res) => {
  invoiceController
    .sent_docs_email_for_client({ invoiceid: req.params.invoiceid })
    .then(data => {
      res.json(Success(data, "Docs sent to client successfully."));
    })
    .catch(error => {
      res.json(Fail(error.message));
    });
});

router.patch(
  "/updatedocapprovalstatus/:invoiceid",
  auth.authenticate,
  (req, res) => {
    invoiceController
      .update_doc_approval_status({
        invoiceid: req.params.invoiceid,
        body: req.body
      })
      .then(data => {
        res.json(Success(data, "Docs approved successfully."));
      })
      .catch(error => {
        res.json(Fail(error.message));
      });
  }
);

router.patch(
  "/uploadsignedcreditnote/:invoiceid",
  auth.authenticate,
  uploadFile.upload.fields([
    { name: "rejection_docs", maxCount: 4 },
    { name: "signed_creditnote_doc", maxCount: 1 }
  ]),
  (req, res) => {
    let signed_creditnote_doc = "";
    if (req.files["signed_creditnote_doc"]) {
      signed_creditnote_doc = req.files["signed_creditnote_doc"][0].filename;
    }
    invoiceController
      .uploadsigned_creditnote({
        invoiceid: req.params.invoiceid,
        body: req.body,
        files: req.files["rejection_docs"],
        signedcreditnote_doc: signed_creditnote_doc
      })
      .then(data => {
        res.json(Success(data, "Signed creditnote doc uploaded Successfully"));
      })
      .catch(error => {
        res.json(Fail(error.message));
      });
  }
);

router.patch("/updateinvoicetoopeninvoice/:invoiceid", auth.authenticate, (req, res) => {
  invoiceController
    .update_invoice_to_openinvoice({
      req,
      invoiceuid: req.params.invoiceid,
      body: req.body
    })
    .then(data => {
      res.json(Success(data, "Invoice updated Successfully"));
    })
    .catch(error => {
      res.json(Fail(error.message));
    });
}
);

router.patch(
  "/addmaterialdeliverydate/:invoiceid",
  auth.authenticate,
  (req, res) => {
    invoiceController
      .add_material_delivery_date({
        req,
        invoiceuid: req.params.invoiceid,
        body: req.body
      })
      .then(data => {
        res.json(Success(data, "Delivery date added Successfully"));
      })
      .catch(error => {
        res.json(Fail(error.message));
      });
  }
);

router.get("/getsignedcreditnotes/:invoiceid", auth.authenticate, (req, res) => {
  invoiceController
    .getsigned_creditnotes({ invoiceid: req.params.invoiceid })
    .then(data => {
      res.json(Success(data, "Signed creditnote doc fetched Successfully"));
    })
    .catch(error => {
      res.json(Fail(error.message));
    });
}
);

router.get("/getmanagepayments/:invoiceid", auth.authenticate, (req, res) => {
  invoiceController
    .getmanagepayments({ invoiceid: req.params.invoiceid, req: req })
    .then(data => {
      res.json(Success(data, "manage payments fetched Successfully"));
    })
    .catch(error => {
      res.json(Fail(error.message));
    });
});

router.patch("/addshippedinfo/:invoiceid", auth.authenticate, (req, res) => {
  invoiceController
    .Add_shipped_info({ invoiceid: req.params.invoiceid, body: req.body })
    .then(data => {
      res.json(
        Success(data, "Invoice document shipped info added Successfully")
      );
    })
    .catch(error => {
      res.json(Fail(error.message));
    });
});

router.patch("/shippedatport/:invoiceid", auth.authenticate, (req, res) => {
  invoiceController
    .shipped_at_port({ invoiceid: req.params.invoiceid })
    .then(data => {
      res.json(Success(data, "Goods delivered to port successfully."));
    })
    .catch(error => {
      res.json(Fail(error.message));
    });
});

router.patch(
  "/updateinvoicestatus/:invoiceid",
  auth.authenticate,
  (req, res) => {
    invoiceController
      .update_invoicestatus({ invoiceid: req.params.invoiceid })
      .then(data => {
        res.json(Success(data, "Invoice status changed successfully."));
      })
      .catch(error => {
        res.json(Fail(error.message));
      });
  }
);

router.get("/invoicedetails/:invoiceid", auth.authenticate, (req, res) => {
  invoiceController
    .getinvoice_details({ invoiceid: req.params.invoiceid, req: req })
    .then(data => {
      res.json(Success(data, "Invoice details fetched Successfully"));
    })
    .catch(error => {
      res.json(Fail(error.message));
    });
});

router.get("/getinvoiceuids/:client_uid", auth.authenticate, (req, res) => {
  invoiceController
    .getinvoice_uids({ client_uid: req.params.client_uid })
    .then(data => {
      res.json(Success(data, "Invoices returned Successfully"));
    })
    .catch(error => {
      res.json(Fail(error.message));
    });
});

router.get("/getinvoiceproducts", auth.authenticate, (req, res) => {
  invoiceController
    .getinvoice_products({
      client_uid: req.query.client_uid,
      invoice_uid: req.query.invoice_uid
    })
    .then(data => {
      res.json(Success(data, "Invoice products returned Successfully"));
    })
    .catch(error => {
      res.json(Fail(error.message));
    });
});

router.get("/getinvoiceproductbatches", auth.authenticate, (req, res) => {
  invoiceController
    .getinvoice_product_batches({
      invoice_uid: req.query.invoice_uid,
      product_id: req.query.product_id
    })
    .then(data => {
      res.json(Success(data, "Invoice products batches returned Successfully"));
    })
    .catch(error => {
      res.json(Fail(error.message));
    });
});

router.patch("/delete/:invoiceuid", auth.authenticate, (req, res) => {
  invoiceController
    .delete_invoice({ invoiceuid: req.params.invoiceuid })
    .then(data => {
      res.json(Success(data, "Invoice deleted successfully"));
    })
    .catch(error => {
      res.json(Fail(error.message));
    });
});

router.get("/coaData/:invoiceid", auth.authenticate, (req, res) => {
  invoiceController
    .coaData({ invoiceuid: req.params.invoiceid })
    .then(data => {
      res.json(Success(data, "Coa Docs list shown Successfully"));
    })
    .catch(error => {
      res.json(Fail(error.message));
    });
});
module.exports = router;
