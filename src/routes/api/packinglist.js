const express = require('express');
const packingListController = require('../../controllers/packinglist')
const router = express.Router();
const { Success, Fail } = require('../../services/response')
const auth = require('../../services/auth');

router
    .post('/', auth.authenticate, (req, res) => {
        packingListController
            .create_packinglist({ res: res, body: req.body })
            .then((data) => {
                res.json(Success(data, 'Packing List created Successfully'))
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

router
    .patch('/:packinglistid', auth.authenticate, (req, res) => {
        packingListController
            .update_packinglist({ res: res, body: req.body, packinglistid: req.params.packinglistid })
            .then((data) => {
                res.json(Success(data, 'Packing List updated Successfully'))
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

router
    .get('/', auth.authenticate, (req, res) => {
        packingListController
            .get_packinglist({ query: req.query })
            .then((data) => {
                res.json(data)
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

router
    .get('/packinglistuid', auth.authenticate, (req, res) => {
        packingListController
            .get_packinglist_id({})
            .then((data) => {
                res.json(Success(data, 'Packing lists ids fetched Successfully'))
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

router
    .post('/addplaceofreceipt', auth.authenticate, (req, res) => {
        packingListController
            .create_placeofreceipt({ body: req.body })
            .then((data) => {
                res.json(Success(data, 'Place of receipt created successfully'))
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

router
    .get('/placeofreceipts', auth.authenticate, (req, res) => {
        packingListController
            .getplaceofreceipts({})
            .then((data) => {
                res.json(Success(data, 'Place of receipts Lists data fetched Successfully'))
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

router
    .post('/addportofloading', auth.authenticate, (req, res) => {
        packingListController
            .create_portofloading({ body: req.body })
            .then((data) => {
                res.json(Success(data, 'Port of loading created successfully'))
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

router
    .get('/portofloadings', auth.authenticate, (req, res) => {
        packingListController
            .getportofloadings({})
            .then((data) => {
                res.json(Success(data, 'Port of loading data fetched Successfully'))
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })


router
    .get('/productlists/:client_uid', auth.authenticate, (req, res) => {
        packingListController
            .getproductlists({ client_uid: req.params.client_uid })
            .then((data) => {
                res.json(Success(data, 'Product lists data fetched Successfully'))
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

router
    .get('/:id', auth.authenticate, (req, res) => {
        packingListController
            .getOne_packinglist({ packingid: req.params.id })
            .then((data) => {
                res.json(Success(data, 'Packing List data fetched Successfully'))
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

router
    .delete('/delete/:packinglistid', auth.authenticate, (req, res) => {
        packingListController
            .delete_packinglist({ packinglistid: req.params.packinglistid })
            .then((data) => {
                res.json(Success(data, 'Packinglist deleted successfully'))
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

module.exports = router;


