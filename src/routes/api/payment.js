const express = require('express');
const paymentController = require('../../controllers/payment')
const router = express.Router();
const { Success, Fail } = require('../../services/response')
const auth = require('../../services/auth');

router
    .post('/', auth.authenticate, (req, res) => {
        paymentController
            .create_payment({ body: req.body })
            .then((data) => {
                res.json(Success(data, 'Payment Slip created Successfully'))
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

router
    .get('/paymentlists', auth.authenticate, (req, res) => {
        paymentController
            .getallpayments({ query: req.query })
            .then((data) => {
                res.json(data)
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

router
    .patch('/:paymentid', auth.authenticate, (req, res) => {
        paymentController
            .update_payment({ paymentid: req.params.paymentid, body: req.body })
            .then((data) => {
                res.json(Success(data, 'Payment updated Successfully'))
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

router
    .get('/paymentdetails/:paymentid', auth.authenticate, (req, res) => {
        paymentController
            .getpayment_details({ paymentid: req.params.paymentid })
            .then((data) => {
                res.json(Success(data, 'Payment Slip details fetched Successfully'))
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

router
    .get('/invoicepayments/:invoiceuid', auth.authenticate, (req, res) => {
        paymentController
            .get_invoiceuid_payments({ invoiceuid: req.params.invoiceuid })
            .then((data) => {
                res.json(data)
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

router
    .delete('/delete/:invoiceuid/:paymentslipuid', auth.authenticate, (req, res) => {
        paymentController
            .delete_paymentslip({ invoiceid: req.params.invoiceuid, paymentslipid: req.params.paymentslipuid })
            .then((data) => {
                res.json(Success(data, 'Payment slip deleted successfully'))
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })



module.exports = router;
