const express = require("express");
const piUseCase = require("../../controllers/pi");
const uploadFile = require("../../services/uploadFile");
const router = express.Router();
const path = require("path");
const { Success, Fail } = require("../../services/response");
const auth = require("../../services/auth");

router.post(
  "/",
  auth.authenticate,
  uploadFile.upload.single("media"),
  uploadFile.validate,
  (req, res) => {
    piUseCase
      .create_proformainvoice({ res: res, body: req.body, file: req.file })
      .then(data => {
        res.json(Success(data, "Performa Invoice created Successfully"));
      })
      .catch(error => {
        res.json(Fail(error.message));
      });
  }
);

router.get("/sendpiemail/:pi_uid", auth.authenticate, (req, res) => {
  piUseCase
    .sendEmailProformaInvoice({ res: res, pi_uid: req.params.pi_uid })
    .then(data => {
      res.json(Success(data, "Performa Invoice created Successfully"));
    })
    .catch(error => {
      res.json(Fail(error.message));
    });
});

router.patch(
  "/uploadsignedpi/:pi_uid",
  auth.authenticate,
  uploadFile.upload.single("signedpi"),
  uploadFile.validate,
  (req, res) => {
    piUseCase
      .uploadsignedpidoc({ pi_uid: req.params.pi_uid, file: req.file })
      .then(data => {
        res.json(Success(data, "Signed PI uploaded Successfully"));
      })
      .catch(error => {
        res.json(Fail(error.message));
      });
  }
);

router.get("/dashboard", auth.authenticate, (req, res) => {
  piUseCase
    .getallpri_post_order({ req: req })
    .then(data => {
      res.json(Success(data, "All Orders returned Successfully"));
    })
    .catch(error => {
      res.json(Fail(error.message));
    });
});

router.get("/download/doc", (req, res) => {
  let { doc_name, doc_type } = req.query;
  let pathname = "";
  if (doc_type == "podoc") {
    pathname = "./upload/podoc";
  } else if (doc_type == "signedpi") {
    pathname = "./upload/signedpi";
  } else if (doc_type == "invoice") {
    pathname = "./upload/invoice";
  } else if (doc_type == "orderstatusdoc") {
    pathname = "./upload/orderstatusfiles";
  } else if (doc_type == "coadoc") {
    pathname = "./upload/CoA_doc";
  } else if (doc_type == "proformainvoice") {
    pathname = "./upload/proformainvoice";
  } else if (doc_type == "packinglist") {
    pathname = "./upload/packinglist";
  } else if (doc_type == "bill_of_lading_doc") {
    pathname = "./upload/bill_of_lading_doc";
  } else if (doc_type == "insurance_doc") {
    pathname = "./upload/insurance_doc";
  } else if (doc_type == "certificate_of_origin_doc") {
    pathname = "./upload/certificate_of_origin_doc";
  } else if (doc_type == "airwaybill_doc") {
    pathname = "./upload/airwaybill_doc";
  } else if (doc_type == "signed_si_doc") {
    pathname = "./upload/signed_si_doc";
  } else if (doc_type == "service_invoice_doc") {
    pathname = "./upload/service_invoice_doc";
  } else if (doc_type == "signed_creditnote_doc") {
    pathname = "./upload/signed_creditnote_doc";
  }
  const dir = path.join(path.dirname(require.main.filename), pathname);
  var docpath = dir + "/" + doc_name;
  res.sendFile(docpath);
});

router.get(
  "/prishippingorderreport/:customerUid",
  auth.authenticate,
  (req, res) => {
    piUseCase
      .pri_shipping_order_report({ customerUid: req.params.customerUid })
      .then(data => {
        res.json(data);
      })
      .catch(error => {
        res.json(Fail(error.message));
      });
  }
);

router.get("/prishippingorderpi", auth.authenticate, (req, res) => {
  piUseCase
    .pri_shipping_order_pi({ query: req.query, req: req })
    .then(data => {
      res.json(data);
    })
    .catch(error => {
      res.json(Fail(error.message));
    });
});

router.get(
  "/prishippingorderpiproducts/:pi_uid",
  auth.authenticate,
  (req, res) => {
    piUseCase
      .pri_shipping_order_pi_products({ pi_uid: req.params.pi_uid, req: req })
      .then(data => {
        res.json(data);
      })
      .catch(error => {
        res.json(Fail(error.message));
      });
  }
);

router.get(
  "/prishippingorderpiproductbatches/:pi_uid/:productid",
  auth.authenticate,
  (req, res) => {
    piUseCase
      .pri_shipping_order_pi_product_batches({
        pi_uid: req.params.pi_uid,
        productid: req.params.productid,
        req: req
      })
      .then(data => {
        res.json(data);
      })
      .catch(error => {
        res.json(Fail(error.message));
      });
  }
);

router.patch(
  "/changeproductbatchestatus/:actionType",
  auth.authenticate,
  uploadFile.upload.fields([
    { name: "files", maxCount: 10 },
    { name: "coa_doc", maxCount: 1 }
  ]),
  (req, res) => {
    let coa_doc = "";
    let file = "";
    if (req.files["coa_doc"]) {
      coa_doc = req.files["coa_doc"][0].filename;
    }
    if (req.files["files"]) {
      file = req.files["files"][0].filename;
    }
    piUseCase
      .change_product_manufacturingandtesting_status({
        body: req.body,
        actionType: req.params.actionType,
        file: file,
        coa_doc: coa_doc,
        req: req
      })
      .then(data => {
        res.json(data);
      })
      .catch(error => {
        res.json(Fail(error.message));
      });
  }
);

router.get(
  "/producthistory/:pi_uid/:productid",
  auth.authenticate,
  (req, res) => {
    piUseCase
      .get_product_product_history({
        pi_uid: req.params.pi_uid,
        productid: req.params.productid
      })
      .then(data => {
        res.json(data);
      })
      .catch(error => {
        res.json(Fail(error.message));
      });
  }
);

router.get("/batchhistory/:pi_uid/:batchid", auth.authenticate, (req, res) => {
  piUseCase
    .get_product_batch_history({
      pi_uid: req.params.pi_uid,
      batchid: req.params.batchid
    })
    .then(data => {
      res.json(data);
    })
    .catch(error => {
      res.json(Fail(error.message));
    });
});

router.get("/productartworkdoc/:product_uid", auth.authenticate, (req, res) => {
  piUseCase
    .get_product_artworkdoc({ product_uid: req.params.product_uid })
    .then(data => {
      res.json(Success(data, "Product artwork doc returned successfully."));
    })
    .catch(error => {
      res.json(Fail(error.message));
    });
});

router.get("/changeartworkstatus", auth.authenticate, (req, res) => {
  piUseCase
    .copy_productartworkdor_to_piartworkdoc({ query: req.query, req })
    .then(data => {
      res.json(Success(data, "Product artwork status changed successfully."));
    })
    .catch(error => {
      res.json(Fail(error.message));
    });
});

router.get("/:pi_uid/:piordertype", auth.authenticate, (req, res) => {
  piUseCase
    .getPIdetails({
      pi_uid: req.params.pi_uid,
      piordertype: req.params.piordertype,
      req: req
    })
    .then(data => {
      res.json(Success(data, "PI details returned Successfully"));
    })
    .catch(error => {
      res.json(Fail(error.message));
    });
});

router.get("/", auth.authenticate, (req, res) => {
  piUseCase
    .getallPIs({ query: req.query, req: req })
    .then(data => {
      res.json(data);
    })
    .catch(error => {
      res.json(Fail(error.message));
    });
});

router.patch(
  "/:id",
  auth.authenticate,
  uploadFile.upload.single("media"),
  (req, res) => {
    piUseCase
      .update_proformainvoice({
        res: res,
        body: req.body,
        file: req.file,
        pi_uid: req.params.id
      })
      .then(data => {
        res.json(Success(data, "Performa Invoice updated Successfully"));
      })
      .catch(error => {
        res.json(Fail(error.message));
      });
  }
);

router.patch(
  "/orderstatus/:productId/:actiontype/:batchId",
  auth.authenticate,
  uploadFile.upload.fields([
    { name: "files", maxCount: 10 },
    { name: "coa_doc", maxCount: 1 }
  ]),
  (req, res) => {
    let coa_doc = "";
    if (req.files["coa_doc"]) {
      coa_doc = req.files["coa_doc"][0].filename;
    }
    let query = {
      productId: req.params.productId,
      actiontype: req.params.actiontype,
      batchId: req.params.batchId
    };
    piUseCase
      .updateOrderStatus({
        body: req.body,
        files: req.files["files"],
        coa_doc: coa_doc,
        query: query
      })
      .then(data => {
        res.json(
          Success(data, "Performa Invoice Order Status updated Successfullyy")
        );
      })
      .catch(error => {
        res.json(Fail(error.message));
      });
  }
);

router.delete("/delete/:pi_uid", auth.authenticate, (req, res) => {
  piUseCase
    .deletepi({ pi_uid: req.params.pi_uid })
    .then(data => {
      res.json(Success(data, "PI deleted successfully"));
    })
    .catch(error => {
      res.json(Fail(error.message));
    });
});

module.exports = router;
