const express = require('express');
const uploadFile = require('../../services/uploadFile');
const productController = require('../../controllers/product')
const router = express.Router();
const { Success, Fail } = require('../../services/response')
const auth = require('../../services/auth');

router
    .post('/', auth.authenticate, uploadFile.upload.single('file'), uploadFile.validate, (req, res) => {
        productController
            .create_registered_products({ file: req.file.filename })
            .then((data) => {
                res.json(Success(data, 'Excel file successfully loaded'))
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

router
    .post('/single', auth.authenticate, uploadFile.upload.single('artwork_doc'), (req, res) => {
        productController
            .createSingle_Product({ productbody: req.body, file: req.file })
            .then((data) => {
                res.json(Success(data, 'Product saved Successfully'))
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

router
    .post('/updatesheet', auth.authenticate, uploadFile.upload.single('file'), uploadFile.validate, (req, res) => {
        productController
            .update_registered_product({ file: req.file.filename })
            .then((data) => {
                res.json(Success(data, 'Excel file updated successfully.'))
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

router
    .get('/', auth.authenticate, (req, res) => {
        productController
            .get_all_products({ query: req.query })
            .then((data) => {
                res.json(data);
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

router
    .get('/:product_uid', auth.authenticate, (req, res) => {
        productController
            .getOne_product({ product_uid: req.params.product_uid })
            .then((data) => {
                res.json(Success(data, 'Product fetched Successfully'))
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

router
    .patch('/:id', auth.authenticate, uploadFile.upload.single('artwork_doc'), (req, res) => {
        productController
            .update_product({ productid: req.params.id, productbody: req.body, file: req.file })
            .then((data) => {
                res.json(Success(data, 'Product updated Successfully'))
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

router
    .delete('/:product_uid', auth.authenticate, (req, res) => {
        productController
            .delete_product({ product_uid: req.params.product_uid })
            .then((data) => {
                res.json(Success(data, 'Product deleted successfully'))
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })


module.exports = router;

