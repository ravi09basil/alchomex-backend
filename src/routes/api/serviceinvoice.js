const express = require('express');
const serviceInvoiceController = require('../../controllers/serviceinvoice');
const uploadFile = require('../../services/uploadFile');
const router = express.Router();
const { Success, Fail } = require('../../services/response')
const auth = require('../../services/auth');
const path = require('path');

router
    .post('/', auth.authenticate, (req, res) => {
        serviceInvoiceController
            .create_service_invoice({ res: res, body: req.body })
            .then((data) => {
                res.json(Success(data, 'Service Invoice created successfully.'))
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

router
    .get('/si_lists', auth.authenticate, (req, res) => {
        serviceInvoiceController
            .getallserviceinvoices({ query: req.query, req })
            .then((data) => {
                res.json(data)
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

router
    .patch('/uploadsignedsi/:si_uid', auth.authenticate, uploadFile.upload.single('signedsi'), uploadFile.validate, (req, res) => {
        serviceInvoiceController
            .uploadsignedsidoc({ si_uid: req.params.si_uid, file: req.file })
            .then((data) => {
                res.json(Success(data, 'Signed SI uploaded Successfully'))
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

router
    .get('/sendsiemail/:si_uid', auth.authenticate, (req, res) => {
        serviceInvoiceController
            .send_si_email_to_client({ res: res, si_uid: req.params.si_uid })
            .then((data) => {
                res.json(Success(data, 'Service Invoice sent to client successfully'))
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

router
    .delete('/delete/:si_uid', auth.authenticate, (req, res) => {
        serviceInvoiceController
            .deletesi({ si_uid: req.params.si_uid })
            .then((data) => {
                res.json(Success(data, 'SI deleted successfully'))
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

module.exports = router    