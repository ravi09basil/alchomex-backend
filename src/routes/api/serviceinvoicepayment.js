const express = require("express");
const paymentController = require("../../controllers/serviceinvoicepayment");
const router = express.Router();
const { Success, Fail } = require("../../services/response");
const auth = require("../../services/auth");

router.post("/", auth.authenticate, (req, res) => {
  console.log("======>>>>>>>");
  paymentController
    .create_payment({ body: req.body })
    .then(data => {
      res.json(Success(data, "Payment Done Successfully"));
    })
    .catch(error => {
      res.json(Fail(error.message));
    });
});

router.get("/get_si_uid_payments/:si_uid", auth.authenticate, (req, res) => {
  paymentController
    .get_si_uid_payments({ si_uid: req.params.si_uid })
    .then(data => {
      res.json(data);
    })
    .catch(error => {
      res.json(Fail(error.message));
    });
});

router.delete("/delete/:si_uid/:payment_UID", auth.authenticate, (req, res) => {
  paymentController
    .delete_payment({
      si_uid: req.params.si_uid,
      payment_UID: req.params.payment_UID
    })
    .then(data => {
      res.json(Success(data, "Payment  deleted successfully"));
    })
    .catch(error => {
      res.json(Fail(error.message));
    });
});

module.exports = router;
