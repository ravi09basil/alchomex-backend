const express = require('express');
const supplierUseCase = require('../../controllers/supplier');
const router = express.Router();
const { Success, Fail } = require('../../services/response')
const auth = require('../../services/auth');

router
    .post('/', auth.authenticate, (req, res) => {
        supplierUseCase
            .create_supplier({ body: req.body })
            .then((data) => {
                res.json(Success(data, 'Supplier created Successfully'))
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

router
    .get('/', auth.authenticate, (req, res) => {
        supplierUseCase
            .getallsuppliers({ query: req.query })
            .then((data) => {
                res.json(data);
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

router
    .get('/:supplierid', auth.authenticate, (req, res) => {
        supplierUseCase
            .getOne_supplier({ supplierid: req.params.supplierid })
            .then((data) => {
                res.json(Success(data, 'Supplier details returned Successfully'))
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

router
    .patch('/:supplierid', auth.authenticate, (req, res) => {
        supplierUseCase
            .update_supplier({ supplierid: req.params.supplierid, body: req.body })
            .then((data) => {
                res.json(Success(data, 'Supplier updated Successfully'))
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

router
    .delete('/delete/:supplierid', auth.authenticate, (req, res) => {
        supplierUseCase
            .delete_supplier({ supplierid: req.params.supplierid })
            .then((data) => {
                res.json(Success(data, 'Supplier deleted successfully'))
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })

module.exports = router;    