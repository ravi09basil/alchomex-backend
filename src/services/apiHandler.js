module.exports = (res, data, message = 'Success') => {
    if (data && data.isBoom && data.isBoom == true) {
      let error = data.output.payload;
      delete error["error"];
      error = Object.assign({ success: false }, error)
      const statusCode = data.output.statusCode;
      if (data.data != null) {
        error.data = data.data;
      }
      res.status(statusCode).json(error);
    } else {
      const response = {
        statusCode: 200,
        message,
        data,
      };
      res.status(200).json(response);
    }
  };
  