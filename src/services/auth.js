const { verify } = require('./jwt')

exports.authenticate = (req, res, next) => {
    var token = req.headers['authenticate'];
    if (!token)
        res.status(422).send({ success: false, message: 'No token provided.' });
    else {
        try {
            var decoded = verify(token)
            req.decoded = decoded;
            next();
        } catch (err) {
            req.decoded = undefined
            res.status(401).send({ success: false, message: 'Token expired.' });
        }
    }
}

